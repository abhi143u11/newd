<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


?>
<?php include('header.php');
/*
* load customer id and name with full information 
*/

 ?>

 
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PAYMENTADD;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST" role="form" data-toggle="validator">



<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Paymnet Method</label>
  <div class="col-md-4">
    <select id="pay_type" name="pay_type" class="form-control" onchange="getcustomer(this.value)">
    <option value="N">Select Payment</option>
    <option value="P">PURCHASE</option>
    <option value="S">SALES</option>
    <option value="E">EXPENSES</option>
    
    </select>
  </div>
</div>


<!-- Customer Id Select -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Customer</label>
  <div class="col-md-4">
    <select id="customer" name="customer" class="form-control" onchange="getbillnumber(this.value)">

    </select>
  </div>
</div>

        


<!-- Bill Number which fetch from dynamjically which is of puchase or sales bill number-->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Bill No.</label>
  <div class="col-md-4">
    <select id="invoice" name="invoice" class="form-control" onchange="gettotalamount(this.value)">
    
    </select>
  </div>
</div>



<!-- total amount-->
<div class="form-group">
  <label class="col-md-4 control-label" for="brokeragevalue">Total Amount</label>  
  <div class="col-md-4">
  <input id="total_amount" readonly="readonly" name="total_amount" placeholder="" class="form-control input-md" required="" type="text">
  </div>
  
  <div class="col-md-4">
    <label class="checkbox-inline" for="checkboxes-0">
      <input name="balance" id="balance" value="1" type="checkbox">
      Use Customer Balance
    </label>
  </div>
  
</div>

 <!-- Amount paid -->
<div class="form-group">
  <label class="col-md-4 control-label" for="brokeragevalue">Amount Paid</label>  
  <div class="col-md-4">
  <input id="amount_paid" name="amount_paid" placeholder="" class="form-control input-md" required="" type="number">
    
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="brokeragevalue">Customer Balance</label>  
  <div class="col-md-4">
  <input id="use_customer_balance" readonly="readonly" name="use_customer_balance" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Remark-->
<div class="form-group">
  <label class="col-md-4 control-label" for="brokerageamount">Remark</label>  
  <div class="col-md-4">
  <input id="remark" name="remark" placeholder="" class="form-control input-md" type="text">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>







<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="add" name="add" class="btn btn-default" value="add">
  </div>
</div>





</form>
            
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();
</script>

<?php

if(isset($_REQUEST["add"]))
{
 $data = $database->clean_data($_POST);
    $payment_add = new Ds_Payment();
    $payment_add->set_pay_type($data['pay_type']); 
    $payment_add->set_customerid($data['customer']); 
    $payment_add->set_invoiceid($data['invoice']);
    $payment_add->set_total_amount($data['total_amount']); 
    $payment_add->set_amount_paid($data['amount_paid']); 
    $payment_add->set_use_customer_balance($data['use_customer_balance']);
    $payment_add->set_remark($data['remark']);
    $payment_add->set_created_on($data['pdate']);  
    
    $bal=$data['balance'];
    if($bal!=1)
    {
     $bal=0;   
    }
    
    $payment_add->set_balancech($bal);
//    var_dump($payment_add);                                     
    
   $add_payment=$payment_add->add_payment();  
      if($add_payment==TRUE)
      {
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "bottom",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Payment transaction Saved Successfully');
}, 1000);


   </script>
   <?php     
}
}   

?>

 <script type="text/javascript">
//get Customer Information
var pmethod;
var cid;
function getcustomer(val)
{                         
    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'customer='+val,
    success:function(data){
    $("#customer").html(data);
     
    }  
    });
pmethod=val;  
} 

function getmes(val)
{
    
    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'balancecid='+val,
    success:function(data){
    $("#use_customer_balance").val(data);
    }  
    });
}


function getbillnumber(val)
{  
cid=val;                       

    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'pmethod='+pmethod+'&cid='+val,
    success:function(data){
    $("#invoice").html(data);
    }  
    });
this.getmes(cid); 
}


function gettotalamount(val)
{  
    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'totatlmethod='+pmethod+'&tid='+val,
    success:function(data){
    var d=data;
    $("#total_amount").val(d);
    $("#amount_paid").attr('max',d);
    }  
    });
}

</script>


