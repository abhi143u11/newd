<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');
require_once('classes/class.purchase.php');
require_once('classes/class.sales.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

$customer_list=new Ds_Customer();
$result=$customer_list->all_list_customer();

$payment_purchase=new Ds_Payment();
$payment_pending_purchase_result=$payment_purchase->purchase_pending_amount();

$payment_sale=new Ds_Payment();
$payment_pending_sales_result=$payment_sale->sales_pending_amount();

$purchase_record=new Ds_Purchase();
$sales_record=new Ds_Sales();

?>
<?php include('header.php'); ?>

<style type="text/css"> 
</style>
     
        <section class="content">

<div class="col-md-12">
            
          <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Sales</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="revenue-chart" style="height: 300px;"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
              
              
              
                <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Line Chart</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body chart-responsive">
                  <div class="chart" id="line-chart" style="height: 300px;"></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              
              
</div>




                               
    <div class="col-md-12"  >
                <div class="box-header">
                  <h3 class="box-title">Purchase and Sales Pending Amount</h3>
                </div><!-- /.box-header --> 
                <hr size="10" style="border: 1px blue;" /> 
 </div>       
        
  <div class="col-md-6"  >
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo DASHBOARDPURCHASEPAYMENT;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                

<table  id="example2" class="table table-bordered table-striped">
<thead>
<tr>
<th>Name</th>
<th>Weight Type</th>
<th>Type</th>
<th>Weight</th>
<th>rate</th>
<th>Due Date</th>
<th>Due Days</th>
<th>Amount</th>
</tr>
</thead>
<?php


if(!empty($payment_pending_purchase_result))
{    
foreach($payment_pending_purchase_result as $obj)
{
$pid=$obj['pid'];
$amount_sum=$payment_purchase->sum_amount_of_purchase_invoice($pid);
$amount=$obj['amount']-$amount_sum;
$purchase_info=$purchase_record->purchase_info_fromid($pid);
$purchase_info=$purchase_info[0];
$weighttype=$purchase_info['weighttype'];
$type=$purchase_info['type'];
$duedate=$purchase_info['duedate'];
$rate=$purchase_info['rate'];
$weight=$purchase_info['weight'];
//day calculation
$d=date("Y-m-d",time());
$date1 = new DateTime($d);
$date2 = new DateTime(date('Y-m-d',strtotime($duedate)));
$diff = $date1->diff($date2);
$dueday=$diff->days;
if($amount!=0)
{
    echo "<tr>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$weighttype."</td>";
    echo "<td>".$type."</td>";
    echo "<td>".$weight."</td>";
    echo "<td>".$rate."</td>";
    echo "<td>".date('d/m/Y',strtotime($duedate))."</td>";
    echo "<td>".$dueday."</td>";
    echo "<td class='bg-orange-active color-palette'>".$obj['amount']."(".$amount.")</td>";
    echo "</tr>";
}
}    
}    
?>
</table>
</div>
</div>
</div>




<!-- right Side Panel -->

  <div class="col-md-6"  >
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo DASHBOARDSALESPAYMENT;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                

<table  id="example2" class="table table-bordered table-striped">
<thead>
<tr>
<th>Name</th>
<th>Weight Type</th>
<th>Type</th>
<th>Weight</th>
<th>rate</th>
<th>Due Date</th>
<th>Due Days</th>

<th>Amount</th>
</tr>

</thead>
<?php

 

if(!empty($payment_pending_sales_result))
{    
foreach($payment_pending_sales_result as $obj)
{
$sid=$obj['sid'];    
$amount_sum=$payment_sale->sum_amount_of_sale_invoice($sid);
$amount=$obj['amount']-$amount_sum;
$sales_info=$sales_record->sales_info_fromid($sid);
$sales_info=$sales_info[0];
$weighttype=$sales_info['weighttype'];
$type=$sales_info['type'];
$duedate=$sales_info['duedate'];
$rate=$sales_info['rate'];
$weight=$sales_info['weight'];

//day calculation
$d=date("Y-m-d",time());
$date1 = new DateTime($d);
$date2 = new DateTime(date('Y-m-d',strtotime($duedate)));
$diff = $date1->diff($date2);
$dueday=$diff->days;

if($amount!=0)
{
    echo "<tr>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$weighttype."</td>";
    echo "<td>".$type."</td>";
    echo "<td>".$weight."</td>";
    echo "<td>".$rate."</td>";
    echo "<td>".date('d/m/Y',strtotime($duedate))."</td>";
    echo "<td>".$dueday."</td>";
    echo "<td class='bg-orange-active color-palette'>".$obj['amount']."(".$amount.")</td>";
    echo "</tr>";
}
}    
}



?>
</table>
</div>
</div>
</div>

  


</section>
<?php include('footer.php');

  
 ?>
  <script type="text/javascript">
         $(function () {
        "use strict";
        
        // AREA CHART
        var area = new Morris.Area({
          element: 'revenue-chart',
          resize: true,
          data: [
            {y: '01', item1: 200, item2: 400},
            {y: '02', item1: 123, item2: 213},
            {y: '03', item1: 213, item2: 566},
            {y: '04', item1: 565, item2: 556},
            {y: '05', item1: 200, item2: 400},
            {y: '06', item1: 123, item2: 213},
            {y: '8', item1: 21, item2: 546},
            {y: '7', item1: 55, item2: 565}
          ],
          xkey: 'y',
          ykeys: ['item1', 'item2'],
          labels: ['Sales', 'Purchase'],
          lineColors: ['#a0d0e0', '#3c8dbc'],
          hideHover: 'auto'
        });

 
        // LINE CHART
        var line = new Morris.Line({
          element: 'line-chart',
          resize: true,
          data: [
            {y: '2016 W1', item1: 2666},
            {y: '2016 W2', item1: 2778},
            {y: '2016 W3', item1: 4912},
            {y: '2016 Q4', item1: 3767},
            {y: '2016 Q5', item1: 6810},
            {y: '2016 Q6', item1: 5670},
            {y: '2016 Q7', item1: 4820},
            {y: '2016 Q8', item1: 15073},
            {y: '2016 Q9', item1: 10687},
            {y: '2016 Q10', item1: 8432},
            {y: '2016 Q11', item1: 8432},
            {y: '2016 Q12', item1: 8432}
          ],
          xkey: 'y',
          ykeys: ['item1'],
          labels: ['Sales'],
          lineColors: ['#3c8dbc'],
          hideHover: 'auto'
        });
        
        
     
      });

      
   </script>      
  </body>
</html>

 


