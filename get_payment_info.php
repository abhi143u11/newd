<?php

require_once('classes/class.database.php');
require_once('classes/class.customer.php');
require_once('classes/class.purchase.php');
require_once('classes/class.stock.php');
require_once('classes/class.sales.php');

/*
* customer ID and Information 
*/

if(isset($_POST["customer"]))
{
global $database;
$customer=$_POST["customer"];
$query="";
if($customer=="P")
{
$query="SELECT DISTINCT a.cid,a.name,a.balance FROM customer a INNER JOIN purchase b WHERE a.cid=b.pcid";
}
if($customer=="S")
{
$query="SELECT DISTINCT a.cid,a.name,a.balance FROM customer a INNER JOIN sales b WHERE a.cid=b.scid";
}

$def=0;
echo "<option value=".$def.">Select Customer</option>";

$result=$database->query_fetch_full_result($query);

foreach($result as $val)
{
echo "<option value=".$val['cid'].">".$val['name']."</option>";    
}

        
}
    
/*
* get Customertss  bill number
*/
if(isset($_POST["pmethod"]))
{
global $database;
$customer=$_POST["pmethod"];
$query="";
$pcid=$_POST["cid"];
if($customer=="P")
{
$query="SELECT pid as id,amount as camount FROM purchase WHERE pcid=".$pcid."";



}
if($customer=="S")
{
$query="SELECT sid as id,amount as camount FROM sales WHERE scid=".$pcid."";
}

$def=0;
echo "<option value=".$def.">Select Bill Number</option>";

$result=$database->query_fetch_full_result($query);
foreach($result as $val)
{

$query_for_payment_check="SELECT sum(amount_paid) as amount FROM payment WHERE pay_type='".$customer."' and invoiceID=".$val['id']."";
$result_data=$database->query_fetch_full_result($query_for_payment_check);
$result_data=$result_data[0];
$amount=$result_data['amount'];
$purchase_amount=$val['camount'];
if($purchase_amount>$amount)
{    
echo "<option value=".$val['id'].">".$val['id']."</option>";    
}
}
        
}

/*
* get all amount of one customer  
*/
if(isset($_POST["totatlmethod"]))
{
global $database;
$customer=$_POST["totatlmethod"];
$query="";
$tid=$_POST["tid"];
if($customer=="P")
{
$query="SELECT amount  FROM purchase WHERE pid=".$tid."";

}
if($customer=="S")
{
$query="SELECT amount  FROM sales WHERE sid=".$tid."";
}

/*
* First Check wether it is remaining for PAyment 
*/
if(!empty($query))
{
$query_for_payment_check="SELECT sum(amount_paid) as amount FROM payment WHERE pay_type='".$customer."' and invoiceID=".$tid."";
$result_data=$database->query_fetch_full_result($query_for_payment_check);
$result_data=$result_data[0];
$amount=$result_data['amount'];


$result=$database->query_fetch_full_result($query);
$remailpaymrnt=0;
foreach($result as $val)
{
$remailpaymrnt=$val['amount']-$amount;    
echo $remailpaymrnt;    
}
}
else
{
echo "0";    
}    
        
}


//demo


if(isset($_POST["demo"]))
{
global $database;
$customer=$_POST["demo"];
$query="SELECT * FROM customer";
$result=$database->query_fetch_full_result($query);
$data=array();
foreach($result as $val)
{
$data[]=$val;    
}
echo json_encode($data);
}


/*
* Current customer balance get 
*/
if(isset($_POST["balancecid"]))
{
global $database;
$customerid=$_POST["balancecid"];
$query="SELECT balance FROM customer WHERE cid=".$customerid."";
$result=$database->query_fetch_full_result($query);
foreach($result as $val)
{
echo $val['balance'];
}
}

?>