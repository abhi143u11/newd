<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.customer.php');


if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

$customer_list=new Ds_Customer();
$result=$customer_list->all_list_customer();



?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo CUSTOMERALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr><th>Name</th><th>Address</th><th>Phone</th><th>Company Name</th><th>Balance</th><th>Edit</th><th>Delete</th></tr></thead>
<?php
if(!empty($result))
{    
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$obj['address']."</td>";
    echo "<td>".$obj['phoneno']."</td>";
    echo "<td>".$obj['companyname']."</td>";
    echo "<td>".$obj['balance']."</td>";
    echo "<td><a href='customer_edit.php?cid=".$obj['cid']."'><i class=\"fa fa-edit\"></i></a></td>";
    echo "<td><a href='customer_delete.php?cid=".$obj['cid']."'  onclick=\"return confirm('Really delete?');\"><i class=\"fa fa-trash text-red\"></i></a></td>
    </tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
   
}
    ?>
  </body>
</html>



