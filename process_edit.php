<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.process.php');
require_once('classes/class.purchase.php');
require_once('classes/class.processtype.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

  

if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);

    $process_update = new Ds_Process();
    //set all the vars
    $process_update->set_process_id($data['process_id']);
    $process_update->set_purchase_id($data['purchase_id']);
    $process_update->set_ptname($data['process_type_name']); 
    $process_update->set_amount($data['amount']);    
    $process_update->set_weight($data['weight']);
    $update_process=$process_update->update_process();
 
 
    if($update_process==TRUE)
    {
      notify("info","Process Update Successfully");
    } 
}   







$prid=$_GET['prid'];


if(isset($_GET['prid']))
{
$prid=$_GET['prid'];;
}





$prid_process = new Ds_Process($prid);
//$prid_process->list_of_process_data();

$purchase_fetch=new Ds_Purchase();
$purchase_array=$purchase_fetch->all_list_purchase();

$process_type=new Ds_Processtype();
$process_type_list=$process_type->list_of_all_processtype();



?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PROCESSUPDATE;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST">
<input type="hidden" name="process_id" value="<?php echo $prid_process->get_process_id();  ?>">

<!-- Form Name -->


<!-- Purchase ID-->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Purchase ID</label>
  <div class="col-md-4">
    <select id="purchase_id" name="purchase_id" class="form-control">
    <?php
  

  
    
    foreach($purchase_array as $obj)
    {
 
  
     echo " <option value=".$obj['pid'].">".$obj['pid']."</option>";
     
    }
    ?>
    </select>
  </div>
</div>




<!-- process type-->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Process Type</label>
  <div class="col-md-4">
    <select id="process_type_name" name="process_type_name" class="form-control">
    <?php
    foreach($process_type_list as $obj)
    {
     echo " <option value=".$obj['process_type_id'].">".$obj['process_type']."</option>";
     
    }
    ?>
    </select>
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Amount</label>  
  <div class="col-md-4">
  <input id="amount" name="amount" value="<?php echo $prid_process->get_amount(); ?>" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phonno">Weight</label>  
  <div class="col-md-4">
  <input id="weight" name="weight" value="<?php echo $prid_process->get_weight(); ?>" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>


<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="Update">
  </div>
</div>

</fieldset>
</form>              
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?> 