<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.purchase.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

$purchase_list=new Ds_Purchase();
$result=$purchase_list->all_list_purchase();



?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PURCHASEALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<table id="example2" class="table table-bordered table-striped">  
<thead>
<tr>
<th>Bill No</th>
<th>Date</th>
<th>Weight Type</th>
<th>Amount</th>
<th>Edit</th>
<th>Delete</th>
</tr>
</thead>
<?php
if(!empty($result))
{     
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['pid']."</td>";
    echo "<td>".date('d/m/Y',$obj['pdate'])."</td>";
    echo "<td>".$obj['weighttype']."</td>";
    echo "<td>".$obj['amount']."</td>";
    echo "<td><a href='purchase_edit.php?pid=".$obj['pid']."'&cid=".$obj['pcid']."><i class=\"fa fa-edit\"></i></a></td>";
    echo "<td><a href='purchase_delete.php?pid=".$obj['pid']."'  onclick=\"return confirm('Really delete?');\"><i class=\"fa fa-trash text-red\"></i></a></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
   
}
    ?>
  </body>
</html>




