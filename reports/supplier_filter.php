<?php
require '../../includes/session_validator.php';
require '../../config/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../../css/pop-up.css" rel="stylesheet" />

    </head>
    <body>
        <div class="pop-up-wrapper">
            <div class="pop-up-contents">
                <div class="pop-up-header">
                    <div class="close"></div>
                    <h1>Price Filter</h1>

                    <div style="clear: both"></div>
                </div>
                <form class="pop-up-form" id="category-form" action="supplier.php" method="POST" enctype="multipart/form-data">
                    <table border="0" width="100%">
                        <tr>
                            <td width="200">Supplier:</td>
                            <td>
                                <select name="itemLocation" class="select" required="" style="width: 100%;">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query_supplier = "SELECT `supplierID`, `supplierName`  FROM supplier  ORDER BY `supplierName` ASC";
                                    $result_supplier = mysql_query($query_supplier) or die(mysql_error());
                                    while ($row_supplier = mysql_fetch_array($result_supplier)) {
                                        ?>
                                        <option value="<?php echo $row_supplier['supplierID'] ?>"><?php echo $row_supplier['supplierName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="pop-up-footer">
                    <button type="reset" class="post" style="margin-right: 0" form="category-form">Cancel</button>
                    <button type="submit" class="post" form="category-form">Generate</button>
                    <div style="clear: both"></div>
                </div>
            </div>
        </div>
    </body>
</html>
