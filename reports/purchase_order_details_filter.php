<?php
require '../../includes/session_validator.php';
require '../../config/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../../css/pop-up.css" rel="stylesheet" />

        <script type="text/javascript">
            $(document).ready(function() {
                $('.select').chosen();
            });
        </script>
    </head>
    <body>
        <div class="pop-up-wrapper">
            <div class="pop-up-contents">
                <div class="pop-up-header">
                    <div class="close"></div>
                    <h1>Purchase Order Details Filter</h1>

                    <div style="clear: both"></div>
                </div>
                <form class="pop-up-form" id="category-form" action="purchase_order_details.php" method="POST" enctype="multipart/form-data">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td>Date:</td>
                            <td>
                                <select name="dateRange" id="dateRange" class="select" style="width: 400px;">
                                    <option value=""></option>
                                    <option value="All">All</option>
                                    <option value="custom">Custom</option>
                                    <option value="Today">Today</option>
                                    <option value="This month">This month</option>
                                    <option value="This week">This week</option>
                                    <option value="This quarter">This quarter</option>
                                    <option value="This year">This year</option>
                                    <option value="Yesterday">Yesterday</option>
                                    <option value="Last week">Last week</option>
                                    <option value="Last month">Last month</option>
                                    <option value="Last quarter">Last quarter</option>
                                    <option value="Last 7 days">Last 7 days</option>
                                    <option value="Last 30 days">Last 30 days</option>
                                    <option value="Last 90 days">Last 90 days</option>
                                    <option value="Last 365 days">Last 365 days</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="custom-date">
                            <td></td>
                            <td>From: <input name="dateFrom" class="text" max="<?php echo date('Y-m-d'); ?>" required=""  type="date" style="width: 280px; float: right"/></td>
                        </tr>
                        <tr class="custom-date">
                            <td></td>
                            <td>to: <input name="dateTo" class="text" max="<?php echo date('Y-m-d'); ?>" required="" type="date" style="width: 280px; float: right"/></td>
                        </tr>
                        <tr>
                            <td width="200">Supplier Name:</td>
                            <td>
                                <select name="supplierID" class="select" required="" style="width: 400px;">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query_supplier = "SELECT `supplierID`, `supplierName`  FROM supplier  ORDER BY `supplierName` ASC";
                                    $result_supplier = mysqli_query($link, $query_supplier) or die(mysqli_error($link));
                                    while ($row_supplier = mysqli_fetch_array($result_supplier)) {
                                        ?>
                                        <option value="<?php echo $row_supplier['supplierID'] ?>"><?php echo $row_supplier['supplierName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Item Location:</td>
                            <td>
                                <select name="itemLocation" class="select" required="" style="width: 400px;">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query_location = "SELECT `locationID`, `locationName`  FROM location  ORDER BY `locationName` ASC";
                                    $result_location = mysqli_query($link, $query_location) or die(mysqli_error($link));
                                    while ($row_location = mysqli_fetch_array($result_location)) {
                                        ?>
                                        <option value="<?php echo $row_location['locationID'] ?>"><?php echo $row_location['locationName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Category Name:</td>
                            <td>
                                <select name="itemCategory" class="select" required="" style="width: 400px">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query = "SELECT `itemCategoryID`, category FROM itemcategory ORDER BY category ASC";
                                    $result = mysqli_query($link, $query) or die(mysqli_error($link));
                                    while ($row = mysqli_fetch_array($result)) {
                                        ?>
                                        <option value="<?php echo $row['itemCategoryID'] ?>"><?php echo $row['category'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Status:</td>
                            <td>
                                <select name="purchaseStatus" class="select" required="" style="width: 400px">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <option value="Paid">Paid</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="pop-up-footer">
                    <button type="reset" class="post" style="margin-right: 0" form="category-form">Cancel</button>
                    <button type="submit" class="post" form="category-form">Generate</button>
                    <div style="clear: both"></div>
                </div>
            </div>
        </div>
    </body>
</html>
