<?php
require '../../includes/session_validator.php';
require '../../config/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../../css/pop-up.css" rel="stylesheet" />

        <script type="text/javascript">
        $(document).ready(function (){
            $('.select').chosen();
        });
         </script>
    </head>
    <body>
        <div class="pop-up-wrapper">
            <div class="pop-up-contents">
                <div class="pop-up-header">
                    <div class="close"></div>
                    <h1>Sales Order Summary Filter</h1>

                    <div style="clear: both"></div>
                </div>
                <form class="pop-up-form" id="category-form" action="sales_order_summary.php" method="POST" enctype="multipart/form-data">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td>Date:</td>
                            <td>
                                <select name="dateRange" id="dateRange" class="select" style="width: 400px;">
                                    <option value=""></option>
                                    <option value="All">All</option>
                                    <option value="custom">Custom</option>
                                    <option value="Today">Today</option>
                                    <option value="This month">This month</option>
                                    <option value="This week">This week</option>
                                    <option value="This quarter">This quarter</option>
                                    <option value="This year">This year</option>
                                    <option value="Yesterday">Yesterday</option>
                                    <option value="Last week">Last week</option>
                                    <option value="Last month">Last month</option>
                                    <option value="Last quarter">Last quarter</option>
                                    <option value="Last 7 days">Last 7 days</option>
                                    <option value="Last 30 days">Last 30 days</option>
                                    <option value="Last 90 days">Last 90 days</option>
                                    <option value="Last 365 days">Last 365 days</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="custom-date">
                            <td></td>
                            <td>From: <input name="dateFrom" class="text" max="<?php echo date('Y-m-d'); ?>" required=""  type="date" style="width: 280px; float: right"/></td>
                        </tr>
                        <tr class="custom-date">
                            <td></td>
                            <td>to: <input name="dateTo" class="text" max="<?php echo date('Y-m-d'); ?>" required="" type="date" style="width: 280px; float: right"/></td>
                        </tr>
                        <tr>
                            <td width="200">Customer Name:</td>
                            <td>
                                <select name="customerID" class="select" required="" style="width: 400px;">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query_customer = "SELECT `customerID`, `customerName`  FROM customer  ORDER BY `customerName` ASC";
                                    $result_customer = mysqli_query($link, $query_customer) or die(mysqli_error($link));
                                    while ($row_customer = mysqli_fetch_array($result_customer)) {
                                        ?>
                                        <option value="<?php echo $row_customer['customerID'] ?>"><?php echo $row_customer['customerName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Status:</td>
                            <td>
                                <select name="invoiceStatus" class="select" required="" style="width: 400px;">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <option value="Draft">Draft</option>
                                    <option value="Paid">Paid</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="pop-up-footer">
                    <button type="reset" class="post" style="margin-right: 0" form="category-form">Cancel</button>
                    <button type="submit" class="post" form="category-form">Generate</button>
                    <div style="clear: both"></div>
                </div>
            </div>
        </div>
    </body>
</html>
