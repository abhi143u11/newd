<?php
require '../../includes/session_validator.php';
require '../../functions/general_functions.php';
ob_start();
// Getting item data

require '../../config/config.php';


$movementType = $_POST['movementType'];
$fullName = $_POST['fullName'];
$location = $_POST['itemLocation'];
$dateRange = clean($_POST['dateRange']);
$dateFrom = clean($_POST['dateFrom']);
$dateTo = clean($_POST['dateTo']);
$thisMonth = date('Ym');

switch ($location) {
    case 'All':
        $filter = '';

        break;

    case '':
        $filter = '';
        break;

    default:
        $filter = " WHERE l.locationID = '$location'";
        break;
}

switch ($movementType) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';
        break;

    default:
        $filter .= " AND movementType = '$movementType'";
        break;
}

switch ($fullName) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';
        break;

    default:
        $filter .= " AND u.`userID` = '$fullName'";
        break;
}

switch ($dateRange) {
    case 'custom':
        $filter .= " AND DATE(movementDate) BETWEEN '$dateFrom' AND '$dateTo'";
        break;

    case 'Today':
        $filter .= " AND DATE(movementDate) = CURRENT_DATE()";
        break;

    case 'This week':
        $filter .= " AND WEEKOFYEAR(movementDate) = WEEKOFYEAR(CURRENT_DATE())";
        break;

    case 'This month':
        $filter .= " AND EXTRACT(YEAR_MONTH FROM movementDate) = '$thisMonth'";
        break;

    case 'This quarter':
        $filter .= " AND QUARTER(movementDate) = QUARTER(CURRENT_DATE())";
        break;

    case 'This year':
        $filter .= " AND YEAR(movementDate) = YEAR(CURRENT_DATE())";
        break;

    case 'Yesterday':
        $filter .= " AND DATE(movementDate) = SUBDATE(CURRENT_DATE(), 1)";
        break;

    case 'Last week':
        $filter .= " AND WEEKOFYEAR(movementDate) = WEEKOFYEAR(CURRENT_DATE())-1";
        break;

    case 'Last month':
        $filter .= " AND DATE(movementDate) BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND LAST_DAY(CURRENT_DATE() - INTERVAL 1 MONTH)";
        break;

    case 'Last quarter':
        $filter .= " AND QUARTER(movementDate) = QUARTER(CURRENT_DATE)-1";
        break;

    case 'Last year':
        $filter .= " AND YEAR(movementDate) = YEAR(CURRENT_DATE()) - 1";
        break;

    case 'Last 7 days':
        $filter .= " AND DATE(movementDate) BETWEEN SUBDATE(CURRENT_DATE(), 7) AND CURRENT_DATE()";
        break;

    case 'Last 30 days':
        $filter .= " AND DATE(movementDate ) BETWEEN SUBDATE(CURRENT_DATE(), 30) AND CURRENT_DATE()";
        break;
    case 'Last 90 days':
        $filter .= " AND DATE(movementDate) BETWEEN SUBDATE(CURRENT_DATE(), 90) AND CURRENT_DATE()";
        break;
    case 'Last 365 days':
        $filter .= " AND DATE(movementDate) BETWEEN SUBDATE(CURRENT_DATE(), 365) AND CURRENT_DATE()";
        break;

    case 'All':
    case '':
    default:
        $filter .= '';
        break;
}

strpos($filter, 'WHERE') > 0 ? : $filter = preg_replace('/AND/', 'WHERE', $filter, 1);

$query_movement = "SELECT DATE_FORMAT(`movementDate`, '%d %b, %Y %H:%i') AS movementDate,
                      `movementType`, `movedQuantity`, `movementRemarks`,
                       `itemDescription`, `itemCode`, l.`locationName` AS fromLocation,
                       n.`locationName` AS toLocation, `userFullName`, u.`userID`
                 FROM movements m
           INNER JOIN item i
                   ON i.`itemID` = m.`itemID`
            LEFT JOIN location l
                   ON l.`locationID` = m.`fromLocation`
            LEFT JOIN location n
                   ON n.`locationID` = `toLocation`
           INNER JOIN `user`u
                   ON u.`userID` = m.`userID`
                   {$filter}
             ORDER BY DATE(movementDate) DESC, itemDescription ASC";
                   
$result_movementr = mysqli_query($link, $query_movement) or die(mysqli_error($link));

if (mysqli_num_rows($result_movementr) < 1) {
    info('message', 'No Movement(s) match this creteria!');
    header('Location: reports.php');
    exit;
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />

        <title>inWare | MOVEMENTS</title>

        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/tooltip.css" rel="stylesheet" type="text/css">
        <link href="../../css/print.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/tooltip.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {

                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('.tooltip').tipTip({
                    delay: "300"
                });

                $('#pdf').click(function() {

                    savePDF('report', '../../css/print.css', 'item_movements');
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php require '../../includes/header.php'; ?>
            <?php require '../../includes/sidebar.php'; ?>
            <div class="content">
                <?php
                // Displaying messages and errors
                include '../../includes/info.php';
                ?>
                <h1>Item Movements Report</h1>
                <div class="hr-line"></div>
                <div class="actions" style="top: 100px; width: auto; right: 0; margin: 0 15px 0 0" >
                    <button class="print tooltip" accesskey="P" title="Print [Alt+Shift+P]" onClick="printPage('report', '../../css/print.css')">Print</button>
                    <button class="pdf tooltip" accesskey="D" title="Save as PDF [Alt+Shift+D]" id="pdf" >PDF</button>
                </div>
                <form action="../pdf/pdf.php" method="post" id="html-form" style="display: none">
                    <input type="hidden" name="html" id="html">
                    <input type="hidden" name="pdfName" id="pdf-name">
                </form>
                <div class="report-wrapper">
                    <div id="report">
                        <div class="sheet-wraper">

                            <?php
                            // Setting report tittle
                            $report_title = "ITEM MOVEMENT";
                            include '../../includes/report_header.php';
                            ?>

                            <div>
                                <table cellpadding="3" cellspacing="0" border="1" width="100%" class="two-groups">
                                    <tr>
                                        <th>ITEM</th>
                                        <th>DESCRIPTION</th>
                                        <th>DATE</th>
                                        <th>TYPE</th>
                                        <th>FROM LOCATION</th>
                                        <th>TO LOCATION</th>
                                        <th>REMARKS</th>
                                        <th align="right">QUANTITY</th>
                                    </tr>

                                    <?php
                                    $nthrow = 0;

                                    while ($item = mysqli_fetch_array($result_movementr)) {
                                        echo '<tr  class="';
                                        if ($nthrow % 2 != 0)
                                            echo 'odd';
                                        echo '" >';
                                        echo '<td>' . $item['itemCode'] . '</td>';
                                        echo '<td>' . $item['itemDescription'] . '</td>';
                                        echo '<td>' . $item['movementDate'] . '</td>';
                                        echo '<td>' . $item['movementType'] . '</td>';
                                        echo '<td>' . $item['fromLocation'] . '</td>';
                                        echo '<td>' . $item['toLocation'] . '</td>';
                                        echo '<td>' . $item['movementRemarks'] . '</td>';
                                        echo '<td align="right">' . $item['movedQuantity'] . '</td>';
                                        echo '</tr>';
                                        
                                        $nthrow++;
                                    }
                                    ?>
                                    <tr class="no-border"><td colspan="10"  class="no-border" style="height: 15px;"></td>&nbsp;</tr>
                                    <tr class="total-row">
                                        <td colspan="8" align="right"><strong>TOTAL MOVEMENTS <?php echo $nthrow; ?></strong></td>
                                    </tr>
                                </table>

                                <div style="clear: both;"></div>
                            </div>
                            <!-- end sheet-wrapper  --></div>
                        <!-- end #report --></div>
                    <!-- end .report-wrapper --></div>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
            $('.reports').attr("id", "current");
            var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: true, //Should contents open by default be animated into view?
                persiststate: false, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            });
    </script>
</html>
<?php ob_flush(); ?>
