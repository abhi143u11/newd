<?php
require '../../includes/session_validator.php';
require '../../config/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../../css/pop-up.css" rel="stylesheet" />

        <script type="text/javascript">
        $(document).ready(function (){
            $('.select').chosen();
        });
         </script>
    </head>
    <body>
        <div class="pop-up-wrapper">
            <div class="pop-up-contents">
                <div class="pop-up-header">
                    <div class="close"></div>
                    <h1>Inventory Estimation Filter</h1>

                    <div style="clear: both"></div>
                </div>
                <form class="pop-up-form" id="category-form" action="inventory_estimation.php" method="POST" enctype="multipart/form-data">
                    <table border="0" width="100%" cellpadding="5">
                        
                        <tr>
                            <td width="200">Item Location:</td>
                            <td>
                                <select name="itemLocation" class="select" required="" style="width: 400px;">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query_location = "SELECT `locationID`, `locationName`  FROM location  ORDER BY `locationName` ASC";
                                    $result_location = mysqli_query($link, $query_location) or die(mysqli_error($link));
                                    while ($row_location = mysqli_fetch_array($result_location)) {
                                        ?>
                                        <option value="<?php echo $row_location['locationID'] ?>"><?php echo $row_location['locationName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Category Name:</td>
                            <td>
                                <select name="itemCategory" class="select" required="" style="width: 400px">
                                    <option value="" disabled="" selected="" style="display:none;"></option>
                                    <option value="All">All</option>
                                    <?php
                                    $query = "SELECT `itemCategoryID`, category FROM itemcategory ORDER BY category ASC";
                                    $result = mysqli_query($link, $query) or die(mysqli_error($link));
                                    while ($row = mysqli_fetch_array($result)) {
                                        ?>
                                        <option value="<?php echo $row['itemCategoryID'] ?>"><?php echo $row['category'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">Averaging Period (in days):</td>
                            <td>
                                <input name="averagingPeriod" type="number" class="number" style="width: 388px;" />
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="pop-up-footer">
                    <button type="reset" class="post" style="margin-right: 0" form="category-form">Cancel</button>
                    <button type="submit" class="post" form="category-form">Generate</button>
                    <div style="clear: both"></div>
                </div>
            </div>
        </div>
    </body>
</html>
