<?php
require '../../includes/session_validator.php';
require '../../functions/general_functions.php';
ob_start();
// Getting item data

require '../../config/config.php';

$customerID = $_POST['customerID'];
  $dateRange = clean($_POST['dateRange']);
  $dateFrom = clean($_POST['dateFrom']);
$dateTo = clean($_POST['dateTo']);
$thisMonth = date('Ym');
$filter = 'WHERE `customer`.customerID = ""';

switch ($customerID[0]) {
    case 'All':
        $filter = '';

        break;

    default:
        
        for ($i = 0; $i < count($customerID); $i++) {
            $filter .= " OR `customer`.customerID = '$customerID[$i]'";
        }  
        break;
}
 switch ($dateRange) {
    case 'custom':
        $filter .= " AND DATE(created_on) BETWEEN '$dateFrom' AND '$dateTo'";
        break;
    
    case 'Today':
        $filter .= " AND DATE(created_on)= CURRENT_DATE()";
        break;
    
    case 'This week':
        $filter .= " AND WEEKOFYEAR(created_on) = WEEKOFYEAR(CURRENT_DATE())";
        break;

    case 'This month':
        $filter .= " AND EXTRACT(YEAR_MONTH FROM created_on) = '$thisMonth'";
        break;

    case 'This quarter':
        $filter .= " AND QUARTER(created_on)= QUARTER(CURRENT_DATE())";
        break;
    
    case 'This year':
        $filter .= " AND YEAR(created_on)= YEAR(CURRENT_DATE())";
        break;
    
     case 'Yesterday':
        $filter .= " AND DATE(created_on)= SUBDATE(CURRENT_DATE(), 1)";
        break;
    
     case 'Last week':
        $filter .= " AND WEEKOFYEAR(created_on) = WEEKOFYEAR(CURRENT_DATE())-1";
        break;
    
     case 'Last month':
        $filter .= " AND DATE(created_on) BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND LAST_DAY(CURRENT_DATE() - INTERVAL 1 MONTH)";
        break;
    
    case 'Last quarter':
        $filter .= " AND QUARTER(created_on) = QUARTER(CURRENT_DATE)-1";
        break;
    
    case 'Last year':
        $filter .= " AND YEAR(created_on) = YEAR(CURRENT_DATE()) - 1";
        break;
    
    case 'Last 7 days':
        $filter .= " AND DATE(created_on) BETWEEN SUBDATE(CURRENT_DATE(), 7) AND CURRENT_DATE()";
        break;
    
    case 'Last 30 days':
        $filter .= " AND DATE(created_on)BETWEEN SUBDATE(CURRENT_DATE(), 30) AND CURRENT_DATE()";
        break;
    case 'Last 90 days':
        $filter .= " AND DATE(created_on)BETWEEN SUBDATE(CURRENT_DATE(), 90) AND CURRENT_DATE()";
        break;
    case 'Last 365 days':
        $filter .= " AND DATE(created_on) BETWEEN SUBDATE(CURRENT_DATE(), 365) AND CURRENT_DATE()";
        break;

    case 'All':
    case '':
    default:
        $filter .= '';
        break;
}

strpos($filter, 'WHERE') > 0 ? : $filter = preg_replace('/AND/', 'WHERE', $filter, 1);

$query_sales = "SELECT  CASE WHEN `payment_type`='1' Then 'Cash'
                        WHEN `payment_type`='2' Then 'Cheque' END as paymenttype,
                        `invoiceID`, `total_amount`, `amount_paid`,  `cheque_no`, `cheque_date`, 
                        customerName FROM `payment` INNER JOIN `customer` ON `payment`.`customerID` =`customer`.`customerID`
				  
                       {$filter} 
                       
              ORDER BY `payment`.invoiceID ASC";
        
$result_sales = mysqli_query($link, $query_sales) or die(mysqli_error($link));

if (mysqli_num_rows($result_sales) < 1) {
    info('message', 'No Supplier(s) match this creteria!');
    header('Location: reports.php');
    exit;
}

while ($row_item = mysqli_fetch_array($result_sales)) {
    $itemLocation[$row_item['customerName']][] = $row_item['invoiceID'];
    $itemCategory[$row_item['invoiceID']][$row_item['customerName']][] = $row_item;
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />

        <title>inWare | USERS</title>

        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/tooltip.css" rel="stylesheet" type="text/css">
        <link href="../../css/print.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/tooltip.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {

                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('.tooltip').tipTip({
                    delay: "300"
                });

                $('#pdf').click(function() {

                    savePDF('report', '../../css/print.css', 'customer_list');
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php require '../../includes/header.php'; ?>
            <?php require '../../includes/sidebar.php'; ?>
            <div class="content">
                <?php
                // Displaying messages and errors
                include '../../includes/info.php';
                ?>
                <h1>Customer List Report</h1>
                <div class="hr-line"></div>
                <div class="actions" style="top: 100px; width: auto; right: 0; margin: 0 15px 0 0" >
                    <button class="print tooltip" accesskey="P" title="Print [Alt+Shift+P]" onClick="printPage('report', '../../css/print.css')">Print</button>
                    <button class="pdf tooltip" accesskey="D" title="Save as PDF [Alt+Shift+D]" id="pdf" >PDF</button>
                </div>
                <form action="../pdf/pdf.php" method="post" id="html-form" style="display: none">
                    <input type="hidden" name="html" id="html">
                    <input type="hidden" name="pdfName" id="pdf-name">
                </form>
                <div class="report-wrapper">
                    <div id="report">
                        <div class="sheet-wraper">

                            <?php
                            // Setting report tittle
                            $report_title = "CUSTOMER LIST";
                            include '../../includes/report_header.php';
                            ?>

                            <div>
                                <table cellpadding="3" cellspacing="0" border="1" width="100%" class="two-groups">
                                    <tr>
                                        <th>Invoice</th>
                                        <th >Total </th>
                                        <th >Paid</th>
                                        <th >Remaining</th>
                                        <th >Payment Type</th>
                                        <th >Cheque No</th>
                                        <th >Cheque Date</th>
                                    </tr>

                                   <?php
                                    $nthrow = 0;
                                    $MainTotalSoldQnty = 0;
                                    $MainTotalSales = 0;
                                    $MainTotalCost = 0;
                                    $MainTotalPrice = 0;

                                    foreach ($itemLocation as $key => $value) {

                                        $CatTotalSoldQnty = 0;
                                        $CatTotalSales = 0;
                                        $CatTotalCost = 0;
                                        $CatTotalPrice = 0;

                                        echo '<tr><td colspan="6" style="font-style: italic; color: #0ca696;">' . $key . '</td></tr>';
                                                $totalSoldQnty = 0;
                                            $totalSales = 0;
                                            $totalCost = 0;
                                            $totalPrice = 0;
                                            $amount_paid = 0;
                                        foreach (array_unique($value) as $category) {

                                        
                                            echo '<tr><td colspan="6" style="font-style: italic">' . $category . '</td></tr>';

                                            foreach ($itemCategory[$category][$key] as $item) {
                                               $amount_paid += $item['amount_paid'];
                                                echo '<tr  class="';
                                                if ($nthrow % 2 != 0)
                                                    echo 'odd';
                                                echo '" >';
                                                echo '<td>' . $item['invoiceID'] . '</td>';
                                             
                                                echo '<td >' . $SoldQnty = $item['total_amount'] . '</td>';
                                                echo '<td >' . number_format($sales = $item['amount_paid'], '2', '.', ',') . '</td>';
                                                echo '<td>' . number_format($cost = $item['total_amount'] - $amount_paid, '2', '.', ',') . '</td>';
                                              
                                                   echo '<td>' . $item['paymenttype'] . '</td>';
                                                   echo '<td>' . $item['cheque_no'] . '</td>';
                                                   echo '<td>' . date('d-m-Y',strtotime($item['cheque_date'])) . '</td>';
                                                echo '</tr>';

                                                $nthrow++;
                                                         $totalSales += $sales;
                                                         
                                                     
                                            }     
                                               
                                              $totalCost += $cost;
                                                $totalSoldQnty += $SoldQnty;
                                                     
                                                   
                                            
                                                
                                                
                                                $totalPrice += $price;
                                                 $amount_paid = 0;
                                        }
                                               $MainTotalSales +=  $totalSales;
                                             $MainTotalSoldQnty+=    $totalSoldQnty;
                                             //  $MainTotalSales = 0;
                                    $MainTotalCost += $totalCost;
                                    $MainTotalPrice += $totalPrice;
                                    
                                    }
                                            ?>
                                    <tr><td colspan="7">&nbsp;</td></tr>
                                    <tr class="total-row">
                                        <td><strong>MAIN TOTALS</strong></td>
                                        <td><strong><?php echo $MainTotalSoldQnty ?></strong> </td>
                                        <td><strong><?php echo number_format($MainTotalSales, '2', '.', ',') ?></strong> </td>
                                        <td colspan="4"><strong><?php echo number_format($MainTotalCost, '2', '.', ',') ?></strong> </td>
                                     
                                       
                                    </tr>
                                </table>

                                <div style="clear: both;"></div>
                            </div>
                            <!-- end sheet-wrapper  --></div>
                        <!-- end #report --></div>
                    <!-- end .report-wrapper --></div>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
            $('.reports').attr("id", "current");
            var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: true, //Should contents open by default be animated into view?
                persiststate: false, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            });
    </script>
</html>
<?php ob_flush(); ?>
