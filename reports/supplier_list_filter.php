<?php
require '../../includes/session_validator.php';
require '../../config/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../../css/pop-up.css" rel="stylesheet" />
        <link href="../../css/chosen.css.css" rel="stylesheet" />

        <script src="../../js/jquery-1.8.2.js" type="text/javascript"></script>
         <script src="../../js/chosen.jquery.js" type="text/javascript"></script>
         
         <script type="text/javascript">
        $(document).ready(function (){
            $('.select').chosen();
        });
         </script>
    </head>
    <body>
        <div class="pop-up-wrapper">
            <div class="pop-up-contents">
                <div class="pop-up-header">
                    <div class="close"></div>
                    <h1>Suppler Filter</h1>

                    <div style="clear: both"></div>
                </div>
                <form class="pop-up-form" id="category-form" action="supplier_list.php" method="POST" enctype="multipart/form-data">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td width="200" style="vertical-align: top">Supplier Name:</td>
                            <td>
                                <select name="supplierID[]" class="select" multiple="" required="" style="width: 400px; height: 200px;">
                                    <option value="All">All</option>
                                    <?php
                                    $query_supplier = "SELECT `supplierID`, `supplierName`  FROM supplier  ORDER BY `supplierName` ASC";
                                    $result_supplier = mysqli_query($link, $query_supplier) or die(mysqli_error($link));
                                    while ($row_supplier = mysqli_fetch_array($result_supplier)) {
                                        ?>
                                        <option value="<?php echo $row_supplier['supplierID'] ?>"><?php echo $row_supplier['supplierName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="pop-up-footer">
                    <button type="reset" class="post" style="margin-right: 0" form="category-form">Cancel</button>
                    <button type="submit" class="post" form="category-form">Generate</button>
                    <div style="clear: both"></div>
                </div>
            </div>
        </div>
    </body>
</html>
