<?php
require '../../includes/session_validator.php';
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />
        <title>inWare | GENERATE INVOICES</title>
        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/chosen.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>
        <script src="../../js/chosen.jquery.js" type="text/javascript"></script>
        <script src="../../js/report_filters.js" type="text/javascript"></script>
        <style type="text/css">
            textarea {
                font: inherit !important;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('#dateRange').live('change', function() {

                    if ($(this).val() === 'custom') {
                        $('.custom-date').css('display', 'table-row');
                        $('.custom-date').find('input[type="date"]').attr('required', 'required');
                    } else {
                        $('.custom-date').css('display', 'none');
                        $('.custom-date').find('input[type="date"]').removeAttr('required');
                    }
                });
            });

        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php
            require '../../includes/header.php';
            require_once '../../includes/sidebar.php';
            ?>
            <div class="content">
                <?php
                // Displaying message and errors
                include_once '../../includes/info.php';
                ?>
                <h1>Generate Reports</h1>
                <div class="hr-line"></div>
                <?php
                echo access_supplier_reports();
                echo access_purchase_reports();
                echo access_inventory_reports();
                echo access_sale_reports();
                echo access_customer_reports();
                echo access_payment_reports();
                ?>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
        $('.reports').attr("id", "current");
        var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

        ddaccordion.init({
            headerclass: "expandable", //Shared CSS class name of headers group that are expandable
            contentclass: "categoryitems", //Shared CSS class name of contents group
            revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
            mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
            collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
            defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
            onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
            animatedefault: true, //Should contents open by default be animated into view?
            persiststate: false, //persist state of opened contents within browser session?
            toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
            togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
            animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
            oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                //do nothing
            },
            onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                //do nothing
            }
        });
    </script>
</html>
