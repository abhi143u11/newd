<?php
require '../../includes/session_validator.php';
require '../../functions/general_functions.php';
ob_start();
// Getting item data

require '../../config/config.php';

$location = clean($_POST['itemLocation']);
$category = clean($_POST['itemCategory']);
$averagingPeriod = clean($_POST['averagingPeriod']);
$filter = '';

switch ($location) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';
        break;

    default:
        $filter .= " AND o.locationID = '$location'";
        break;
}

switch ($category) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';

    default:
        $filter .= " AND c.itemCategoryID = '$category'";
        break;
}

strpos($filter, 'WHERE') > 0 ? : $filter = preg_replace('/AND/', 'WHERE', $filter, 1);

$query_sales = "SELECT `itemCode`, `itemDescription`, c.`itemCategoryID`, category, 
                       o.`locationID`, `locationName`, p.`invoiceDate`,
                       l.`itemQuantity` AS availableQuantity,
                       DATE_FORMAT(SUBDATE(CURRENT_DATE(), $averagingPeriod), '%d %b, %Y') AS startDate,
                       SUM(IFNULL(p.`itemQuantity`, 0)) AS soldQuatity,
                       $averagingPeriod / SUM(IFNULL(p.`itemQuantity`, 0)) * l.`itemQuantity` AS duration,
                       DATE_FORMAT(DATE_ADD(CURRENT_DATE(), INTERVAL $averagingPeriod / SUM(IFNULL(p.`itemQuantity`, 0)) * l.`itemQuantity` DAY), '%d %b, %Y') AS estDate
                  FROM item t
            INNER JOIN itemcategory c
                    ON c.`itemCategoryID` = t.`itemCategoryID`
            INNER JOIN itemlocation l
                    ON t.`itemID` = l.`itemID`
            INNER JOIN location o
                    ON o.`locationID` = l.`locationID`
             LEFT JOIN (SELECT v.itemLocationID, i.`invoiceDate`, v.`itemQuantity`
                          FROM invoiceitem v
                    INNER JOIN invoice i
                            ON i.`invoiceID` = v.`invoiceID`
                         WHERE `invoiceStatus` = 'Paid'
                           AND `invoiceDate` BETWEEN SUBDATE(CURRENT_DATE(), $averagingPeriod) AND CURRENT_DATE()
                    ) p
                    ON l.`itemLocationID` = p.itemLocationID
                       {$filter}
              GROUP BY locationID, itemCategoryID, `itemCode` 
              ORDER BY category ASC, itemDescription ASC";

$result_sales = mysqli_query($link, $query_sales) or die(mysqli_error($link));

if (mysqli_num_rows($result_sales) < 1) {
    info('message', 'No item(s) match this creteria!');
    header('Location: reports.php');
    exit;
}

while ($row_item = mysqli_fetch_array($result_sales)) {
    $itemLocation[$row_item['locationName']][] = $row_item['category'];
    $itemCategory[$row_item['category']][$row_item['locationName']][] = $row_item;
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />

        <title>inWare | USERS</title>

        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/tooltip.css" rel="stylesheet" type="text/css">
        <link href="../../css/print.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/tooltip.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {

                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('.tooltip').tipTip({
                    delay: "300"
                });

                $('#pdf').click(function() {

                    savePDF('report', '../../css/print.css', 'inventory_reoder');
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php require '../../includes/header.php'; ?>
            <?php require '../../includes/sidebar.php'; ?>
            <div class="content">
                <?php
                // Displaying messages and errors
                include '../../includes/info.php';
                ?>
                <h1>Inventory Estimation</h1>
                <div class="hr-line"></div>
                <div class="actions" style="top: 100px; width: auto; right: 0; margin: 0 15px 0 0" >
                    <button class="print tooltip" accesskey="P" title="Print [Alt+Shift+P]" onClick="printPage('report', '../../css/print.css')">Print</button>
                    <button class="pdf tooltip" accesskey="D" title="Save as PDF [Alt+Shift+D]" id="pdf" >PDF</button>
                </div>
                <form action="../pdf/pdf.php" method="post" id="html-form" style="display: none">
                    <input type="hidden" name="html" id="html">
                    <input type="hidden" name="pdfName" id="pdf-name">
                </form>
                <div class="report-wrapper">
                    <div id="report">
                        <div class="sheet-wraper">

                            <?php
                            // Setting report tittle
                            $report_title = "INVENTORY ESTIMATION REPORT";
                            include '../../includes/report_header.php';
                            ?>

                            <div>
                                <table cellpadding="3" cellspacing="0" border="1" width="100%" class="two-groups">
                                    <tr>
                                        <th>ITEM CODE</th>
                                        <th>ITEM DESCRIPTION</th>
                                        <th>START DATE</th>
                                        <th style="text-align: right">QTY SOLD</th>
                                        <th style="text-align: right">QTY AVAILABLE</th>
                                        <th style="text-align: right">EST DURATION</th>
                                        <th style="text-align: right">EST OUT OF STOCK DATE</th>
                                    </tr>

                                    <?php
                                    $nthrow = 0;
                                    $MainTotalAvQnty = 0;
                                    $MainTotalWanQty = 0;
                                    $MainTotalReoQty = 0;
                                    $MainTotalCost = 0;

                                    foreach ($itemLocation as $key => $value) {

                                        $CatTotalAvQnty = 0;
                                        $CatTotalWanQty = 0;
                                        $CatTotalReoQty = 0;
                                        $CatTotalCost = 0;

                                        echo '<tr><td colspan="7" style="font-style: italic; color: #0ca696;">' . $key . '</td></tr>';

                                        foreach (array_unique($value) as $category) {

                                            $totalAvQnty = 0;
                                            $totalWanQty = 0;
                                            $totalReQty = 0;
                                            $totalCost = 0;

                                            echo '<tr><td colspan="7" style="font-style: italic">' . $category . '</td></tr>';

                                            foreach ($itemCategory[$category][$key] as $item) {
                                                echo '<tr  class="';
                                                if ($nthrow % 2 != 0)
                                                    echo 'odd';
                                                echo '" >';
                                                echo '<td>' . $item['itemCode'] . '</td>';
                                                echo '<td>' . $item['itemDescription'] . '</td>';
                                                echo '<td>' . $item['startDate'] . '</td>';
                                                echo '<td align="right">' . $AvQnty = $item['soldQuatity'] . '</td>';
                                                echo '<td align="right">' . $wanQty = $item['availableQuantity'] . '</td>';
                                                echo '<td align="right">' . number_format($ReoQty = $item['duration'], '1', '.', '') . '</td>';
                                                echo '<td align="right">' . $cost = $item['estDate'] . '</td>';
                                                echo '</tr>';

                                                $nthrow++;
                                                $totalAvQnty += $AvQnty;
                                                $totalWanQty += $wanQty;
                                                $totalReQty += $ReoQty;
                                                $totalCost += $cost;
                                            }
                                            ?>

                                            <tr class="no-border"><td colspan="7"  class="no-border" style="height: 3px;"></td></tr>
                                            <tr><td align="right" colspan="3">CATEGORY TOTALS</td>
                                                <td align="right"><strong><?php echo $totalAvQnty ?></strong> </td>
                                                <td align="right"><strong><?php echo $totalWanQty ?></strong> </td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>

                                            <?php
                                            $CatTotalAvQnty += $totalAvQnty;
                                            $CatTotalWanQty += $totalWanQty;
                                            $CatTotalReoQty += $totalReQty;
                                            $CatTotalCost += $totalCost;
                                        }
                                        ?>
                                        <tr class="no-border"><td colspan="7"  class="no-border" style="height: 3px;"></td></tr>
                                        <tr><td align="right" colspan="3">LOCATION TOTALS</td>
                                            <td align="right"><strong><?php echo $CatTotalAvQnty ?></strong> </td>
                                            <td align="right"><strong><?php echo $CatTotalWanQty ?></strong> </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <?php
                                        $MainTotalAvQnty += $CatTotalAvQnty;
                                        $MainTotalWanQty += $CatTotalWanQty;
                                        $MainTotalReoQty += $CatTotalReoQty;
                                        $MainTotalCost += $CatTotalCost;
                                    }
                                    ?>

                                    <tr><td colspan="7">&nbsp;</td></tr>
                                    <tr class="total-row">
                                        <td align="right" colspan="3"><strong>MAIN TOTALS</strong></td>
                                        <td align="right"><strong><?php echo $MainTotalAvQnty ?></strong> </td>
                                        <td align="right"><strong><?php echo $MainTotalWanQty ?></strong> </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>

                                <div style="clear: both;"></div>
                            </div>
                            <!-- end sheet-wrapper  --></div>
                        <!-- end #report --></div>
                    <!-- end .report-wrapper --></div>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
        $('.reports').attr("id", "current");
        var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

        ddaccordion.init({
            headerclass: "expandable", //Shared CSS class name of headers group that are expandable
            contentclass: "categoryitems", //Shared CSS class name of contents group
            revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
            mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
            collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
            defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
            onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
            animatedefault: true, //Should contents open by default be animated into view?
            persiststate: false, //persist state of opened contents within browser session?
            toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
            togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
            animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
            oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                //do nothing
            },
            onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                //do nothing
            }
        });
    </script>
</html>
<?php ob_flush(); ?>
