<?php
require '../../includes/session_validator.php';
require '../../config/config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="../../css/pop-up.css" rel="stylesheet" />

        <script type="text/javascript">
        $(document).ready(function (){
            $('.select').chosen();
        });
         </script>
    </head>
    <body>
        <div class="pop-up-wrapper">
            <div class="pop-up-contents">
                <div class="pop-up-header">
                    <div class="close"></div>
                    <h1>Customer Filter</h1>

                    <div style="clear: both"></div>
                </div>
                <form class="pop-up-form" id="category-form" action="customer_list.php" method="POST" enctype="multipart/form-data">
                    <table border="0" width="100%" cellpadding="5">
                        <tr>
                            <td width="200" style="vertical-align: top">Supplier Name:</td>
                            <td>
                                <select name="customerID[]" class="select" multiple="" required="" style="width: 400px; height: 200px;">
                                    <option value="All">All</option>
                                    <?php
                                    $query_customer = "SELECT `customerID`, `customerName`  FROM customer  ORDER BY `customerName` ASC";
                                    $result_customer = mysqli_query($link, $query_customer) or die(mysqli_error($link));
                                    while ($row_customer = mysqli_fetch_array($result_customer)) {
                                        ?>
                                        <option value="<?php echo $row_customer['customerID'] ?>"><?php echo $row_customer['customerName'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
                <div class="pop-up-footer">
                    <button type="reset" class="post" style="margin-right: 0" form="category-form">Cancel</button>
                    <button type="submit" class="post" form="category-form">Generate</button>
                    <div style="clear: both"></div>
                </div>
            </div>
        </div>
    </body>
</html>
