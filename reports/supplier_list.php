<?php
require '../../includes/session_validator.php';
require '../../functions/general_functions.php';
ob_start();
// Getting item data

require '../../config/config.php';

$supplierID = $_POST['supplierID'];

$filter = 'WHERE supplierID = ""';

switch ($supplierID[0]) {
    case 'All':
        $filter = '';

        break;

    default:
        
        for ($i = 0; $i < count($supplierID); $i++) {
            $filter .= " OR supplierID = '$supplierID[$i]'";
        }  
        break;
}

$query_sales = "SELECT `supplierID`, `supplierName`, `supplierContactPerson`,
                       `supplierAddress`, `supplierPhone`, `supplierFax`, `supplierEmail`
                  FROM supplier
                       {$filter} 
              ORDER BY supplierName ASC";

$result_sales = mysqli_query($link, $query_sales) or die(mysqli_error($link));

if (mysqli_num_rows($result_sales) < 1) {
    info('message', 'No Supplier(s) match this creteria!');
    header('Location: reports.php');
    exit;
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />

        <title>inWare | USERS</title>

        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/tooltip.css" rel="stylesheet" type="text/css">
        <link href="../../css/print.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/tooltip.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {

                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('.tooltip').tipTip({
                    delay: "300"
                });

                $('#pdf').click(function() {

                    savePDF('report', '../../css/print.css', 'supplier_list');
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php require '../../includes/header.php'; ?>
            <?php require '../../includes/sidebar.php'; ?>
            <div class="content">
                <?php
                // Displaying messages and errors
                include '../../includes/info.php';
                ?>
                <h1>Suppliers List Report</h1>
                <div class="hr-line"></div>
                <div class="actions" style="top: 100px; width: auto; right: 0; margin: 0 15px 0 0" >
                    <button class="print tooltip" accesskey="P" title="Print [Alt+Shift+P]" onClick="printPage('report', '../../css/print.css')">Print</button>
                    <button class="pdf tooltip" accesskey="D" title="Save as PDF [Alt+Shift+D]" id="pdf" >PDF</button>
                </div>
                <form action="../pdf/pdf.php" method="post" id="html-form" style="display: none">
                    <input type="hidden" name="html" id="html">
                    <input type="hidden" name="pdfName" id="pdf-name">
                </form>
                <div class="report-wrapper">
                    <div id="report">
                        <div class="sheet-wraper">

                            <?php
                            // Setting report tittle
                            $report_title = "SUPPLIERS LIST";
                            include '../../includes/report_header.php';
                            ?>

                            <div>
                                <table cellpadding="3" cellspacing="0" border="1" width="100%" class="two-groups">
                                    <tr>
                                        <th>NAME</th>
                                        <th>CONTACT PERSON</th>
                                        <th>PHONE</th>
                                        <th>FAX</th>
                                        <th>EMAIL</th>
                                        <th>ADDRESS</th>
                                    </tr>

                                    <?php
                                    $nthrow = 0;

                                    while ($item = mysqli_fetch_array($result_sales)) {
                                        echo '<tr  class="';
                                        if ($nthrow % 2 != 0)
                                            echo 'odd';
                                        echo '" >';
                                        echo '<td>' . $item['supplierName'] . '</td>';
                                        echo '<td>' . $item['supplierContactPerson'] . '</td>';
                                        echo '<td>' . $item['supplierPhone'] . '</td>';
                                        echo '<td>' . $item['supplierFax'] . '</td>';
                                        echo '<td>' . $item['supplierEmail'] . '</td>';
                                        echo '<td>' . $item['supplierAddress'] . '</td>';
                                        echo '</tr>';
                                        
                                        $nthrow++;
                                    }
                                    ?>
                                    <tr class="no-border"><td colspan="10"  class="no-border" style="height: 15px;"></td>&nbsp;</tr>
                                    <tr class="total-row">
                                        <td colspan="6" align="right"><strong>TOTAL SUPPLIERS <?php echo $nthrow; ?></strong></td>
                                    </tr>
                                </table>

                                <div style="clear: both;"></div>
                            </div>
                            <!-- end sheet-wrapper  --></div>
                        <!-- end #report --></div>
                    <!-- end .report-wrapper --></div>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
            $('.reports').attr("id", "current");
            var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: true, //Should contents open by default be animated into view?
                persiststate: false, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            });
    </script>
</html>
<?php ob_flush(); ?>
