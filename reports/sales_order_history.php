<?php
require '../../includes/session_validator.php';
require '../../functions/general_functions.php';
ob_start();
// Getting item data

require '../../config/config.php';

$dateRange = clean($_POST['dateRange']);
$customerID = $_POST['customerID'];
$itemLocationID = $_POST['itemLocation'];
$itemCategoryID = $_POST['itemCategory'];
$invoiceStatus = $_POST['invoiceStatus'];
$dateFrom = clean($_POST['dateFrom']);
$dateTo = clean($_POST['dateTo']);
$thisMonth = date('Ym');

switch ($customerID) {
    case 'All':
        $filter = '';

        break;

    case '':
        $filter = '';
        break;

    default:
        $filter = "WHERE u.customerID = '$customerID'";
        break;
}

switch ($itemLocationID) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';

    default:
        $filter .= " AND l.locationID = '$itemLocationID'";
        break;
}

switch ($itemCategoryID) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';

    default:
        $filter .= " AND c.itemCategoryID = '$itemCategoryID'";
        break;
}

switch ($invoiceStatus) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';

    default:
        $filter .= " AND invoiceStatus = '$invoiceStatus'";
        break;
}

switch ($dateRange) {
    case 'custom':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) BETWEEN '$dateFrom' AND '$dateTo'";
        break;
    
    case 'Today':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) = CURRENT_DATE()";
        break;
    
    case 'This week':
        $filter .= " AND IFNULL(WEEKOFYEAR(receiptDate), WEEKOFYEAR(invoiceDate)) = WEEKOFYEAR(CURRENT_DATE())";
        break;

    case 'This month':
        $filter .= " AND IFNULL(EXTRACT(YEAR_MONTH FROM receiptDate), EXTRACT(YEAR_MONTH FROM invoiceDate)) = '$thisMonth'";
        break;

    case 'This quarter':
        $filter .= " AND IFNULL(QUARTER(receiptDate), QUARTER(invoiceDate)) = QUARTER(CURRENT_DATE())";
        break;
    
    case 'This year':
        $filter .= " AND IFNULL(YEAR(receiptDate), YEAR(invoiceDate)) = YEAR(CURRENT_DATE())";
        break;
    
     case 'Yesterday':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) = SUBDATE(CURRENT_DATE(), 1)";
        break;
    
     case 'Last week':
        $filter .= " AND IFNULL(WEEKOFYEAR(receiptDate), WEEKOFYEAR(invoiceDate)) = WEEKOFYEAR(CURRENT_DATE())-1";
        break;
    
     case 'Last month':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND LAST_DAY(CURRENT_DATE() - INTERVAL 1 MONTH)";
        break;
    
    case 'Last quarter':
        $filter .= " AND IFNULL(QUARTER(receiptDate), QUARTER(invoiceDate)) = QUARTER(CURRENT_DATE)-1";
        break;
    
    case 'Last year':
        $filter .= " AND IFNULL(YEAR(receiptDate), YEAR(invoiceDate)) = YEAR(CURRENT_DATE()) - 1";
        break;
    
    case 'Last 7 days':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) BETWEEN SUBDATE(CURRENT_DATE(), 7) AND CURRENT_DATE()";
        break;
    
    case 'Last 30 days':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) BETWEEN SUBDATE(CURRENT_DATE(), 30) AND CURRENT_DATE()";
        break;
    case 'Last 90 days':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) BETWEEN SUBDATE(CURRENT_DATE(), 90) AND CURRENT_DATE()";
        break;
    case 'Last 365 days':
        $filter .= " AND IFNULL(DATE(receiptDate), invoiceDate) BETWEEN SUBDATE(CURRENT_DATE(), 365) AND CURRENT_DATE()";
        break;

    case 'All':
    case '':
    default:
        $filter .= '';
        break;
}

strpos($filter, 'WHERE') > 0 ? : $filter = preg_replace('/AND/', 'WHERE', $filter, 1);

$query_sales = "SELECT i.`invoiceID`, u.`customerID`, `customerName`,
                       t.`itemCode`, `itemDescription`, l.`locationID`,
                       `locationName`, c.`itemCategoryID`, category,
                       `invoiceStatus`, DATE_FORMAT(`invoiceDate`,'%d %b, %Y') AS invoiceDate,
                       DATE_FORMAT(`receiptDate`,'%d %b, %Y') AS receiptDate,
                       receiptDate, SUM(m.`itemQuantity`) AS soldQuantity,
                       SUM(m.`itemQuantity` * m.`itemPrice`) AS subTotal
                  FROM invoiceitem m
            INNER JOIN itemlocation l
                    ON l.`itemLocationID` = m.`itemLocationID`
            INNER JOIN location o
                    ON o.`locationID` = l.`locationID`
            INNER JOIN item t
                    ON t.`itemID` = l.`itemID`
            INNER JOIN itemcategory c
                    on c.`itemCategoryID` = t.`itemCategoryID`
            INNER JOIN invoice i
                    ON i.`invoiceID` = m.`invoiceID`
            INNER JOIN customer u
                    ON u.`customerID` = i.`customerID`
             LEFT JOIN receipt r
                    ON r.`invoiceID` = i.`invoiceID`
                       {$filter} 
              GROUP BY invoiceID, itemCode
              ORDER BY invoiceDate DESC, customerName, itemDescription ASC ";

$result_sales = mysqli_query($link, $query_sales) or die(mysqli_error($link));

if (mysqli_num_rows($result_sales) < 1) {
    info('message', 'No item(s) match this creteria!');
    header('Location: reports.php');
    exit;
}

while ($row_item = mysqli_fetch_array($result_sales)) {
    $itemLocation[$row_item['customerName']][] = $row_item['invoiceID'];
    $itemCategory[$row_item['invoiceID']][$row_item['customerName']][] = $row_item;
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />

        <title>inWare | USERS</title>

        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/tooltip.css" rel="stylesheet" type="text/css">
        <link href="../../css/print.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/tooltip.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {

                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('.tooltip').tipTip({
                    delay: "300"
                });

                $('#pdf').click(function() {

                    savePDF('report', '../../css/print.css', 'sales_order_history');
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php require '../../includes/header.php'; ?>
            <?php require '../../includes/sidebar.php'; ?>
            <div class="content">
                <?php
                // Displaying messages and errors
                include '../../includes/info.php';
                ?>
                <h1>Sales Order History</h1>
                <div class="hr-line"></div>
                <div class="actions" style="top: 100px; width: auto; right: 0; margin: 0 15px 0 0" >
                    <button class="print tooltip" accesskey="P" title="Print [Alt+Shift+P]" onClick="printPage('report', '../../css/print.css')">Print</button>
                    <button class="pdf tooltip" accesskey="D" title="Save as PDF [Alt+Shift+D]" id="pdf" >PDF</button>
                </div>
                <form action="../pdf/pdf.php" method="post" id="html-form" style="display: none">
                    <input type="hidden" name="html" id="html">
                    <input type="hidden" name="pdfName" id="pdf-name">
                </form>
                <div class="report-wrapper">
                    <div id="report">
                        <div class="sheet-wraper">

                            <?php
                            // Setting report tittle
                            $report_title = "SALES ORDER HISTORY";
                            include '../../includes/report_header.php';
                            ?>

                            <div>
                                <table cellpadding="3" cellspacing="0" border="1" width="100%" class="two-groups">
                                    <tr>
                                        <th>ITEM CODE</th>
                                        <th>ITEM DESCRIPTION</th>
                                        <th>CATEGORY</th>
                                        <th>LOCATION</th>
                                        <th>ORDER DATE</th>
                                        <th>STATUS</th>
                                        <th style="text-align: right">QUANTITY</th>
                                        <th style="text-align: right">AMOUNT</th>
                                    </tr>

                                    <?php
                                    $nthrow = 0;
                                    $MainTotalSoldQnty = 0;
                                    $MainTotalSales = 0;

                                    foreach ($itemLocation as $key => $value) {

                                        $CatTotalSoldQnty = 0;
                                        $CatTotalSales = 0;

                                        echo '<tr><td colspan="8" style="font-style: italic; color: #0ca696;">' . $key . '</td></tr>';

                                        foreach (array_unique($value) as $invoiceID) {

                                            $totalSoldQnty = 0;
                                            $subTotal = 0;

                                            echo '<tr><td colspan="8" style="font-style: italic">' . $invoiceID . '</td></tr>';

                                            foreach ($itemCategory[$invoiceID][$key] as $item) {
                                                echo '<tr  class="';
                                                if ($nthrow % 2 != 0)
                                                    echo 'odd';
                                                echo '" >';
                                                echo '<td>' . $item['itemCode'] . '</td>';
                                                echo '<td>' . $item['itemDescription'] . '</td>';
                                                echo '<td>' . $item['category'] . '</td>';
                                                echo '<td>' . $item['locationName'] . '</td>';
                                                echo '<td>' . $item['invoiceDate'] . '</td>';
                                                echo '<td>' . $item['invoiceStatus'] . '</td>';
                                                echo '<td align="right">' . $SoldQnty = $item['soldQuantity'] . '</td>';
                                                echo '<td align="right">' . number_format($sales = $item['subTotal'], '2', '.', ',') . '</td>';
                                                echo '</tr>';

                                                $nthrow++;
                                                $totalSoldQnty += $SoldQnty;
                                                $subTotal += $sales;
                                            }
                                            ?>

                                            <tr class="no-border"><td colspan="8"  class="no-border" style="height: 3px;"></td></tr>
                                            <tr><td  align="right" colspan="6">SALES TOTALS</td>
                                                <td align="right"><strong><?php echo $totalSoldQnty ?></strong> </td>
                                                <td align="right"><strong><?php echo number_format($subTotal, '2', '.', ',') ?></strong> </td>
                                            </tr>

                                            <?php
                                            $CatTotalSoldQnty += $totalSoldQnty;
                                            $CatTotalSales += $subTotal;
                                        }
                                        ?>

                                        <tr class="no-border"><td colspan="8"  class="no-border" style="height: 3px;"></td></tr>
                                        <tr><td  align="right" colspan="6">CUSTOMER TOTALS</td>
                                            <td align="right"><strong><?php echo $CatTotalSoldQnty ?></strong> </td>
                                            <td align="right"><strong><?php echo number_format($CatTotalSales, '2', '.', ',') ?></strong> </td>
                                        </tr>

                                        <?php
                                        $MainTotalSoldQnty += $CatTotalSoldQnty;
                                        $MainTotalSales += $CatTotalSales;
                                    }
                                    ?>

                                    <tr><td colspan="8">&nbsp;</td></tr>
                                    <tr class="total-row">
                                        <td align="right" colspan="6"><strong>MAIN TOTALS</strong></td>
                                        <td align="right"><strong><?php echo $MainTotalSoldQnty ?></strong> </td>
                                        <td align="right"><strong><?php echo number_format($MainTotalSales, '2', '.', ',') ?></strong> </td>
                                    </tr>
                                </table>

                                <div style="clear: both;"></div>
                            </div>
                            <!-- end sheet-wrapper  --></div>
                        <!-- end #report --></div>
                    <!-- end .report-wrapper --></div>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
            $('.reports').attr("id", "current");
            var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: true, //Should contents open by default be animated into view?
                persiststate: false, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            });
    </script>
</html>
<?php ob_flush(); ?>
