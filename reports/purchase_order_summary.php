<?php
require '../../includes/session_validator.php';
require '../../functions/general_functions.php';
ob_start();
// Getting item data

require '../../config/config.php';

$dateRange = clean($_POST['dateRange']);
$supplierID = $_POST['supplierID'];
$purchaseStatus = $_POST['purchaseStatus'];
$dateFrom = clean($_POST['dateFrom']);
$dateTo = clean($_POST['dateTo']);
$thisMonth = date('Ym');

switch ($supplierID) {
    case 'All':
        $filter = '';

        break;

    case '':
        $filter = '';
        break;

    default:
        $filter = "WHERE s.supplierID = '$supplierID'";
        break;
}

switch ($purchaseStatus) {
    case 'All':
        $filter .= '';

        break;

    case '':
        $filter .= '';

    default:
        $filter .= " AND purchaseStatus = '$purchaseStatus'";
        break;
}

switch ($dateRange) {
    case 'custom':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) BETWEEN '$dateFrom' AND '$dateTo'";
        break;

    case 'Today':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) = CURRENT_DATE()";
        break;

    case 'This week':
        $filter .= " AND IFNULL(WEEKOFYEAR(receivedDate), WEEKOFYEAR(purchaseDate)) = WEEKOFYEAR(CURRENT_DATE())";
        break;

    case 'This month':
        $filter .= " AND IFNULL(EXTRACT(YEAR_MONTH FROM receivedDate), EXTRACT(YEAR_MONTH FROM purchaseDate)) = '$thisMonth'";
        break;

    case 'This quarter':
        $filter .= " AND IFNULL(QUARTER(receivedDate), QUARTER(purchaseDate)) = QUARTER(CURRENT_DATE())";
        break;

    case 'This year':
        $filter .= " AND IFNULL(YEAR(receivedDate), YEAR(purchaseDate)) = YEAR(CURRENT_DATE())";
        break;

    case 'Yesterday':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) = SUBDATE(CURRENT_DATE(), 1)";
        break;

    case 'Last week':
        $filter .= " AND IFNULL(WEEKOFYEAR(receivedDate), WEEKOFYEAR(purchaseDate)) = WEEKOFYEAR(CURRENT_DATE())-1";
        break;

    case 'Last month':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01') AND LAST_DAY(CURRENT_DATE() - INTERVAL 1 MONTH)";
        break;

    case 'Last quarter':
        $filter .= " AND IFNULL(QUARTER(receivedDate), QUARTER(purchaseDate)) = QUARTER(CURRENT_DATE)-1";
        break;

    case 'Last year':
        $filter .= " AND IFNULL(YEAR(receivedDate), YEAR(purchaseDate)) = YEAR(CURRENT_DATE()) - 1";
        break;

    case 'Last 7 days':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) BETWEEN SUBDATE(CURRENT_DATE(), 7) AND CURRENT_DATE()";
        break;

    case 'Last 30 days':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) BETWEEN SUBDATE(CURRENT_DATE(), 30) AND CURRENT_DATE()";
        break;
    case 'Last 90 days':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) BETWEEN SUBDATE(CURRENT_DATE(), 90) AND CURRENT_DATE()";
        break;
    case 'Last 365 days':
        $filter .= " AND IFNULL(DATE(receivedDate), purchaseDate) BETWEEN SUBDATE(CURRENT_DATE(), 365) AND CURRENT_DATE()";
        break;

    case 'All':
    case '':
    default:
        $filter .= '';
        break;
}

strpos($filter, 'WHERE') > 0 ? : $filter = preg_replace('/AND/', 'WHERE', $filter, 1);

$query_sales = "SELECT p.`purchaseOrderID`, s.`supplierID`, `supplierName`,
                       `purchaseStatus`, DATE_FORMAT(`purchaseDate`,'%d %b, %Y') AS purchaseDate,
                       DATE_FORMAT(`receivedDate`, '%d %b, %Y @ %H:%i:%s') AS receivedDate,
                       SUM(o.`itemQuantity` * o.`itemCost`) AS subTotal, `amountPayed`
                  FROM purchaseorderitem o
            INNER JOIN purchaseorder p
                    ON p.`purchaseOrderID` = o.`purchaseOrderID`
            INNER JOIN itemlocation l
                    On l.`itemLocationID` = o.`itemLocationID`
            INNER JOIN item t
                    ON t.`itemID` = l.`itemID`
            INNER JOIN supplier s
                    ON s.`supplierID` = p.`supplierID`
             LEFT JOIN purchaseorderreceived r
                    ON r.`purchaseOrderID` = p.`purchaseOrderID`
                       {$filter} 
              GROUP BY `purchaseOrderID`
              ORDER BY itemDescription ASC";

$result_sales = mysqli_query($link, $query_sales) or die(mysqli_error($link));

if (mysqli_num_rows($result_sales) < 1) {
    info('message', 'No Purchase order(s) match this creteria!');
    header('Location: reports.php');
    exit;
}

while ($row_item = mysqli_fetch_array($result_sales)) {
    $itemLocation[$row_item['supplierName']][] = $row_item['purchaseStatus'];
    $itemCategory[$row_item['purchaseStatus']][$row_item['supplierName']][] = $row_item;
}
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />

        <title>inWare | USERS</title>

        <link href="../../css/layout.css" rel="stylesheet" type="text/css">
        <link href="../../css/tooltip.css" rel="stylesheet" type="text/css">
        <link href="../../css/print.css" rel="stylesheet" type="text/css">

        <script src="../../js/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="../../js/tooltip.js" type="text/javascript"></script>
        <script src="../../js/core.js" type="text/javascript"></script>
        <script src="../../js/popup.js" type="text/javascript"></script>
        <script src="../../js/accordion.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function() {

                $('.message, .error').hide().slideDown('normal').click(function() {
                    $(this).slideUp('normal');
                });

                $('.tooltip').tipTip({
                    delay: "300"
                });

                $('#pdf').click(function() {

                    savePDF('report', '../../css/print.css', 'purchase_order_summary');
                });
            });
        </script>
    </head>

    <body>
        <div class="container">
            <div id="pop-up"></div>
            <?php require '../../includes/header.php'; ?>
            <?php require '../../includes/sidebar.php'; ?>
            <div class="content">
                <?php
                // Displaying messages and errors
                include '../../includes/info.php';
                ?>
                <h1>Sales Order Summary</h1>
                <div class="hr-line"></div>
                <div class="actions" style="top: 100px; width: auto; right: 0; margin: 0 15px 0 0" >
                    <button class="print tooltip" accesskey="P" title="Print [Alt+Shift+P]" onClick="printPage('report', '../../css/print.css')">Print</button>
                    <button class="pdf tooltip" accesskey="D" title="Save as PDF [Alt+Shift+D]" id="pdf" >PDF</button>
                </div>
                <form action="../pdf/pdf.php" method="post" id="html-form" style="display: none">
                    <input type="hidden" name="html" id="html">
                    <input type="hidden" name="pdfName" id="pdf-name">
                </form>
                <div class="report-wrapper">
                    <div id="report">
                        <div class="sheet-wraper">

                            <?php
                            // Setting report tittle
                            $report_title = "PURCHASE ORDER SUMMARY";
                            include '../../includes/report_header.php';
                            ?>

                            <div>
                                <table cellpadding="3" cellspacing="0" border="1" width="100%" class="two-groups">
                                    <tr>
                                        <th>PURCHASE ORDER #</th>
                                        <th>ORDER DATE</th>
                                        <th>DATE RECEIVED</th>
                                        <th style="text-align: right">ORDER TOTAL</th>
                                        <th style="text-align: right">AMOUNT PAID</th>
                                    </tr>

                                    <?php
                                    $nthrow = 0;
                                    $MainTotalCost = 0;
                                    $MainTotalPaid = 0;

                                    foreach ($itemLocation as $key => $value) {

                                        $PurchaseTotalCost = 0;
                                        $PurchaseTotalPaid = 0;

                                        echo '<tr><td colspan="5" style="font-style: italic; color: #0ca696;">' . $key . '</td></tr>';

                                        foreach (array_unique($value) as $purchaseStatus) {

                                            $$totalCost = 0;
                                            $totalPaid = 0;

                                            echo '<tr><td colspan="5" style="font-style: italic">' . $purchaseStatus . '</td></tr>';

                                            foreach ($itemCategory[$purchaseStatus][$key] as $item) {
                                                echo '<tr  class="';
                                                if ($nthrow % 2 != 0)
                                                    echo 'odd';
                                                echo '" >';
                                                echo '<td>' . $item['purchaseOrderID'] . '</td>';
                                                echo '<td>' . $item['purchaseDate'] . '</td>';
                                                echo '<td>' . $item['receivedDate'] . '</td>';
                                                echo '<td align="right">' . number_format($cost = $item['subTotal'], '2', '.', ',') . '</td>';
                                                echo '<td align="right">' . number_format($paid = $item['amountPayed'], '2', '.', ',') . '</td>';
                                                echo '</tr>';

                                                $nthrow++;
                                                $$totalCost += $cost;
                                                $totalPaid += $paid;
                                            }
                                            ?>

                                            <tr class="no-border"><td colspan="5"  class="no-border" style="height: 3px;"></td></tr>
                                            <tr><td  align="right" colspan="3">STATUS TOTALS</td>
                                                <td align="right"><strong><?php echo number_format($$totalCost, '2', '.', ',') ?></strong> </td>
                                                <td align="right"><strong><?php echo number_format($totalPaid, '2', '.', ',') ?></strong> </td>
                                            </tr>

                                            <?php
                                            $PurchaseTotalCost += $$totalCost;
                                            $PurchaseTotalPaid += $totalPaid;
                                        }
                                        ?>

                                        <tr class="no-border"><td colspan="5"  class="no-border" style="height: 3px;"></td></tr>
                                        <tr><td  align="right" colspan="3">SUPPLIER TOTALS</td>
                                            <td align="right"><strong><?php echo number_format($PurchaseTotalCost, '2', '.', ',') ?></strong> </td>
                                            <td align="right"><strong><?php echo number_format($PurchaseTotalPaid, '2', '.', ',') ?></strong> </td>
                                        </tr>

                                        <?php
                                        $MainTotalCost += $PurchaseTotalCost;
                                        $MainTotalPaid += $PurchaseTotalPaid;
                                    }
                                    ?>

                                    <tr><td colspan="5">&nbsp;</td></tr>
                                    <tr class="total-row">
                                        <td align="right" colspan="3"><strong>MAIN TOTALS</strong></td>
                                        <td align="right"><strong><?php echo number_format($MainTotalCost, '2', '.', ',') ?></strong> </td>
                                        <td align="right"><strong><?php echo number_format($MainTotalPaid, '2', '.', ',') ?></strong> </td>
                                    </tr>
                                </table>

                                <div style="clear: both;"></div>
                            </div>
                            <!-- end sheet-wrapper  --></div>
                        <!-- end #report --></div>
                    <!-- end .report-wrapper --></div>
                <!-- end .content --></div>
            <?php include '../../includes/footer.php'; ?>
            <!-- end .container --></div>
    </body>
    <script type="text/javascript">
            $('.reports').attr("id", "current");
            var i = $('h3#current').index('.menuheader') - 1; // Find zero indexed position of class menubar containing id current

            ddaccordion.init({
                headerclass: "expandable", //Shared CSS class name of headers group that are expandable
                contentclass: "categoryitems", //Shared CSS class name of contents group
                revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
                mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
                collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
                defaultexpanded: [i], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
                onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
                animatedefault: true, //Should contents open by default be animated into view?
                persiststate: false, //persist state of opened contents within browser session?
                toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
                togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
                animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
                oninit: function(headers, expandedindices) { //custom code to run when headers have initalized
                    //do nothing
                },
                onopenclose: function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
                    //do nothing
                }
            });
    </script>
</html>
<?php ob_flush(); ?>
