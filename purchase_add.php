<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.customer.php');
require_once('classes/class.purchase.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


?>
<?php include('header.php');
/*
* load customer id and name with full information 
*/
$customer_list=new Ds_Customer();
$result=$customer_list->all_list_customer();

 ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PURCHASEADD;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST">

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Customer</label>
  <div class="col-md-4">
    <select id="selectbasic" name="pcid" class="form-control">
    <?php
    foreach($result as $obj)
    {
     echo " <option value=".$obj['cid'].">".$obj['name']."</option>";
//      <option value="2">Option two</option>
    }
    ?>
    </select>
  </div>
</div>

<!-- Text input-->

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="broker">Broker</label>
  <div class="col-md-4">
    <select id="broker" name="broker" class="form-control" onchange="getbroker(this.value)">
<option value="0">NONE</option>
     <?php
    foreach($result as $obj)
    {
     echo " <option value=".$obj['cid'].">".$obj['name']."</option>";
    }
    ?>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group" id="b1">
  <label class="col-md-4 control-label" for="brokeragetype">Brokerage Type</label>
  <div class="col-md-4">
    <select id="brokeragetype" name="brokeragetype" class="form-control">
      <option value="PERCENTAGE">PERCENTAGE</option>
      <option value="FIXED">FIXED</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group" id="b2">
  <label class="col-md-4 control-label" for="brokeragevalue">Brokerage Value</label>  
  <div class="col-md-4">
  <input id="brokeragevalue" name="brokeragevalue" placeholder="" class="form-control input-md"  type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group" id="b3">
  <label class="col-md-4 control-label" for="brokerageamount">Brokerage Amount</label>  
  <div class="col-md-4">
  <input id="brokerageamount" name="brokerageamount" placeholder="" class="form-control input-md" type="text">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">NO Of Days</label>  
  <div class="col-md-4">
  <input id="no_of_days" name="no_of_days" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rate">Rate</label>  
  <div class="col-md-4">
  <input id="rate" name="rate" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="weight">Weight</label>  
  <div class="col-md-4">
  <input id="weight" name="weight" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">

   <label class="col-md-4 control-label" for="weight">Weight type</label>  
  <div class="col-md-4">
  <input id="weighttype" name="weighttype" placeholder="" readonly="readonly" class="form-control input-md" value="carat" required="" type="text">
    
  </div>
</div>



<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="type">Type</label>
  <div class="col-md-4">
    <select id="type" name="type" class="form-control">
      <option value="palcha">palcha</option>
      <option value="rough">rough</option>
      <option value="taiyar">taiyar</option>
      <option value="direct">direct</option>
    </select>
  </div>
</div>




<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="add" name="add" class="btn btn-default" value="add">
  </div>
</div>




</form>
            
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();
</script>

<?php

if(isset($_REQUEST["add"]))
{
 $data = $database->clean_data($_POST);
    $purchase = new Ds_purchase();
    $purchase->set_pcid($data['pcid']);
    $purchase->set_broker($data['broker']);    
    $purchase->set_brokeragetype($data['brokeragetype']);
    echo "Brokerage Result is ".$data['brokeragetype'];
    $purchase->set_brokeragevalue($data['brokeragevalue']);
    $purchase->set_brokerageamount($data['brokerageamount']);
    $purchase->set_pdate($data['pdate']);    
    $purchase->set_no_of_days($data['no_of_days']);
    $purchase->set_rate($data['rate']);
    $purchase->set_weight($data['weight']);
    $purchase->set_weighttype($data['weighttype']);    
    $purchase->set_type($data['type']);
    $status=1;
    $purchase->set_status($status);  
    $add_purchase=$purchase->add_purchase();  
    
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "bottom",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Purcahse transaction Saved Successfully');
}, 1000);





   </script>
   <?php     
     
}   

?>
<script type="text/javascript">
$("#b1").hide();
$("#b2").hide();
$("#b3").hide();
function getbroker(val)
{
   if(val!=0)
   {
$("#b1").show();
$("#b2").show();
$("#b3").show();
   }
   else
   {
$("#b1").hide();
$("#b2").hide();
$("#b3").hide();
   }
}
</script>

