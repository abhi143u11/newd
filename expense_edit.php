<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.expense.php');

if(!isset($_SESSION['user']))
{
header('Location:login.php');

}


$id= $_GET['id'];
$expense = new Ds_Expense($id);

?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo EXPENSEEDIT; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <div class="container">
<div class="row">
<div class="col-md-12">
                
     
<form class="form-horizontal" method="POST">

<input type="hidden" name="id" value="<?php echo $expense->get_id();  ?>">
<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" placeholder="" class="form-control input-md" required="" value="<?php echo date("m/d/Y",$expense->get_created_on()); ?>" type="text">
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Expense Description</label>  
  <div class="col-md-4">
  <input id="description" name="description" placeholder="" value="<?php echo $expense->get_remark(); ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Amount</label>  
  <div class="col-md-4">
  <input id="amonut" name="amonut" placeholder="" class="form-control input-md" required="" value="<?php echo $expense->get_amount_paid(); ?>" type="text">
    
  </div>
</div>




<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="UPDATE">
  </div>
</div>

</form>
                
</div>
</div>
</div>
</div>
</div>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();
</script>
<?php

echo "Value is ".$expense->get_remark();

if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);


    $expense = new Ds_Expense();
    //set all the vars
    $expense->set_id($data['id']);
    $expense->set_remark($data['description']);
    $expense->set_amount_paid($data['amonut']);
    $expense->set_created_on($data['pdate']);
    
 
    $update_expense=$expense->update_expense();  
    
    if($add_customer=TRUE){
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Expense Updated Successfully');
}, 1000);
window.location='expense_list.php';
   </script>
   <?php     
     
    }}   

?>


