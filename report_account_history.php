<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.customer.php');
require_once('classes/class.payment.php');


if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

$start=0;
$second=0;
if(isset($_GET['f']))
{
$start=$_GET['f'];    
$second=$_GET['s'];    
}

/*
* customer class 
*/
$customer=new Ds_Customer();
/*
* get all data from payment list 
*/
$payment_list=new Ds_Payment();
$payment_record=$payment_list->list_of_all_payment_list_for_statement();
?>
<?php include('header.php'); ?>
        <section class="content">


         
        
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTCUSTOMACCOUNTHISTORY;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                
 <!--  start of datepicker  -->
                
 <div class="col-md-12"  >
              
                    <div class="form-group col-md-2">
                    
                     <div class="input-group">
                      <button class="btn btn-default pull-right" id="daterange-btn">
                        <i class="fa fa-calendar"></i> Date range 
                        <i class="fa fa-caret-down"></i>
                      </button>
                    </div>
                  
                    <form name="f1">
                     <input type="hidden" name="f" /><input type="hidden" name="s" />                  
                    
                   </div>   
         
              
                   
                   <div class="col-md-2">
                    <input type="submit" class="btn btn-primary" onclick="getdate()" type="button" name="loaddata" id="loaddata" value="Load"/>
                     
                   </div>
                        
               </form> 
 </div>                
 <!--  end of date picker -->               
                
                
                
                
                
                
                
                
                
                
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr>
<th>DATE</th>
<th>Customer Name</th>

<th>Account</th>
<th>Description</th>
<th>Dr.</th>
<th>Cr.</th>
<th>Balance</th>
</tr>
</thead>
<?php

/*
*mearge all array 
*/

/*
*  handled exception  
*/

$balance=0;
if(!empty($payment_record))
{    
foreach($payment_record as $obj)
{
$customer_name=$customer->get_customer_byid($obj['customerID']);
$customer_name=$customer_name[0];    
$account="";
$type="";    
if($obj['pay_type']=="P"){$account="Purchase ";$type="Expense";}
if($obj['pay_type']=="S"){$account="Sales ";$type="Income";}
if($obj['pay_type']=="E"){$account="Expense ";$type="Expense";}
echo "<tr>";    
echo "<td>".date("d/m/Y",$obj['created_on'])."</td>";
echo "<td>".$customer_name['name']."</td>";
echo "<td>".$account."</td>";
echo "<td>".$obj['remark']."</td>";

if($type=="Income"){echo "<td></td><td>".$obj['amount_paid']."</td>";}

if($type=="Expense"){echo "<td>".$obj['amount_paid']."</td><td></td>";}
echo "<td>".$balance."</td>";
if($type=="Income"){$balance=$balance+$obj['amount_paid'];}else{$balance=$balance-$obj['amount_paid'];}

echo "</tr>";

}    
} 
   
?>





</table>
</div>
</div>
</section>
<?php include('footer.php'); ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     

var startDate;
var endDate;
//Date range picker


        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            startDate = start;
            endDate = end
        
        }
        );

        
 function getdate()
 {
    // var first=startDate.format('YYYY-MM-DD');
     //var second=endDate.format('YYYY-MM-DD');
     var first=startDate.format('MM/DD/YYYY');
     var second=endDate.format('MM/DD/YYYY');
  document.f1.f.value=first;
  document.f1.s.value=second;
 // alert(first+" and "+second);
 }       
        
   </script>
      
  
  </body>
</html>



