<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$payment_list=new Ds_Payment();
$payment_result=$payment_list->list_of_all_payment_list_with_customername();


?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PAYMENTALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<table id="example2" class="table table-bordered table-striped">  
<thead>
<tr>
<th>Payment ID</th>
<th>Customer Name</th>
<th>Invoice ID</th>
<th>Payment Type</th>
<th>Date</th>
<th>Amount</th>
<th>Edit</th>
</tr>
</thead>
<?php

if(!empty($payment_result))
{       
foreach($payment_result as $obj)
{
$ptype="";
$background="";    
if($obj['pay_type']=="P")
{
$background="bg-orange disabled color-palette";
$ptype="PURCHASE";    
}
else
{
$ptype="SALES";    
$background="bg-red disabled color-palette";
}

    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$obj['invoiceID']."</td>";
    echo "<td class='".$background."'>".$ptype."</td>";
    echo "<td>".date("d/m/Y",$obj['created_on'])."</td>";
    echo "<td>".$obj['amount_paid']."</td>";
    echo "<td><a href='payment_edit.php?id=".$obj['id']."'> <i class=\"fa fa-edit\"></i></a></td>";
    echo "</tr>";
}
}    
    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
   
}
    ?>
  </body>
</html>




