<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



$id=0;
if(isset($_GET["pid"]))
{
$id=$_GET["pid"];    
}

$customer_data=new Ds_Customer($id);

$payment_list=new Ds_Payment();
$result=$payment_list->list_of_all_payment_list_of_on_customer($id);

?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTPAYMENTRONECUSTOMER."[ ".$customer_data->get_customer_name()." ]";    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">  
                  
                  <thead>
<tr><th>Payment NO</th><th>Name</th><th>Payment Date</th><th>Amount</th><th>Status</th><th>Remark</th><th>View</th></tr></thead>
<?php
if(!empty($result))
{       
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".$customer_data->get_customer_name()."</td>";
    echo "<td>".date("m/d/Y",$obj['created_on'])."</td>";
    echo "<td>".$obj['amount_paid']."</td>";
    echo "<td>".$obj['status']."</td>";
    echo "<td>".$obj['remark']."</td>";
    echo "<td><button>Print</button></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
  </body>
</html>




