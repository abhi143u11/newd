-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.32 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for diamond
CREATE DATABASE IF NOT EXISTS `diamond` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `diamond`;


-- Dumping structure for table diamond.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phoneno` varchar(12) DEFAULT NULL,
  `companyname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `cid` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.customer: ~6 rows (approximately)
DELETE FROM `customer`;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`cid`, `name`, `address`, `phoneno`, `companyname`) VALUES
	(10, 'Abhishek', 'Golwad', '123456', 'BANGLORE'),
	(20, 'Vicky', 'sd', '798978787', 'Abhishek'),
	(21, 'Abhi', 'a', 'a', 'New '),
	(22, 'Nilesh Bhai', 'VALSAD', '02635478954', 'BAGAD INDUSRTRY'),
	(23, 'BHAVESH BHAI', 'GOLVAD', '02634517584', 'BHAVESH KIRANA'),
	(24, 'ANKIT BHAI', 'SURAT', '02658796545', 'SELFINE KIRANA STORES');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;


-- Dumping structure for table diamond.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_type` enum('P','S') NOT NULL,
  `customerID` int(11) NOT NULL DEFAULT '0',
  `total_amount` float NOT NULL DEFAULT '0',
  `invoiceID` int(11) NOT NULL DEFAULT '0',
  `amount_paid` float NOT NULL DEFAULT '0',
  `payment_type` varchar(10) NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT '0',
  `use_customer_balance` tinyint(4) NOT NULL,
  `remark` text,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Dumping data for table diamond.payment: ~1 rows (approximately)
DELETE FROM `payment`;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`id`, `pay_type`, `customerID`, `total_amount`, `invoiceID`, `amount_paid`, `payment_type`, `status`, `use_customer_balance`, `remark`, `created_on`) VALUES
	(24, 'P', 23, 450, 6, 450, '1', 'CASH', 127, 'OK DONE', 1453896032);
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


-- Dumping structure for table diamond.process
CREATE TABLE IF NOT EXISTS `process` (
  `process_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(20) NOT NULL,
  `process_type` varchar(50) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `weight` float(10,2) NOT NULL,
  PRIMARY KEY (`process_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.process: ~0 rows (approximately)
DELETE FROM `process`;
/*!40000 ALTER TABLE `process` DISABLE KEYS */;
/*!40000 ALTER TABLE `process` ENABLE KEYS */;


-- Dumping structure for table diamond.process_type
CREATE TABLE IF NOT EXISTS `process_type` (
  `process_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`process_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.process_type: ~8 rows (approximately)
DELETE FROM `process_type`;
/*!40000 ALTER TABLE `process_type` DISABLE KEYS */;
INSERT INTO `process_type` (`process_type_id`, `process_type`) VALUES
	(1, 'rough'),
	(2, 'laser'),
	(3, 'ghatt'),
	(4, 'AGENT'),
	(5, 'polish'),
	(6, 'dalali'),
	(7, 'rent'),
	(8, 'majuri');
/*!40000 ALTER TABLE `process_type` ENABLE KEYS */;


-- Dumping structure for table diamond.purchase
CREATE TABLE IF NOT EXISTS `purchase` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pcid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `broker` int(30) NOT NULL,
  `brokeragetype` enum('PERCENTAGE','FIXED') NOT NULL,
  `brokeragevalue` int(11) NOT NULL,
  `brokerageamount` int(11) NOT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `pdate` int(11) DEFAULT NULL,
  `no_of_days` varchar(20) NOT NULL,
  `duedate` date DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.purchase: ~3 rows (approximately)
DELETE FROM `purchase`;
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
INSERT INTO `purchase` (`pid`, `pcid`, `name`, `broker`, `brokeragetype`, `brokeragevalue`, `brokerageamount`, `amount`, `pdate`, `no_of_days`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
	(6, 23, NULL, 20, 'PERCENTAGE', 10, 50, 450.00, 1453849200, '120', '2016-05-26', 100.00, 5.00, 'carat', 'palcha', 1),
	(7, 22, NULL, 21, 'PERCENTAGE', 10, 60, 540.00, 1453849200, '120', '2016-05-26', 200.00, 3.00, 'carat', 'palcha', 1),
	(8, 22, NULL, 24, 'PERCENTAGE', 10, 40, 360.00, 1453849200, '130', '2016-06-05', 100.00, 4.00, 'carat', 'palcha', 1);
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;


-- Dumping structure for table diamond.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(50) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `sell_id` int(11) NOT NULL,
  `p_from` date NOT NULL,
  `to_date` date NOT NULL,
  `total` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.reports: ~2 rows (approximately)
DELETE FROM `reports`;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` (`report_id`, `customer_name`, `purchase_id`, `sell_id`, `p_from`, `to_date`, `total`, `status`) VALUES
	(1, '', 0, 0, '0000-00-00', '0000-00-00', '', 0),
	(2, '1', 1, 0, '2016-01-06', '2016-01-06', '12000', 0);
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;


-- Dumping structure for table diamond.sales
CREATE TABLE IF NOT EXISTS `sales` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `scid` int(11) DEFAULT NULL,
  `pid` int(50) DEFAULT NULL,
  `broker` int(30) NOT NULL,
  `brokeragetype` enum('PERCENTAGE','FIXED') NOT NULL,
  `brokeragevalue` int(11) NOT NULL,
  `brokerageamount` int(11) NOT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `pdate` int(11) DEFAULT NULL,
  `no_of_days` varchar(20) NOT NULL,
  `duedate` date DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.sales: ~3 rows (approximately)
DELETE FROM `sales`;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` (`sid`, `scid`, `pid`, `broker`, `brokeragetype`, `brokeragevalue`, `brokerageamount`, `amount`, `pdate`, `no_of_days`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
	(6, 10, 6, 10, 'PERCENTAGE', 10, 75, 675.00, 1453849200, '120', '2016-05-26', 150.00, 5.00, 'carat', 'palcha', 1),
	(7, 21, 7, 21, 'PERCENTAGE', 10, 75, 675.00, 1453935600, '120', '0000-00-00', 250.00, 3.00, 'carat', 'palcha', 1),
	(8, 21, 8, 21, 'PERCENTAGE', 10, 40, 360.00, 1453849200, '120', '0000-00-00', 100.00, 4.00, 'carat', 'palcha', 1);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;


-- Dumping structure for table diamond.selling
CREATE TABLE IF NOT EXISTS `selling` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `scid` int(11) DEFAULT NULL,
  `seid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `selling_date` date DEFAULT NULL,
  `duedate` date NOT NULL,
  `no_of_days` varchar(20) NOT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.selling: ~2 rows (approximately)
DELETE FROM `selling`;
/*!40000 ALTER TABLE `selling` DISABLE KEYS */;
INSERT INTO `selling` (`sid`, `scid`, `seid`, `name`, `amount`, `selling_date`, `duedate`, `no_of_days`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
	(4, 1, 3, 'Abhishek', 2000.00, '2016-01-05', '2016-05-04', '120', 500.00, 4.00, 'carat', 'palcha', b'1'),
	(5, 1, 3, 'Abhii', 2100.00, '2016-01-05', '2016-03-05', '60', 525.00, 4.00, 'carat', 'palcha', b'0');
/*!40000 ALTER TABLE `selling` ENABLE KEYS */;


-- Dumping structure for table diamond.stock
CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` float(10,2) NOT NULL,
  `purchase_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.stock: ~0 rows (approximately)
DELETE FROM `stock`;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;


-- Dumping structure for table diamond.stock_new
CREATE TABLE IF NOT EXISTS `stock_new` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(20) NOT NULL,
  `pcid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `pdate` int(11) DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table diamond.stock_new: ~3 rows (approximately)
DELETE FROM `stock_new`;
/*!40000 ALTER TABLE `stock_new` DISABLE KEYS */;
INSERT INTO `stock_new` (`stock_id`, `purchase_id`, `pcid`, `name`, `amount`, `pdate`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
	(6, 6, 23, NULL, 450.00, 1453849200, '2016-05-26', 100.00, 0.00, 'carat', 'palcha', 0),
	(7, 7, 22, NULL, 540.00, 1453849200, '2016-05-26', 200.00, 0.00, 'carat', 'palcha', 0),
	(8, 8, 22, NULL, 360.00, 1453849200, '2016-06-05', 100.00, 0.00, 'carat', 'palcha', 0);
/*!40000 ALTER TABLE `stock_new` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
