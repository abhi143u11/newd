-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2016 at 02:16 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `diamond`
--
CREATE DATABASE IF NOT EXISTS `diamond` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `diamond`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `password_text` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`uid`, `user`, `password_text`, `password`) VALUES
(1, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phoneno` varchar(12) DEFAULT NULL,
  `companyname` varchar(50) DEFAULT NULL,
  `balance` int(11) NOT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `cid` (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cid`, `name`, `address`, `phoneno`, `companyname`, `balance`) VALUES
(31, 'VICKY', 'NAVSARI', '9786548978', 'PALCHA COMPANY', 1000),
(32, 'BHAVESH', 'CHIKHLI', '9875463587', 'BHAVESH KIRANA', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_type` enum('P','S','E') NOT NULL,
  `customerID` int(11) NOT NULL DEFAULT '0',
  `total_amount` float NOT NULL DEFAULT '0',
  `invoiceID` int(11) NOT NULL DEFAULT '0',
  `amount_paid` float NOT NULL DEFAULT '0',
  `payment_type` varchar(10) NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT '0',
  `use_customer_balance` int(11) NOT NULL,
  `remark` text,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `pay_type`, `customerID`, `total_amount`, `invoiceID`, `amount_paid`, `payment_type`, `status`, `use_customer_balance`, `remark`, `created_on`) VALUES
(22, 'P', 31, 1800, 4, 1000, '1', 'CASH', 0, 'OK DONE', 1454540400),
(23, 'S', 32, 1080, 15, 1000, '1', 'CASH', 0, 'OK DONE', 1454540400),
(24, 'P', 31, 800, 4, 500, '1', 'CASH', 0, 'OK DONE', 1454713200),
(25, 'P', 31, 300, 4, 300, '1', 'CASH', 0, 'OK DONE', 1454713200),
(26, 'P', 32, 1800, 5, 1000, '1', 'CASH', 0, 'OK DONE', 1454713200),
(27, 'P', 32, 800, 5, 200, '1', 'CASH', 0, 'OK DONE', 1454713200),
(28, 'P', 32, 600, 5, 600, '1', 'CASH', 0, 'OK DONE', 1454713200),
(29, 'P', 32, 1800, 6, 1500, '1', 'CASH', 0, 'OK DONE', 1454713200),
(30, 'S', 32, 80, 15, 80, '1', 'CASH', 0, 'OK DONE', 1454713200),
(33, 'E', 0, 0, 0, 2000, '0', '0', 0, 'Personal Expense', 1454886000),
(34, 'E', 0, 0, 0, 2000, '0', '0', 0, 'Buying a furniture best', 1454886000);

-- --------------------------------------------------------

--
-- Table structure for table `process`
--

CREATE TABLE IF NOT EXISTS `process` (
  `process_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(20) NOT NULL,
  `process_type` varchar(50) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `weight` float(10,2) NOT NULL,
  PRIMARY KEY (`process_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `process`
--

INSERT INTO `process` (`process_id`, `purchase_id`, `process_type`, `amount`, `weight`) VALUES
(2, 4, '1', 90.00, 18.00);

-- --------------------------------------------------------

--
-- Table structure for table `process_type`
--

CREATE TABLE IF NOT EXISTS `process_type` (
  `process_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `process_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`process_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `process_type`
--

INSERT INTO `process_type` (`process_type_id`, `process_type`) VALUES
(1, 'rough'),
(2, 'laser'),
(3, 'ghatt'),
(4, 'AGENT'),
(5, 'polish'),
(6, 'dalali'),
(7, 'rent'),
(8, 'majuri');

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE IF NOT EXISTS `purchase` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pcid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `broker` int(30) NOT NULL,
  `brokeragetype` enum('PERCENTAGE','FIXED') NOT NULL,
  `brokeragevalue` int(11) NOT NULL,
  `brokerageamount` int(11) NOT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `pdate` int(11) DEFAULT NULL,
  `no_of_days` varchar(20) NOT NULL,
  `duedate` date DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`pid`, `pcid`, `name`, `broker`, `brokeragetype`, `brokeragevalue`, `brokerageamount`, `amount`, `pdate`, `no_of_days`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
(4, 31, NULL, 32, 'PERCENTAGE', 10, 200, 1800.00, 1454540400, '120', '2016-06-03', 100.00, 20.00, 'carat', 'palcha', 1),
(5, 32, NULL, 31, 'PERCENTAGE', 10, 200, 1800.00, 1454626800, '120', '2016-06-04', 100.00, 20.00, 'carat', 'palcha', 1),
(6, 32, NULL, 31, 'PERCENTAGE', 10, 200, 1800.00, 1454626800, '120', '2016-06-04', 200.00, 10.00, 'carat', 'palcha', 1),
(7, 31, NULL, 31, 'PERCENTAGE', 10, 400, 3600.00, 1454626800, '120', '2016-06-04', 200.00, 20.00, 'carat', 'palcha', 1),
(8, 31, NULL, 0, 'PERCENTAGE', 0, 0, 40000.00, 1454886000, '120', '2016-06-07', 200.00, 200.00, 'carat', 'palcha', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(50) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `sell_id` int(11) NOT NULL,
  `p_from` date NOT NULL,
  `to_date` date NOT NULL,
  `total` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`report_id`, `customer_name`, `purchase_id`, `sell_id`, `p_from`, `to_date`, `total`, `status`) VALUES
(1, '', 0, 0, '0000-00-00', '0000-00-00', '', 0),
(2, '1', 1, 0, '2016-01-06', '2016-01-06', '12000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `scid` int(11) DEFAULT NULL,
  `pid` int(50) DEFAULT NULL,
  `broker` int(30) NOT NULL,
  `brokeragetype` enum('PERCENTAGE','FIXED') NOT NULL,
  `brokeragevalue` int(11) NOT NULL,
  `brokerageamount` int(11) NOT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `pdate` int(11) DEFAULT NULL,
  `no_of_days` varchar(20) NOT NULL,
  `duedate` date DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sid`, `scid`, `pid`, `broker`, `brokeragetype`, `brokeragevalue`, `brokerageamount`, `amount`, `pdate`, `no_of_days`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
(15, 32, 4, 31, 'PERCENTAGE', 10, 120, 1080.00, 1454540400, '120', '2016-06-03', 120.00, 10.00, 'carat', 'palcha', 1);

-- --------------------------------------------------------

--
-- Table structure for table `selling`
--

CREATE TABLE IF NOT EXISTS `selling` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `scid` int(11) DEFAULT NULL,
  `seid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `selling_date` date DEFAULT NULL,
  `duedate` date NOT NULL,
  `no_of_days` varchar(20) NOT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` float(10,2) NOT NULL,
  `purchase_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stock_new`
--

CREATE TABLE IF NOT EXISTS `stock_new` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(20) NOT NULL,
  `pcid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `pdate` int(11) DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `rate` float(10,2) DEFAULT NULL,
  `weight` float(10,2) DEFAULT NULL,
  `weighttype` enum('carat','cent') DEFAULT NULL,
  `type` enum('palcha','rough','taiyar','direct') DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `stock_new`
--

INSERT INTO `stock_new` (`stock_id`, `purchase_id`, `pcid`, `name`, `amount`, `pdate`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES
(16, 4, 31, NULL, 2070.00, 1454540400, '2016-06-03', 115.00, 18.00, 'carat', 'palcha', 1),
(17, 5, 32, NULL, 1800.00, 1454626800, '2016-06-04', 100.00, 20.00, 'carat', 'palcha', 1),
(18, 6, 32, NULL, 1800.00, 1454626800, '2016-06-04', 200.00, 10.00, 'carat', 'palcha', 1),
(19, 7, 31, NULL, 3600.00, 1454626800, '2016-06-04', 200.00, 20.00, 'carat', 'palcha', 1),
(20, 8, 31, NULL, 40000.00, 1454886000, '2016-06-07', 200.00, 200.00, 'carat', 'palcha', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
