<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.sales.php');
require_once('classes/class.payment.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



$sales_list=new Ds_Sales();
$sales_result=$sales_list->list_of_all_sales_with_name();

$payment_record=new Ds_Payment();

?>
<?php include('header.php'); ?>

  <section class="content">
<!-- 
Select a range 
     -->


                  

     
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTSALESRALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">  
                  
<thead>
<tr>
<th>Bill No</th>
<th>Customer Name</th>
<th>Date</th>
<th>Broker</th>
<th>Brokarage Type</th>
<th>Brokarage Value</th>
<th>Brokarage Amount</th>
<th>Weight Type</th>
<th>Type</th>
<th>Rate</th>
<th>Weight</th>
<th>Amount</th>
<th>Paid</th>
</tr></thead>
<?php
if(!empty($sales_result))
{    
foreach($sales_result as $obj)
{
$bval=$obj['broker'];    
$brokername=$sales_list->sales_broker_name($bval);
$sid=$obj['sid'];
$paidamount=$payment_record->sum_amount_of_sale_invoice($sid);    
    echo "<tr><td>".$obj['sid']."</td>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".date("d/m/Y",$obj['pdate'])."</td>";
    echo "<td>".$brokername."</td>";
    echo "<td>".$obj['brokeragetype']."</td>";
    echo "<td>".$obj['brokeragevalue']."</td>";
    echo "<td>".$obj['brokerageamount']."</td>";
    echo "<td>".$obj['weighttype']."</td>";
    echo "<td>".$obj['type']."</td>";
    echo "<td>".$obj['rate']."</td>";
    echo "<td>".$obj['weight']."</td>";
    echo "<td>".$obj['amount']."</td>";
    echo "<td>".$paidamount."</td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');


 ?>

 <script>
     
      $(function(){
          
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
   $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );   
 
    </script>
  </body>
</html>




