<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
header('Location:login.php');

}


$cid= $_GET['cid'];
 $customer = new Ds_Customer($cid);

?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo CUSTOMEREDIT; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <div class="container">
<div class="row">
<div class="col-md-12">
                
     
<form class="form-horizontal" method="POST">

<input type="hidden" name="cid" value="<?php echo $customer->get_customer_id();  ?>">
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Customer Name</label>  
  <div class="col-md-4">
  <input id="cname" name="cname" placeholder="Enter Customer name" class="form-control input-md" required="" type="text" value="<?php echo $customer->get_customer_name(); ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Address</label>  
  <div class="col-md-4">
  <input id="address" name="address" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $customer->get_address(); ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phonno">Phone</label>  
  <div class="col-md-4">
  <input id="phoneno" name="phoneno" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $customer->get_phoneno(); ?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="companyname">Company Name</label>  
  <div class="col-md-4">
  <input id="companyname" name="companyname" placeholder="" class="form-control input-md" required="" type="text" value="<?php echo $customer->get_companyname(); ?>">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="companyname">Customer Balance</label>  
  <div class="col-md-4">
  <input id="balance" value="<?php echo $customer->get_balance(); ?>" name="balance" placeholder="" class="form-control input-md" required="" type="text" >
    
  </div>
</div>



<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="UPDATE">
  </div>
</div>

</form>
                
</div>
</div>
</div>
</div>
</div>
</section>
<?php include('footer.php'); ?>

<?php

if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);

    $customer = new Ds_Customer();
    //set all the vars
    $customer->set_customer_id($data['cid']);
    $customer->set_customer_name($data['cname']);
    $customer->set_address($data['address']);
    $customer->set_phoneno($data['phoneno']);    
    $customer->set_companyname($data['companyname']);
    $customer->set_balance($data['balance']);
 
    $add_customer=$customer->update_customer();  
    if($add_customer=TRUE){
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Customer Updated Successfully');
}, 1000);
window.location='customer_show.php';
   </script>
   <?php     
     
    }}   

?>


