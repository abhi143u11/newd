<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.process.php');
require_once('classes/class.processtype.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$process_list=new Ds_Process();
$result=$process_list->list_of_process_data();

$processtype_list=new Ds_Processtype();
$type=$processtype_list->list_of_all_processtype();
//var_dump($result);
$ptype=array();
foreach($type as $val)
{
$ptype[$val['process_type_id']]=$val['process_type'];    
}





?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PROCESSALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<table id="example2" class="table table-bordered table-striped">
<thead>
<tr>
<th>ID</th>
<th>Type</th>
<th>Amount</th>
<th>Weight</th>
<th>Edit</th>
</tr>
</thead>
<?php
if(!empty($result))
{ 
foreach($result as $obj)
{
$id=$obj['process_type'];
$pro=$ptype[$id];

    
   echo "<tr>";
   echo "<td>".$obj['process_id']."</td>";
   echo "<td>".$pro."</td>";
   echo "<td>".$obj['amount']."</td>";
   echo "<td>".$obj['weight']."</td>";
   echo "<td><a href='process_edit.php?prid=".$obj['process_id']."'><i class=\"fa fa-edit\"></i></a></td>";
   echo "</tr>";  

}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
  
}
    ?>
  </body>
</html>



