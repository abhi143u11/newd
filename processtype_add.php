<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.processtype.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


?>
<?php include('header.php');
 ?>
 <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PROCESSTYPEADD;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST">

<!-- Select Basic -->
<div class="form-group">
 
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="weight">Process Type</label>  
  <div class="col-md-4">
  <input id="process_name" name="process_name" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>




<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="add" name="add" class="btn btn-default" value="add">
  </div>
</div>




</form>
            
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();
</script>

<?php

if(isset($_REQUEST["add"]))
{
 $data = $database->clean_data($_POST);
    $processadd = new Ds_Processtype();
    
    $processadd->set_process_type_name($data['process_name']);  
    
    
    $add_processtype=$processadd->add_processtype();  
    
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "bottom",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Process Type Saved Successfully');
}, 1000);
   </script>
   <?php     
     
}   

?>


