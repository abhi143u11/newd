<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.purchase.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
  echo "<script>window.location='login.php';</script>";
echo "Helloo..";
}



$cid=0;
$pid=0;

if(isset($_GET['cid']))
{
$cid=$_GET['cid'];
}

if(isset($_GET['pid']))
{
$pid=$_GET['pid'];;
}

$customer = new Ds_Customer($cid);
$result=$customer->all_list_customer();

$purchase = new Ds_Purchase($pid);

?>

<?php include('header.php'); ?>


 <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PURCHASEADD;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST">

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Customer</label>
  <div class="col-md-4">
    <input type="hidden" name="pid" value="<?php echo $purchase->get_pid(); ?>"/> 
    <select id="selectbasic" name="pcid" class="form-control">
    <?php
    foreach($result as $obj)
    {
        if($obj['cid']==$purchase->get_pcid())
        {
echo "<option selected='selected' value=".$obj['cid'].">".$obj['name']."</option>";            
        }   
     echo " <option value=".$obj['cid'].">".$obj['name']."</option>";
//      <option value="2">Option two</option>
    }
    ?>
    </select>
  </div>
</div>

<!-- Text input-->

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="broker">Broker</label>
  <div class="col-md-4">
    <select id="broker" name="broker" class="form-control" onchange="getbroker(this.value)">
    
    <?php
       if($purchase->get_broker()==0)
        {
        echo "<option selected='selected' value='0'>NONE</option>";            
        }
     foreach($result as $obj)
    {
        if($obj['cid']==$purchase->get_broker())
        {
        echo "<option selected='selected' value=".$obj['cid'].">".$obj['name']."</option>";            
        }
         
        echo " <option value=".$obj['cid'].">".$obj['name']."</option>";
//      <option value="2">Option two</option>
    }
    ?>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group" id="b1">
  <label class="col-md-4 control-label" for="brokeragetype">Brokerage Type</label>
  <div class="col-md-4">
    <select id="brokeragetype" name="brokeragetype" class="form-control" >
      <option value="<?php echo $purchase->get_brokeragetype();  ?>"><?php echo $purchase->get_brokeragetype();  ?></option>
      <option value="PERCENTAGE">PERCENTAGE</option>
      <option value="FIXED">FIXED</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group" id="b2">
  <label class="col-md-4 control-label" for="brokeragevalue">Brokerage Value</label>  
  <div class="col-md-4">
  <input id="brokeragevalue" name="brokeragevalue" placeholder="" value="<?php echo $purchase->get_brokeragevalue();  ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group" id="b3">
  <label class="col-md-4 control-label" for="brokerageamount">Brokerage Amount</label>  
  <div class="col-md-4">
  <input id="brokerageamount" name="brokerageamount" placeholder="" value="<?php echo $purchase->get_brokerageamount();  ?>" class="form-control input-md" type="text">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" placeholder="" value="<?php echo $purchase->get_pdate();  ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">NO Of Days</label>  
  <div class="col-md-4">
  <input id="no_of_days" name="no_of_days" placeholder="" value="<?php echo $purchase->get_no_of_days();  ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rate">Rate</label>  
  <div class="col-md-4">
  <input id="rate" name="rate" placeholder="" value="<?php echo $purchase->get_rate();  ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="weight">Weight</label>  
  <div class="col-md-4">
  <input id="weight" name="weight" placeholder="" value="<?php echo $purchase->get_weight();  ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="weighttype">Weight type</label>
  <div class="col-md-4">
    <select id="weighttype" name="weighttype" class="form-control">
      <option selected="selected" value="<?php echo $purchase->get_weighttype();  ?>"><?php echo $purchase->get_weighttype();  ?></option>
      <option value="carat">carat</option>
      <option value="cent">cent</option>
    </select>
  </div>
</div>



<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="type">Type</label>
  <div class="col-md-4">
    <select id="type" name="type" class="form-control">
      <option selected="selected" value="<?php echo $purchase->get_type(); ?>"><?php echo $purchase->get_type();  ?></option>
      <option value="palcha">palcha</option>
      <option value="rough">rough</option>
      <option value="taiyar">taiyar</option>
      <option value="direct">direct</option>
    </select>
  </div>
</div>




<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="Update">
  </div>
</div>




</form>
            
</div>
</div>
</div>
</div>
</div>
</section>

<?php include('footer.php'); ?>


<?php

if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);

    $purchase = new Ds_purchase();
    $purchase->set_pid($data['pid']);
    $purchase->set_pcid($data['pcid']);
    $purchase->set_broker($data['broker']);    
    $purchase->set_brokeragetype($data['brokeragetype']);
    echo "Brokerage Result is ".$data['brokeragetype'];
    $purchase->set_brokeragevalue($data['brokeragevalue']);
    $purchase->set_brokerageamount($data['brokerageamount']);
    $purchase->set_pdate($data['pdate']);    
    $purchase->set_no_of_days($data['no_of_days']);
    $purchase->set_rate($data['rate']);
    $purchase->set_weight($data['weight']);
    $purchase->set_weighttype($data['weighttype']);    
    $purchase->set_type($data['type']);
    $status=1;
    $purchase->set_status($status);  

 
    $update_purchase=$purchase->update_purchase();  
    
    if($update_purchase=TRUE){
    
    notify("info","Purchase updated successfully");
    ?>
<script type="text/javascript">
window.location='purchase_list.php';
</script>
    
    <?php
   }}   


?>


<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();


$("#b1").hide();
$("#b2").hide();
$("#b3").hide();
function getbroker(val)
{
   if(val!=0)
   {
$("#b1").show();
$("#b2").show();
$("#b3").show();
   }
   else
   {
$("#b1").hide();
$("#b2").hide();
$("#b3").hide();
   }
}
</script>

