<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.expense.php');


if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

$expense_list=new Ds_Expense();
$result=$expense_list->list_of_all_expenses();



?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo EXPENSELIST;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr><th>ID</th><th>Date</th><th>Description</th><th>Amount</th><th>Edit</th></tr>
</thead>
<?php
if(!empty($result))
{    
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".date("d/m/Y",$obj['created_on'])."</td>";
    echo "<td>".$obj['remark']."</td>";
    echo "<td>".$obj['amount_paid']."</td>";
    echo "<td><a href='expense_edit.php?id=".$obj['id']."'><i class=\"fa fa-edit\"></i></a></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');


 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
 

   </script>
      
 
  </body>
</html>



