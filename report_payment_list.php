<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$payment_list=new Ds_Payment();
$result=$payment_list->list_of_all_payment_list_with_name();

?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PAYMENTALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">  
                  
                  <thead>
<tr><th>NO</th><th>Name</th><th>Address</th><th>Phone</th><th>Company Nmae</th><th>View</th></tr></thead>
<?php
if(!empty($result))
{       
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$obj['ar']."</td>";
    echo "<td>".$obj['phoneno']."</td>";
    echo "<td>".$obj['companyname']."</td>";
    echo "<td><a href='report_payment_customer.php?pid=".$obj['id']."'><i class=\"fa fa-edit\"></i></a></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
  </body>
</html>




