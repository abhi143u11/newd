      <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>New</b>Diamond</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>New Diam</b>ond</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
         
       
              <!-- Tasks: style can be found in dropdown.less -->
            
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">  -->
                  <span class="hidden-xs"><?php 
                  if(isset($_SESSION["user"]))
                  {
                  echo $_SESSION["user"]; 
                  }
                   ?></span>
                </a>
              
              </li>
              <!-- Control Sidebar Toggle Button -->
           
            </ul>
          </div>

        </nav>
      </header>




  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
          
        <section class="sidebar">
    
  
         
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            
            
             <li class="treeview">
              <a href="dashboard.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
              </a>
              </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span>Customer</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
             <ul class="treeview-menu">
                <li><a href="customer_add.php"><i class="fa fa-circle-o"></i>Add Customer</a></li>
                <li><a href="customer_show.php"><i class="fa fa-circle-o"></i>View Customer</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-shopping-cart"></i> <span>Purchase</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="purchase_add.php"><i class="fa fa-circle-o"></i>Purchase Order</a></li>
                <li><a href="purchase_list.php"><i class="fa fa-circle-o"></i>Purchase Transaction List</a></li>
                
              </ul>
              
              </li>
              
              
               <li class="treeview">
              <a href="#">
                <i class="fa  fa-shopping-cart"></i> <span>Sales</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="sales_add.php"><i class="fa fa-circle-o"></i>Sales Order</a></li>
                <li><a href="sales_list.php"><i class="fa fa-circle-o"></i>Sales Transaction List</a></li>
                
              </ul>
              
              </li>
              
              
                 <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Process Type</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="processtype_add.php"><i class="fa fa-circle-o"></i>Add</a></li>
                <li><a href="processtype_list.php"><i class="fa fa-circle-o"></i>Process type List</a></li>
                
              </ul>
              
              </li>
              
                 <li class="treeview">
              <a href="#">
                <i class="fa  fa-spinner"></i> <span>Process</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="process_add.php"><i class="fa fa-circle-o"></i>Process Add</a></li>
                <li><a href="process_list.php"><i class="fa fa-circle-o"></i>Process List</a></li>
                
              </ul>
              
              </li>
            
              <li class="treeview">
              <a href="#">
                <i class="fa fa-rupee"></i> <span>Payment</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
                      <ul class="treeview-menu">
                            <li><a href="payment_add.php"><i class="fa fa-circle-o"></i>Payment Add</a></li>
                            <li><a href="payment_list.php"><i class="fa fa-circle-o"></i>Payment List</a></li>
                      </ul>
              </li>
              
              <li class="treeview">
              <a href="#">
                <i class="fa fa-rupee"></i> <span>Expence</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
                      <ul class="treeview-menu">
                            <li><a href="expense_add.php"><i class="fa fa-circle-o"></i>Expense Add</a></li>
                            <li><a href="expense_list.php"><i class="fa fa-circle-o"></i>Expense List</a></li>
                      </ul>
              </li>
              
            
              
              
              <li class="treeview">
              <a href="#">
                <i class="fa  fa-file-pdf-o"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
                      <ul class="treeview-menu">
                            <li ><a href="report_customer_list.php" ><i class="fa fa-circle-o"></i>Customer list</a></li>
                            <li ><a href="report_custom_list.php" ><i class="fa fa-circle-o"></i>Customer Custom List</a></li>
                            <li ><a href="report_sales_list.php" ><i class="fa fa-circle-o"></i>Sales list</a></li>
                            <li ><a href="report_purchase_list.php" ><i class="fa fa-circle-o"></i>Purchase list</a></li>
                            <li ><a href="report_payment_list.php" ><i class="fa fa-circle-o"></i>Payment list</a></li>
                            <li ><a href="report_payment_custom.php" ><i class="fa fa-circle-o"></i>Payment Customt</a></li>
                            <li ><a href="report_stock_list.php" ><i class="fa fa-circle-o"></i>Stock list</a></li>
                            <li ><a href="report_expense_custom.php" ><i class="fa fa-circle-o"></i>Expense Custom list</a></li>
                            <li ><a href="report_account_history.php" ><i class="fa fa-circle-o"></i>Account History</a></li>
                            <li ><a href="report_balancesheet.php" ><i class="fa fa-circle-o"></i>Balance Sheet</a></li>
                      </ul>
              </li>

              


        
               <li class="treeview">
              <a href="logout.php">
                <i class="fa fa-circle"></i> <span>Logout</span> 
              </a>
              </li>
        
        
          </ul>
           
           
        </section>
        <!-- /.sidebar -->
      </aside>

    