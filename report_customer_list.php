<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.customer.php');
require_once('classes/class.payment.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$customer_list=new Ds_Customer();
$result=$customer_list->all_list_customer();

$customer_total_amount=new Ds_Payment();

?>
<?php include('header.php'); ?>

<section class="content">
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTCUSTOMERALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr>
<th>Name</th>
<th>Address</th>
<th>Phone</th>
<th>Purchase Amount</th>
<th>Payable</th>
<th>Sales Amount</th>
<th>Receivable</th>
<th>All Bill</th>
</tr>
</thead>
<?php

if(!empty($result))
{  
    
foreach($result as $obj)
{
$pamount=$customer_total_amount->customer_purchase_amount($obj['cid']);    
$samount=$customer_total_amount->customer_sales_amount($obj['cid']);    
$receiveamount=$customer_total_amount->customer_paid_amount_sales($obj['cid']);
$paidamount=$customer_total_amount->customer_paid_amount_purchase($obj['cid']);

    echo "<tr>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$obj['address']."</td>";
    echo "<td>".$obj['phoneno']."</td>";
    echo "<td>".$pamount."(".$paidamount.")</td>";
    echo "<td>".($pamount-$paidamount)."</td>";
    echo "<td>".$samount."(".$receiveamount.")</td>";
    echo "<td>".($samount-$receiveamount)."</td>";
    echo "<td><a href='report_customer_allbill.php?cid=".$obj['cid']."'> View</a></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>

</section>






<?php include('footer.php'); ?>


 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      

$(document).ready(function(){
    
$("#customerlist").click(function(){
    
    alert("Call to table");
});    
    
}); 

   </script>
      
  </body>
</html>





