<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.stock.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



$stock_list=new Ds_Stock();
$stock_result=$stock_list->list_of_active_stock();

?>
<?php include('header.php'); ?>

  <section class="content">
<!-- 
Select a range 
     -->


                  

     
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTSTOCKINFO;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">  
                  
<thead>
<tr>
<th>Stock Code</th>
<th>Date</th>
<th>Weigth Type</th>
<th>Type</th>
<th>Rate</th>
<th>Amount</th>
<th>Stock In </th>
</tr>
</thead>
<?php
if(!empty($stock_result))
{    
foreach($stock_result as $obj)
{
$weight=$obj['weight'];    
    echo "<tr><td>".$obj['stock_id']."</td>";
    echo "<td>".date("d/m/Y",$obj['pdate'])."</td>";
    echo "<td>".$obj['weighttype']."</td>";
    echo "<td>".$obj['type']."</td>";
    echo "<td>".$obj['rate']."</td>";
    echo "<td>".$obj['amount']."</td>";
    
    if($weight!=0)
    {
    echo "<td class='bg-green-active color-palette'>".$weight."</td>";
    }else{
    echo "<td class='bg-orange-active color-palette'>".$weight."</td>";
    }    
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');


 ?>

 <script>
     
      $(function(){
          
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
   $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );   
 
    </script>
  </body>
</html>




