<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.customer.php');
require_once('classes/class.stock.php');
require_once('classes/class.sales.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


?>
<?php include('header.php');
/*
* load customer id and name with full information 
*/

$sid=0;
if(isset($_GET['sid']))
{
$sid=$_GET['sid'];;
}
$sales_data=new Ds_Sales($sid);





$cid = $sales_data->get_scid();

$customer = new Ds_Customer($cid);
$result=$customer->all_list_customer();

                             

$stock_list=new Ds_Stock();
$stock_result=$stock_list->list_of_active_stock();



 ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo SALESEDIT;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST" role="form" data-toggle="validator">
<input type="hidden" value="<?php echo $sales_data->get_sid()  ?>" name="sid" /> 
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Customer</label>
  <div class="col-md-4">
    <select id="scid" name="scid" class="form-control">
    <?php

    foreach($result as $obj)
    {
     if($sales_data->get_scid()==$obj['cid'])
     {   
     echo "<option selected value=".$obj['cid'].">".$obj['name']."</option>";
     }
     else
     {
     echo "<option  value=".$obj['cid'].">".$obj['name']."</option>";
     }
    }
    ?>
    </select>
  </div>
</div>

<!-- Purchase id-->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Purchase ID</label>
  <div class="col-md-4">
   <input id="purchase_id" name="purchase_id" value="<?php echo $sales_data->get_pid();  ?>" placeholder="" class="form-control input-md"  type="text" readonly="readonly"> 
   
  </div>
</div>


<!-- Select Basic -->
<div class="form-group" >
  <label class="col-md-4 control-label" for="broker">Broker</label>
  <div class="col-md-4">
    <select id="broker" name="broker" class="form-control" onchange="getbroker(this.value)">
      <?php
  
        if($sales_data->get_broker()==0)
        {
        echo "<option selected='selected' value='0'>NONE</option>";            
        }
        else
        {
        echo "<option  value='0'>NONE</option>";            
        }
        
    foreach($result as $obj)
    {
      if($sales_data->get_broker()==$obj['cid'])
     {   
     echo "<option selected value=".$obj['cid'].">".$obj['name']."</option>";
     }
     else
     {        
     echo " <option value=".$obj['cid'].">".$obj['name']."</option>";
    }
    }
    ?>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group" id="b1">
  <label class="col-md-4 control-label" for="brokeragetype">Brokerage Type</label>
  <div class="col-md-4">
    <select id="brokeragetype" name="brokeragetype" class="form-control">
      <option value="<?php echo $sales_data->get_brokertype(); ?>"><?php echo $sales_data->get_brokertype(); ?></option>
      <option value="PERCENTAGE">PERCENTAGE</option>
      <option value="FIXED">FIXED</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group" id="b2">
  <label class="col-md-4 control-label" for="brokeragevalue">Brokerage Value</label>  
  <div class="col-md-4">
  <input id="brokeragevalue" name="brokeragevalue" value="<?php echo $sales_data->get_brokeragevalue();  ?>" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group" id="b3">
  <label class="col-md-4 control-label" for="brokerageamount">Brokerage Amount</label>  
  <div class="col-md-4">
  <input id="brokerageamount" value="<?php echo $sales_data->get_brokerageamount();  ?>" name="brokerageamount" placeholder="" class="form-control input-md" type="text">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" value="<?php echo $sales_data->get_pdate();  ?>" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">NO Of Days</label>  
  <div class="col-md-4">
  <input id="no_of_days" name="no_of_days" value="<?php echo $sales_data->get_no_of_days(); ?>" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="rate">Rate</label>  
  <div class="col-md-4">
  <input id="rate" name="rate" placeholder="" value="<?php echo $sales_data->get_rate();  ?>" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="weight">Available Weight</label>  
  <div class="col-md-4">
 <label class="col-md-4 control-label"   for="weight" id="avaweight"></label>
    
  </div>
  
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="weight">Weight</label>  
  <div class="col-md-4">
  <input type="hidden" name="oldweigth" value="<?php echo $sales_data->get_weight();  ?>" /> 
  <input id="weight" name="weight" value="<?php echo $sales_data->get_weight();  ?>" placeholder="" class="form-control input-md" required="" type="number" min="0">
    
  </div>
  
</div>

<!-- Select Basic -->
<div class="form-group">

   <label class="col-md-4 control-label" for="weight">Weight type</label>  
  <div class="col-md-4">
  <input id="weighttype" name="weighttype" value="<?php echo $sales_data->get_weighttype();  ?>" placeholder="" readonly="readonly" class="form-control input-md" value="carat" required="" type="text">
    
  </div>
</div>



<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="type">Type</label>
  <div class="col-md-4">
    <select id="type" name="type" class="form-control">
   
      <option selected="selected" value="<?php echo $sales_data->get_type(); ?>">palcha</option>
      <option value="palcha">palcha</option>
      <option value="rough">rough</option>
      <option value="taiyar">taiyar</option>
      <option value="direct">direct</option>
    </select>
  </div>
</div>




<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="Update">
  </div>
</div>




</form>
            
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();

 function getInfo(val)
{                  
 
    $.ajax({
    type:"POST",
    url:"get_stock.php",
    data:'pinfo='+val,
    success:function(data){
      
    $("#avaweight").html(data);
   // $("#weight").attr('max',data);
    }    
        
    });

}
      
     getInfo(<?php echo $sales_data->get_pid(); ?>); 
      

 
</script>
<?php

if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);
 $sales_update = new Ds_Sales();
    $sales_update->set_oldweigth($data['oldweigth']);
    $sales_update->set_sid($data['sid']); 
    $sales_update->set_scid($data['scid']); 
    $sales_update->set_pid($data['purchase_id']); 
    $sales_update->set_broker($data['broker']); 
    $sales_update->set_brokertype($data['brokeragetype']);
    $sales_update->set_brokeragevalue($data['brokeragevalue']); 
    $sales_update->set_brokerageamount($data['brokerageamount']); 
    $sales_update->set_pdate($data['pdate']);    
    $sales_update->set_no_of_days($data['no_of_days']);
    $sales_update->set_rate($data['rate']);
    $sales_update->set_weight($data['weight']);
    $sales_update->set_weighttype($data['weighttype']);    
    $sales_update->set_type($data['type']);
    $status=1;
    
    $sales_update->set_status($status);  
  
    $update_sales=$sales_update->update_sales();  
    
      if($update_sales==TRUE)
      {
      
             
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "bottom",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Sales transaction Update Successfully');
}, 1000);
window.location = 'sales_list.php';
   </script>
   <?php  
     
}
}  

if($sales_data->get_broker()==0)
{
?>
$("#b1").hide();
$("#b2").hide();
$("#b3").hide();

<?php
        
    
}
 
?>
<script type="text/javascript">
function getbroker(val)
{
   if(val!=0)
   {
$("#b1").show();
$("#b2").show();
$("#b3").show();
   }
   else
   {
$("#b1").hide();
$("#b2").hide();
$("#b3").hide();
   }
}
</script>