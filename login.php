<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.admin.php');

    
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>New Diamond | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="index2.html"><b>New Dia</b>mond</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg" id="invalid"></p>
        <form action="" method="post" name="f1">
          <div class="form-group has-feedback">
            <input type="text" name="user" class="form-control" placeholder="User Name">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" >
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
             
            </div><!-- /.col -->
            <div class="col-xs-4">
              <input type="submit" name="login" value="Login" class="btn btn-primary btn-block btn-flat" onclick="login()" />
            </div><!-- /.col -->
          </div>
        </form>

   
    
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
     <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
      
      function login()
      {
       document.f1.user.value='';   
       document.f1.password.value='';   
       alert( document.f1.user.value);
      }
    </script>
  </body>
</html>
<?php
 if(isset($_REQUEST['login']))
 {
    $login=new Ds_Admin();
    $no=$login->login($_POST['user'],$_POST['password']);
    if($no>0)
    {
        $_SESSION['user']=$_POST['user'];
        $_SESSION['uid']=$no;
        header("Location:dashboard.php");
        //echo "<script>window.location='dashboard.php';</script>";
    }
    else
    {
        echo "<script>document.getElementById('invalid').innerHTML='User and Password Invalid';</script>";
        
    }
  }       
    
    
?>