<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.expense.php');
if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo EXPENSEADD;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST">
<!-- Form Name -->


<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Expense Description</label>  
  <div class="col-md-4">
  <input id="description" name="description" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="address">Amount</label>  
  <div class="col-md-4">
  <input id="amonut" name="amonut" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>




<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="add" name="add" class="btn btn-default" value="add">
  </div>
</div>

</fieldset>
</form>              
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$("#pdate").datepicker();
</script>
<?php

if(isset($_REQUEST["add"]))
{
 $data = $database->clean_data($_POST);

    $expense = new Ds_Expense();
    //set all the vars
    $expense->set_remark($data['description']);
    $expense->set_amount_paid($data['amonut']);
    $expense->set_created_on($data['pdate']);
    
 
    $add_expense=$expense->add_expense();  
    
    if($add_expense=TRUE)
    {
     notify("info","Expense Added Successfully");
    }
}   

?>


