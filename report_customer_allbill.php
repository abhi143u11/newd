<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');
require_once('classes/class.purchase.php');
require_once('classes/class.sales.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$cid=0;
if(isset($_GET['cid']))
{
$cid=$_GET['cid'];    
}

/*
*  get Customer Name
*/


/* $customer_list=new Ds_Customer();
$result=$customer_list->all_list_customer();
*/
$customer_name=new Ds_Customer($cid);

$purchase_bill=new Ds_Purchase();
$purchase_bill_result=$purchase_bill->list_of_all_purchase_with_name_desc($cid);

$sales_bill=new Ds_Sales();
$sales_bill_result=$sales_bill->list_of_all_sales_with_name_desc($cid);

$payment_record=new Ds_Payment();
$payment_pending_sales_result=$payment_record->sales_pending_amount();
$payment_pending_purchase_result=$payment_record->purchase_pending_amount();

/*
* payment all records 
*/

$all_purchase_payment=$payment_record->all_payment_record_purchase_invoice($cid);
$all_sales_payment=$payment_record->all_payment_record_sales_invoice($cid);

?>
<?php include('header.php'); ?>

<style type="text/css"> 
</style>
     
        <section class="content">
                               
        
 <div class="col-md-12"  >
                <div class="box-header">
                  <h3 class="box-title"><?php echo "Customer Ledger Report  [<font color=blue> ".$customer_name->get_customer_name()."</font> }";    ?> </h3>
                </div><!-- /.box-header -->  
 </div>        
 
  <div class="col-md-12"  >
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTCUSTOMERPURCHASEINFO;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                

<table  id="example2" class="table table-bordered table-striped">
<thead>
<tr><th>Date</th><th>Bill No</th><th>Name</th><th>Weight</th><th>Rate</th><th>Amount</th>
<th>Payment Date</th><th>Bill No</th><th>Name</th><th>Days</th><th>Amount</th>
</tr></thead>
<?php
if(!empty($purchase_bill_result))
{    
foreach($purchase_bill_result as $obj)
{
$pid=$obj['pid'];
$paidamount=$payment_record->sum_amount_of_purchase_invoice($pid);
$actualamount=$obj['amount'];
    echo "<tr>";
    echo "<td>".date("d/m/Y",$obj['pdate'])."</td>";    
    echo "<td>".$obj['pid']."</td>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$obj['weight']."</td>";
    echo "<td>".$obj['rate']."</td>";
 if($actualamount==$paidamount)
    {
    echo "<td class='bg-green-active color-palette'>".$actualamount."</td>";
    }else
    {
    echo "<td class='bg-orange-active color-palette'>".$actualamount."(".($actualamount-$paidamount).")</td>";
    }
$i=0;    
if(!empty($all_purchase_payment))
{    
foreach($all_purchase_payment as $val)
{
$pid=$val['invoiceID'];
$purchase_result=$purchase_bill->purchase_info_fromid($pid);
$purchase_result=$purchase_result[0];
$name=$customer_name->get_customer_name();
$weight=$purchase_result['weight'];    
$date=date("d/m/Y",$val['created_on']);
$duedate=$purchase_result["duedate"];

$d=date("Y-m-d",time());
$date1 = new DateTime($d);
$date2 = new DateTime(date('Y-m-d',$val['created_on']));
$diff = $date1->diff($date2);
$dueday=$diff->days;
if($dueday==0){$dueday="Today";}else if($dueday==1){$dueday="Yesterday";}
  
if($obj['pid']==$val['invoiceID'])
{ 
  // echo "<tr>";
   if($i!=0)
   {
   echo "<tr><td><td><td><td><td><td>";    
   }
    echo "<td>".$date."</td>";    
    echo "<td>".$val['invoiceID']."</td>";
    echo "<td>".$name."</td>";
    echo "<td>".$dueday."</td>";
    echo "<td >".$val["amount_paid"]."</td>";
    //echo "</tr>";
$i=1;
}

}
}    
 echo "</tr>";
}
}    
   
?>
</table>
</div>
</div>
</div>



 





<!-- right Side Panel -->

  <div class="col-md-12"  >
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTCUSTOMERSALESINFO;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                

<table  id="example2" class="table table-bordered table-striped">
<thead>
<tr><th>Date</th><th>Bill No</th><th>Name</th><th>Weight</th><th>Rate</th><th>Amount</th>
<th>Payment Date</th><th>Bill No</th><th>Name</th><th>Days</th><th>Amount</th>

</tr></thead>
<?php
if(!empty($sales_bill_result))
{        
foreach($sales_bill_result as $obj)
{
$sid=$obj['sid'];
$paidamount=$payment_record->sum_amount_of_sale_invoice($sid);
$actualamount=$obj['amount'];    
    echo "<tr>";
    echo "<td>".date("d/m/Y",$obj['pdate'])."</td>";
    echo "<td>".$obj['sid']."</td>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".$obj['weight']."</td>";
    echo "<td>".$obj['rate']."</td>";

    if($actualamount==$paidamount)
    {
    echo "<td class='bg-green-active color-palette'>".$actualamount."</td>";
    }else{
    echo "<td class='bg-orange-active color-palette'>".$actualamount."(".($actualamount-$paidamount).")</td>";
    }
    
 /*
 * inner sid payment data 
 */   

$i=0; 
if(!empty($all_sales_payment))
{    
foreach($all_sales_payment as $val)
{
$sid=$val['invoiceID'];
$sales_result=$sales_bill->sales_info_fromid($sid);
$sales_result=$sales_result[0];
$name=$customer_name->get_customer_name();
$weight=$sales_result['weight'];    
$date=date("d/m/Y",$val['created_on']);
$duedate=$sales_result["duedate"];

$d=date("Y-m-d",time());
$date1 = new DateTime($d);
$date2 = new DateTime(date('Y-m-d',$val['created_on']));
$diff = $date1->diff($date2);
$dueday=$diff->days;
if($dueday==0){$dueday="Today";}else if($dueday==1){$dueday="Yesterday";}

if($obj['sid']==$val['invoiceID'])
{
  // echo "<tr>";
   if($i!=0)
   {
   echo "<tr><td><td><td><td><td><td>";    
   }   
    echo "<td>".$date."</td>";    
    echo "<td>".$val['invoiceID']."</td>";
    echo "<td>".$name."</td>";
    echo "<td>".$dueday."</td>";
    echo "<td >".$val["amount_paid"]."</td>";
   
   $i=1;
  // echo "</tr>";
}
}    
}      
    
    
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</div>

  
 




</section>
<?php include('footer.php');


 ?>
     
  </body>
</html>




