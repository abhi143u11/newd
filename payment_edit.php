<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



?>
<?php include('header.php');
/*
* load payment id and name with full information 
*/


 


$id=0;
if(isset($_GET["id"]))
{
$id=$_GET["id"];    
}
/*
* load data from payment 
*/
$payment_data=new Ds_Payment($id);
/*
* load data from customer table 
*/
$customer_data=new Ds_Customer($payment_data->get_customerid());

?>

 
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PAYMENTUPDATE; ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST" role="form" data-toggle="validator">

<input type="hidden" name="id" value="<?php echo $payment_data->get_id();  ?>"/> 

<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Paymnet Method</label>
  <div class="col-md-4">
    <select id="pay_type" name="pay_type" class="form-control" onchange="getcustomer(this.value)">
    <option value="N">Select Payment</option>
    <?php 
    if($payment_data->get_pay_type()=="P")
    {
    echo "<option selected='selected' value='P'>PURCHASE</option>";
    echo "<option  value='S'>SALES</option>";

    }
    else
    {
    echo "<option selected='selected' value='S'>SALES</option>";
    echo "<option  value='P'>PURCHASE</option>";    

    }
     ?>
    </select>
  </div>
</div>


<!-- Customer Id Select -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Customer</label>
  <div class="col-md-4">
    <select id="customer" name="customer" class="form-control" onchange="getbillnumber(this.value)">
    <option value="<?php echo $payment_data->get_customerid(); ?>"><?php echo $customer_data->get_customer_name(); ?></option>

    </select>
  </div>
</div>

        


<!-- Bill Number which fetch from dynamjically which is of puchase or sales bill number-->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Bill No.</label>
  <div class="col-md-4">
    <select id="invoice" name="invoice" class="form-control" onchange="gettotalamount(this.value)">
    <option value="<?php echo $payment_data->get_invoiceid(); ?>"><?php echo $payment_data->get_invoiceid(); ?></option>
    
    </select>
  </div>
</div>



<!-- total amount-->
<div class="form-group">
  <label class="col-md-4 control-label" for="brokeragevalue">Total Amount</label>  
  <div class="col-md-4">
  <input id="total_amount" readonly="readonly" value="<?php echo $payment_data->get_total_amount(); ?>" name="total_amount" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
  
   <div class="col-md-4">
    <label class="checkbox-inline" for="checkboxes-0">
      <input name="balance" id="balance" value="1" type="checkbox">
      Use Customer Balance
    </label>
  </div>
  
</div>

 <!-- Amount paid -->
<div class="form-group">
  <label class="col-md-4 control-label" for="brokeragevalue">Amount Paid</label>  
  <div class="col-md-4">
  <input id="amount_paid" value="<?php echo $payment_data->get_amount_paid();  ?>" name="amount_paid" placeholder="" class="form-control input-md" required="" type="number">
    
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="brokeragevalue">Customer Balance</label>  
  <div class="col-md-4">
  <input id="use_customer_balance" readonly="readonly" name="use_customer_balance" value="<?php echo $customer_data->get_balance();?>" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Remark-->
<div class="form-group">
  <label class="col-md-4 control-label" for="brokerageamount">Remark</label>  
  <div class="col-md-4">
  <input id="remark" name="remark" placeholder="" value="<?php echo $payment_data->get_remark();?>" class="form-control input-md" type="text">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="no_of_days">Date</label>  
  <div class="col-md-4">
  <input id="pdate" name="pdate" placeholder="" class="form-control input-md" required="" value="<?php echo $payment_data->get_created_on();?>" type="text">
    
  </div>
</div>







<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="Update">
  </div>
</div>





</form>
            
</div>
</div>
</div>
</div>
</div>
</section>
</section>
<?php include('footer.php'); ?>
<link rel="stylesheet" type="text/css" href="plugins/datepicker/datepicker3.css">
<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
<?php
if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);
    $payment_update = new Ds_Payment();
    $payment_update->set_id($data['id']); 
    $payment_update->set_pay_type($data['pay_type']); 
    $payment_update->set_customerid($data['customer']); 
    $payment_update->set_invoiceid($data['invoice']);
    $payment_update->set_total_amount($data['total_amount']); 
    $payment_update->set_amount_paid($data['amount_paid']); 
    $payment_update->set_use_customer_balance($data['use_customer_balance']);
    $payment_update->set_remark($data['remark']);
    $payment_update->set_created_on($data['pdate']);  
   
    $bal=$data['balance'];
    if($bal!=1)
    {
     $bal=0;   
    }
    $payment_update->set_balancech($bal);
 
    $update_payment=$payment_update->update_payment();  
      if($update_payment==TRUE)
      {
        ?>
  <script type="text/javascript">
    var notify = $.notify('', {
    type: 'info',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "bottom",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', 'Payment transaction Update Successfully');
}, 1000);

window.location='payment_list.php';
   </script>
   <?php     
}
}      
    
?>
<script type="text/javascript">
$("#pdate").datepicker();
</script>



  <script type="text/javascript">
//get Customer Information
var pmethod;
var cid;
function getcustomer(val)
{                         
    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'customer='+val,
    success:function(data){
    $("#customer").html(data);
     
    }  
    });
pmethod=val;  
} 

function getmes(val)
{
    
    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'balancecid='+val,
    success:function(data){
    $("#use_customer_balance").val(data);
    }  
    });
}


function getbillnumber(val)
{  
cid=val;                       

    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'pmethod='+pmethod+'&cid='+val,
    success:function(data){
    $("#invoice").html(data);
    }  
    });
this.getmes(cid); 
}


function gettotalamount(val)
{  
    $.ajax({
    type:"POST",
    url:"get_payment_info.php",
    data:'totatlmethod='+pmethod+'&tid='+val,
    success:function(data){
    var d=data;
    $("#total_amount").val(d);
    $("#amount_paid").attr('max',d);
    }  
    });
}

</script>

