<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.sales.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$sales_list=new Ds_Sales();
$sales_result=$sales_list->list_of_all_sales();


?>
<?php include('header.php'); ?>
        <section class="content">

        
                  
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo SALESALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
 <table id="example2" class="table table-bordered table-striped">  
 <thead>
<tr>
<th>Bill No</th>
<th>Date</th>
<th>Weight Type</th>
<th>Amount</th>
<th>Edit</th>
<th>Delete</th>
</tr>
</thead>
<?php
if(!empty($sales_result))
{     
foreach($sales_result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['sid']."</td>";
    echo "<td>".date("m/d/Y",$obj['pdate'])."</td>";
    echo "<td>".$obj['weighttype']."</td>";
    echo "<td>".$obj['amount']."</td>";
    echo "<td><a href='sales_edit.php?sid=".$obj['sid']."'> <i class=\"fa fa-edit\"></i></a></td>";
    echo "<td><a href='sales_delete.php?sid=".$obj['sid']."'  onclick=\"return confirm('Really delete?');\"><i class=\"fa fa-trash text-red\"></i></a></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
   
}
    ?>
  </body>
</html>




