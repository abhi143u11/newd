<?php
//session_start();
/*
* =======================================================================
* @script:          Database_Class
* -----------------------------------------------------------------------
* @version:         1.2
* =======================================================================
*/

require_once ("constants.php");
include_once("common.functions.php");

class MySQLDB
{
    private $connection; //The MySQL database connection
    private $result = array();
    private $enum = 1;
    private $err = array();
    /* Class constructor */
    function MySQLDB()
    {
        /* Make connection to database */
        $this->connection=mysql_connect(DB_API_SERVER, DB_API_USER, DB_API_PASS) or die(mysql_error());
        mysql_select_db(DB_API_NAME, $this->connection) or die(mysql_error());
        mysql_set_charset('utf8',$this->connection);
    }
    
    function __destruct()
    {
        foreach ($this->result as $val)
        {
            if(is_resource($val)) mysql_free_result($val);
        }
        unset($this->result);
        mysql_close($this->connection);
        unset($this->connection);
    }
    
 /*
  * query - Performs the given query on the database and
  * returns the result, which may be false, true or a
  * resource identifier.
  */
    function query($query)
    {
        $this->result[$this->enum]=mysql_query($query, $this->connection);
        if ($this->result[$this->enum])
        {
            $this->enum++;
            return $this->enum - 1;
        }
        else
        {
            //echo $query;
            $this->err[$this->enum] = mysql_error($this->connection);
            unset ($this->result[$this->enum]);
            return FALSE;
        }
    }
    
    function query_fetch_full_result($query)
    {
        $result = mysql_query($query, $this->connection);
        $rows = array();
        
        if($result)
        {
            while($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }
            mysql_free_result($result);
            if(count($rows) < 1) $rows = FALSE;
            return $rows;
        }
        else return FALSE;
    }
    
    function fetch_array($enum)
    {
        if ($this->result[$enum]) $row=mysql_fetch_assoc($this->result[$enum]);
        else $row=FALSE;
        return $row;
    }
    function row_count($enum)
    {
        if ($this->result[$enum]) $count=mysql_num_rows($this->result[$enum]);
        else $count=FALSE;
        return $count;
    }
    function sanitize($data)
    {
        $data = stripcslashes(strip_tags($data));
        $result=mysql_real_escape_string($data, $this->connection);
        return $result;
    }
    function free_result($enum)
    {
        if ($this->result[$enum] && is_resource($this->result[$enum]))
        {
            mysql_free_result ($this->result[$enum]);
            unset ($this->result[$enum]);
        }
    }
    
    function clean_data($input)
    {
        if (is_array($input))
        {
            foreach ($input as &$value)
            {
                $value = $this->clean_data($value);
            }
        }
        else
        {
            $input = $this->sanitize($input);
        }
        return $input;
    }
    
    function get_last_err()
    {
        return $this->err[$this->enum];
    }
    
    function get_last_insert_id()
    {
        return mysql_insert_id($this->connection);
    }
}
;
/* Create database connection */
$database = new MySQLDB;
?>