<?php
class Ds_Admin
{
private $uid;
private $user;
private $password_text;
private $password;

/*
* constructor called 
*/
public function Ds_admin($uid = '')
    {
        if ($uid != '') {
            if($this->set_uid($uid)){
                if ($this->load_admin())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    

    public function __destruct()
    {
        // TODO: destructor code
    }  

/*
* load data of admin  
*/    
   public function load_admin()
 {
        global $database;
        $query = "SELECT * FROM `admin` WHERE `uid`= " . $this->uid . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
        
        $result            = $result[0];
        $this->uid = $result['uid'];
        $this->user    = $result['user'];
        $this->password_text    = $result['password_text'];
        $this->password    = $result['password'];
        return TRUE;    
 }
 
 /*
 * admin login data 
 */
 public function add_admin()
 {
 global $database;
 $query = "INSERT INTO `admin`( `user`, `password_text`, `password`) VALUES('".$this->user."','".$this->password_text."','".$this->password."')";
 $result=$database->query($query);   
 return $result;
 }
/*
* update user password 
*/
 public function update_admin()
 {
 global $database;
 $query = "UPDATE `admin` SET `user`='".$this->user."', `password_text`='".$this->password_text."', `password`='".$this->password."' WHERE `uid`=".$this->uid."";
 $result=$database->query($query);   
 return $result;
 }   

/*
* delete user password 
*/ 
 public function delete_admin()
 {
 global $database;
 $query = "DELETE FROM `admin` WHERE `uid`=".$this->uid."";
 $result=$database->query($query);   
 return $result;
 }   
 
 /*
 * lis of admin user 
 */
 public function all_admin()
 {
 global $database;
 $query = "SLECT* FROM `admin`";
 $result=$database->query_fetch_full_result($query);   
 return $result;
 }   
 
 
 public function login($user,$password)
 {
 global $database;
 $query = "SELECT * FROM `admin` WHERE `user`='".$user."' AND `password`='".$password."'";
 $result=$database->query_fetch_full_result($query);   
 $no=0;
 if(!empty($result)){$result=$result[0];$no=$result['uid'];} 
 return $no;     
 }
 
 
/*
* php setter and getter method 
*/    

    public function get_uid(){
        return $this->uid;
    }

    public function set_uid($uid){
        $this->uid = $uid;
        $this->load_admin();
    }

    public function get_user(){
        return $this->user;
    }

    public function set_user($user){
        $this->user = $user;
    }

    public function get_password_text(){
        return $this->password_text;
    }

    public function set_password_text($password_text){
        $this->password_text = $password_text;
    }

    public function get_password(){
        return $this->password;
    }

    public function set_password($password){
        $this->password = $password;
    }


}  
?>
