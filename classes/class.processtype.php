<?php

require_once('class.database.php');



class Ds_Processtype
{
private $process_type_id;
private $process_type_name;


/*
* constructor declared 
*/
public function Ds_Processtype($process_type_id = '')
    {
        if ($process_type_id != '') 
        {
            if($this->set_process_type_id($process_type_id)){
                if ($this->load_processtype())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    
    public function __destruct()
    {
        // TODO: destructor code
    }  
 
/*
*   load process type
*/ 
 public function load_processtype()
 {
        global $database;
        $query = "SELECT * FROM `process_type` WHERE `process_type_id`= " . $this->process_type_id . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
 
        $result            = $result[0];
        $this->process_type_id = $result['process_type_id'];
        $this->process_type_name    = $result['process_type'];
        return TRUE;    
 }
 
/*
*  add process type loginc 
*/    
public function add_processtype()
{
        global $database;
        $ary_err    = array();
        $ary_return = array();
   
        if ($this->process_type = '') {
            $ary_err[] = 'Process type not entered';
        } 
        
      if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }    
        
     $query = "INSERT INTO `process_type`(`process_type`) VALUES('".$this->process_type_name."')";
     $result = $database->query($query);
          
     if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;       
}   

/*
* update sales order transaction 
*/ 

public function update_processtype()
{
        global $database;
      
        $ary_err    = array();
        $ary_return = array();
   
        if ($this->process_type = '') {
            $ary_err[] = 'Process type not entered';
        }
        
        if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }
        
      $query = "UPDATE `process_type` SET `process_type`='" . $this->process_type_name ."' WHERE `process_type_id`= " . $this->process_type_id."";
        
        $result=$database->query($query);
        
        if ($result)
        {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;           
        
}

/*
*  delete sales transaction bill 
*/
public function delete_processtype()
{
    global $database;
         $ary_err    = array();
        $ary_return = array();
    $query="DELETE FROM `process_type` WHERE `process_type_id`=".$this->process_type_id."";
    $result=$database->query($query);
     if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        
        $ary_return[]      = FALSE;
        $ary_err["errors"] = $ary_err;
        return $ary_return;
    return $ary_return;
}

/*
*  get all sales bills result
*/
public function list_of_all_processtype()
{
    global $database;
    $query="SELECT * FROM `process_type`";
    $result=$database->query_fetch_full_result($query);
    return $result;
}

/*
* setter and getter method of process type  
*/    

    public function get_process_type_id(){
        return $this->process_type_id;
    }

    public function set_process_type_id($process_type_id){
        $this->process_type_id = $process_type_id;
        $this->load_processtype();
    }

    public function get_process_type_name(){
        return $this->process_type_name;
    }

    public function set_process_type_name($process_type_name){
        $this->process_type_name = $process_type_name;
     }

}
?>
