<?php
require_once('class.database.php');


class Ds_Payment
{
private $id;
private $pay_type;
private $customerid;
private $total_amount;
private $invoiceid;
private $amount_paid;
private $payment_type;
private $status;
private $use_customer_balance;
private $remark;
private $created_on;
private $balancech;




/*
*  Constructor define 
*/
public function Ds_payment($id = '')
    {
        if ($id != '') {
            if($this->set_id($id)){
                if ($this->load_payment())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    

    public function __destruct()
    {
        // TODO: destructor code
    }
    
    
/*
* load payment data 
*/
  public function load_payment()
 {
        global $database;
        $query = "SELECT * FROM `payment` WHERE `id`= " . $this->id . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
        
        $result            = $result[0];
        $this->id = $result['id'];
        $this->pay_type    = $result['pay_type'];
        $this->customerid    = $result['customerID'];
        $this->total_amount    = $result['total_amount'];
        $this->invoiceid   = $result['invoiceID'];
        $this->amount_paid    = $result['amount_paid'];
        $this->payment_type    = $result['payment_type'];
        $this->status    = $result['status'];
        $this->use_customer_balance    = $result['use_customer_balance'];
        $this->remark     = $result['remark'];
        $this->created_on     = $result['created_on'];   
        return TRUE;    
 }

/*
* Add Payment 
* 
*/

public function add_payment()
{
global $database;
$ary_err    = array();
$ary_return = array();

$customerbalance=0;
$balancecondition=0;
if($this->balancech==1)
{
$customerbalance=$this->amount_paid;
$balancecondition=1;
}
else
{
$customerbalance=0;    
$balancecondition=0;
}

$paymenttype="1";
$status="CASH";
$query="INSERT INTO `payment`(`pay_type`, `customerID`, `total_amount`, `invoiceID`, `amount_paid`, `payment_type`, `status`, `use_customer_balance`, `remark`, `created_on`) VALUES('".$this->pay_type."',".$this->customerid.",".$this->total_amount.",".$this->invoiceid.",".$this->amount_paid.",'".$paymenttype."','".$status."',".$customerbalance.",'".$this->remark."',".$this->created_on.")";        

var_dump($query);

$result=$database->query($query);

if($balancecondition==1 && $this->use_customer_balance!=0)
{
$customer_bal_query="UPDATE customer SET balance=balance-".$customerbalance." WHERE cid=".$this->customerid."";
$bal_result=$database->query($customer_bal_query);    
}
       
if($result)
{
    $ary_return["success"] = TRUE;
      return $ary_return;
}
 $ary_return["success"] = FALSE;
 $ary_err["errors"]     = $ary_err;
 return $ary_return;    
}

/*
* Update payment 
*/    

public function update_payment()
{
global $database;
$ary_err    = array();
$ary_return = array();

$paymenttype="1";
$status="CASH";

$customerbalance=0;
$balancecondition=0;
if($this->balancech==1)
{
$customerbalance=$this->amount_paid;
$balancecondition=1;
}
else
{
$customerbalance=0;  
$balancecondition=0;  
}

/*
* When add back to customer bal
*/
$query="SELECT * FROM payment WHERE id=".$this->id."";    
$payment_result=$database->query_fetch_full_result($query);
$payment_result=$payment_result[0];
$bal_add=$payment_result['use_customer_balance'];





$query="UPDATE `payment` SET `pay_type`='".$this->pay_type."', `customerID`=".$this->customerid.", `total_amount`=".$this->total_amount.", `invoiceID`=".$this->invoiceid.", `amount_paid`=".$this->amount_paid.", `payment_type`='".$paymenttype."', `status`='".$status."', `use_customer_balance`=".$customerbalance.", `remark`='".$this->remark."', `created_on`=".$this->created_on." WHERE `id`=".$this->id."";
$result=$database->query($query);

if($balancecondition==1)
{
$customer_bal_query="UPDATE customer SET balance=(balance+".$bal_add.")-".$customerbalance." WHERE cid=".$this->customerid."";
var_dump($customer_bal_query);
$bal_result=$database->query($customer_bal_query);    
}

if($balancecondition==0)
{
$customer_bal_query="UPDATE customer SET balance=balance+".$bal_add." WHERE cid=".$this->customerid."";
var_dump($customer_bal_query);
$bal_result=$database->query($customer_bal_query);    
}

if($result)
 {
    $ary_return["success"] = TRUE;
      return $ary_return;
 }
 $ary_return["success"] = FALSE;
 $ary_err["errors"]     = $ary_err;
 return $ary_return;    
    
}

/*
* all Payment List 
*/

public function list_of_all_payment_list()
{
global $database;
$query="SELECT * FROM `payment`";
$result=$database->query_fetch_full_result($query);
return $result;    
}

/*
* balace statement
* 
*/

public function list_of_all_payment_list_for_statement()
{
global $database;
$query="SELECT * FROM `payment` ORDER BY created_on DESC";
$result=$database->query_fetch_full_result($query);
return $result;    
}


/*
* get data whose customer paid 
* 
*/
public function list_of_all_payment_list_with_name()
{
global $database;
$query="select distinct b.customerID as id,a.name as name,a.address as ar,a.phoneno as phoneno,a.companyname as companyname from customer a INNER JOIN payment b WHERE a.cid=b.customerID";
$result=$database->query_fetch_full_result($query);
return $result;    
}

/*
* get one customer full payment list  
*/
public function list_of_all_payment_list_of_on_customer($customerid)
{
global $database;
$query="SELECT * FROM `payment` WHERE customerId=".$customerid."";
$result=$database->query_fetch_full_result($query);
return $result;    
}


public function list_of_all_payment_list_with_customername()
{
global $database;
$query="SELECT a.*,b.name FROM payment a inner join customer b where a.customerID=b.cid";
$result=$database->query_fetch_full_result($query);
return $result;    
}

/*
* pending purchase amount display 
*/


public function purchase_pending_amount()
{
global $database;
$query="SELECT distinct a.cid,a.name,a.address,a.phoneno,b.amount,b.pid,b.pdate from customer a inner join purchase b on a.cid=b.pcid order by b.pdate desc limit 0,20";
$result=$database->query_fetch_full_result($query);
return $result;    
}
/*
* Pending Sales amount display 
*/
public function sales_pending_amount()
{
global $database;
$query="SELECT distinct a.cid,a.name,a.address,a.phoneno,b.amount,b.sid,b.pdate from customer a inner join sales b on a.cid=b.scid order by b.pdate desc limit 0,20";
$result=$database->query_fetch_full_result($query);
return $result;    
}
/*
* get total amount from purchase id  
*/
public function sum_amount_of_purchase_invoice($id)
{
global $database;
$query="SELECT sum(amount_paid) as amount FROM payment WHERE invoiceID=".$id." and pay_type='P'";
$result=$database->query_fetch_full_result($query);
$result=$result[0];
$amount=$result['amount'];
return $amount;    
}

/*
* one pid payment record display all 
*/
public function all_payment_record_purchase_invoice($id)
{
global $database;
$query="SELECT * FROM payment WHERE customerID=".$id." and pay_type='P' ORDER BY created_on DESC,invoiceID DESC";
$result=$database->query_fetch_full_result($query);
return $result;    
}


/*
*sum of amount of sale from one id 
*/
public function sum_amount_of_sale_invoice($id)
{
global $database;
$query="SELECT sum(amount_paid)as amount FROM payment WHERE invoiceID=".$id." and pay_type='S'";
$result=$database->query_fetch_full_result($query);
$result=$result[0];
$amount=$result['amount'];
return $amount;    
}
/*
* one pid payment record display all 
*/
public function all_payment_record_sales_invoice($id)
{
global $database;
$query="SELECT * FROM payment WHERE customerID=".$id." and pay_type='S' ORDER BY created_on  DESC,invoiceID DESC";
$result=$database->query_fetch_full_result($query);
return $result;    
}
/*
* customer purchase amount 
*/
public function customer_purchase_amount($id)
{
global $database;
$query="SELECT sum(amount)as amount FROM purchase WHERE pcid=".$id."";
$result=$database->query_fetch_full_result($query);
$result=$result[0];
$amount=$result['amount'];
return $amount;    
}
/*
* SALES sales amount 
*/
public function customer_sales_amount($id)
{
global $database;
$query="SELECT sum(amount)as amount FROM sales WHERE scid=".$id."";
$result=$database->query_fetch_full_result($query);
$result=$result[0];
$amount=$result['amount'];
return $amount;    
}
/*
* Total amount paid from Payment 
*/
public function customer_paid_amount_sales($id)
{
global $database;
$query="SELECT sum(amount_paid)as amount FROM payment WHERE customerID=".$id." and pay_type='S'";
$result=$database->query_fetch_full_result($query);
$result=$result[0];
$amount=$result['amount'];
return $amount;    
}
/*
* payable amount 
*/
public function customer_paid_amount_purchase($id)
{
global $database;
$query="SELECT sum(amount_paid)as amount FROM payment WHERE customerID=".$id." and pay_type='P'";
$result=$database->query_fetch_full_result($query);
$result=$result[0];
$amount=$result['amount'];
return $amount;    
}

/*
* payment record from custom date
*/
public function purchase_payment_custom($start,$end,$customerid)
{
global $database;
$customer="";
if($customerid!=0)
{
$customer="customerID=".$customerid." AND";    
}
$datequery="";
if($start!=0 || $end!=0)
{
$datequery="AND created_on BETWEEN ".strtotime($start)." AND ".strtotime($end)."";    
}
$query="SELECT * FROM `payment` WHERE ".$customer." pay_type='P' ".$datequery."  ORDER BY created_on DESC";

$result=$database->query_fetch_full_result($query);
return $result;    
}

/*
* payment record from custom date of sales
*/
public function sales_payment_custom($start,$end,$customerid)
{
global $database;
$customer="";
if($customerid!=0)
{
$customer="customerID=".$customerid." AND";    
}
$datequery="";
if($start!=0 || $end!=0)
{
$datequery="AND created_on BETWEEN ".strtotime($start)." AND ".strtotime($end)."";    
}
$query="SELECT * FROM `payment` WHERE ".$customer." pay_type='S' ".$datequery."  ORDER BY created_on DESC";

$result=$database->query_fetch_full_result($query);
return $result;    
}

      
/*
* setter and getter method 
*/
    public function get_id(){
        return $this->id;
    }

    public function set_id($id){
        $this->id = $id;
        $this->load_payment();
    }

    
    public function get_pay_type(){
        return $this->pay_type;
    }

    public function set_pay_type($pay_type){
        $this->pay_type = $pay_type;
    }

    public function get_customerid(){
        return $this->customerid;
    }

    public function set_customerid($customerid){
        $this->customerid = $customerid;
    }

    public function get_total_amount(){
        return $this->total_amount;
    }

    public function set_total_amount($total_amount){
        $this->total_amount = $total_amount;
    }

    public function get_invoiceid(){
        return $this->invoiceid;
    }

    public function set_invoiceid($invoiceid){
        $this->invoiceid = $invoiceid;
    }

    public function get_amount_paid(){
        return $this->amount_paid;
    }

    public function set_amount_paid($amount_paid){
        $this->amount_paid = $amount_paid;
    }

    public function get_payment_type(){
        return $this->payment_type;
    }

    public function set_payment_type($payment_type){
        $this->payment_type = $payment_type;
    }

    public function get_status(){
        return $this->status;
    }

    public function set_status($status){
        $this->status = $status;
    }

    public function get_use_customer_balance(){
        return $this->use_customer_balance;
    }

    public function set_use_customer_balance($use_customer_balance){
        $this->use_customer_balance = $use_customer_balance;
    }

    public function get_remark(){
        return $this->remark;
    }

    public function set_remark($remark){
        $this->remark = $remark;
    }

    public function get_created_on(){
        return date("m/d/Y",$this->created_on);
    }

    public function set_created_on($created_on){
        $this->created_on = strtotime($created_on);
    } 

    public function get_balancech(){
        return $this->balancech;
    }

    public function set_balancech($balancech){
        $this->balancech = $balancech;
    }
}



  
?>
