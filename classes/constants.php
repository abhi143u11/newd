<?php
define('DB_API_SERVER', 'localhost');
define('DB_API_USER', 'root');
define('DB_API_PASS', '');
define('DB_API_NAME', 'diamond');

define('APPNAME','newdiamond');
define('WEBSITE','newdiamond');
define('WEBSITEURL','http://localhost/newdiamond');
define('DASHBOARD','Dashboard');
define('DASHBOARDPURCHASEPAYMENT','Purchase Payment Payable');
define('DASHBOARDSALESPAYMENT','Sales Payment Receivable');
define('CUSTOMEREDIT','Customer Edit');
define('CUSTOMERADD','ADD Customer');
define('CUSTOMERALL','All Customers');
define('PURCHASEADD','Purchase Transaction');
define('PURCHASEEDIT','Purchase Transaction Edit');
define('PURCHASEALL','All Purchase Transaction');
define('SALESADD','Sales Transaction');
define('SALESEDIT','Sales Transaction Edit');
define('SALESALL','All Sales Transaction');
define('PROCESSTYPEALL','All Process Type');
define('PROCESSTYPEADD','Process Type Add');
define('PROCESSTYPEUPDATE','Process Type Update');
define('PROCESSALL','All Process');
define('PROCESSADD','Process  Add');
define('PROCESSUPDATE','Process  Update');
define('PAYMENTADD','Payment  Entry');
define('PAYMENTUPDATE','Payment  Edit');
define('PAYMENTALL','Payment  List');
define('REPORTCUSTOMERALL','Customer List Report');
define('REPORTSALESRALL','Sales Transaction Report ');
define('REPORTPURCHASESRALL','Purchase Transaction Report ');
define('REPORTPAYMENTRALL','Payment Transaction Report ');
define('REPORTPAYMENTRONECUSTOMER','Payment List Report ');
define('REPORTCUSTOMERPURCHASEINFO','Purchase Bill Report ');
define('REPORTCUSTOMERSALESINFO','Sales Bill Report ');
define('REPORTSTOCKINFO','Current Stock Report ');
define('REPORTAMOUTPAID','Purchase Amount Payable');
define('REPORTAMOUTRECEIVED','Sales Amount Receivable');
define('REPORTCUSTOMPURCHASE','Customer Purchase Information');
define('REPORTCUSTOMSALES','Customer Sales Information');
define('REPORTCUSTOMEXPENSES','Custom Expense Transaction');
define('REPORTCUSTOMACCOUNTHISTORY','Account History');
define('REPORTBALANCESHEET','Balancesheet');
define('EXPENSEADD','Expense Add');
define('EXPENSEEDIT','Expense Edit');
define('EXPENSELIST','All Expense Transaction');

define('VERSION','1.0');

//define('DB_API_USER', 'matrixso_general');
//define('DB_API_PASS', 'general@123');
//define('DB_API_NAME', 'matrixso_general');

  
?>
