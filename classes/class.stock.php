<?php
require_once('class.database.php');






class Ds_Stock
{
private   $stock_id;
private   $purchase_id;
private   $pcid;
private   $name;
private   $amount;
private   $pdate;
private   $duedate;
private   $rate;
private   $weight;
private   $weighttype;
private   $type;
private   $status;

/*
*  Constructor define 
*/
public function Ds_stock($stock_id = '')
    {
        if ($stock_id != '') {
            if($this->set_stock_id($stock_id)){
                if ($this->load_stocks())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    

    public function __destruct()
    {
        // TODO: destructor code
    }  




/*
*  load stock data 
*/
  public function load_stocks()
 {
        global $database;
        $query = "SELECT * FROM `stock_new` WHERE `stock_id`= " . $this->stock_id . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
        
        $result            = $result[0];
        $this->stock_id = $result['stock_id'];
        $this->purchase_id    = $result['purchase_id'];
        $this->pcid    = $result['pcid'];
        $this->name    = $result['name'];
        $this->amount    = $result['amount'];
        $this->pdate    = $result['pdate'];
        $this->duedate    = $result['duedate'];
        $this->rate    = $result['rate'];
        $this->weight    = $result['weight'];
        $this->weighttype     = $result['weighttype'];
        $this->type     = $result['type'];
        $this->status     = $result['status'];
        return TRUE;    
 }



/*
* 
* get all Stock data
*/ 
public function list_of_active_stock()
{
global $database;
$active=0;
$query_stock="SELECT * FROM `stock_new`";
$result=$database->query_fetch_full_result($query_stock);
return $result;    
} 

/*
*  get Stock information from Purchase ID 
*/
public function stock_info_from_purchase_id($pid)
{
global $database;
$active=0;
$query_stock="SELECT * FROM `stock_new` WHERE `purchase_id`=".$pid."";
$result=$database->query_fetch_full_result($query_stock);

return $result;    
}




/*
*  Setter and getter Method 
*/
    public function get_stock_id(){
        return $this->stock_id;
    }

    public function set_stock_id($stock_id){
        $this->stock_id = $stock_id;
        
    }

    public function get_purchase_id(){
        return $this->purchase_id;
    }

    public function set_purchase_id($purchase_id){
        $this->purchase_id = $purchase_id;
    }

    public function get_pcid(){
        return $this->pcid;
    }

    public function set_pcid($pcid){
        $this->pcid = $pcid;
    }

    public function get_name(){
        return $this->name;
    }

    public function set_name($name){
        $this->name = $name;
    }

    public function get_amount(){
        return $this->amount;
    }

    public function set_amount($amount){
        $this->amount = $amount;
    }

    

    public function get_pdate(){
     
        return date('m/d/Y',$this->pdate);
    }

    public function set_pdate($pdate){
        $this->pdate = strtotime($pdate);
    }
    
    public function set_duedate($duedate){
        $this->duedate = $duedate;
    }

    public function get_rate(){
        return $this->rate;
    }

    public function set_rate($rate){
        $this->rate = $rate;
    }

    public function get_weight(){
        return $this->weight;
    }

    public function set_weight($weight){
        $this->weight = $weight;
    }

    public function get_weighttype(){
        return $this->weighttype;
    }

    public function set_weighttype($weighttype){
        $this->weighttype = $weighttype;
    }

    public function get_type(){
        return $this->type;
    }

    public function set_type($type){
        $this->type = $type;
    }

    public function get_status(){
        return $this->status;
    }

    public function set_status($status){
        $this->status = $status;
    }

 
}



  
?>
