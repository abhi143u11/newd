<?php

require_once('class.database.php');


class Ds_Customer
{
    
    private $customer_id;
    private $customer_name;
    private $address;
    private $phoneno;
    private $companyname;
    private $balance;
    
    // TODO: Load customer
    // TODO: Add customer
    // TODO: Edit customer
    // TODO: delete customer
    
    
    public function Ds_customer($customer_id = '')
    {
        if ($customer_id != '') {
            if($this->set_customer_id($customer_id)){
                if ($this->load_customer())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    
    public function __destruct()
    {
        // TODO: destructor code
    }
    
    
    /*            
     * 
     *      LOAD CUSTOMER INFORMATION  
     * 
     * 
     */
    
    public function load_customer()
    {
        global $database;
        
        $query = "SELECT * FROM `customer` WHERE cid= '" . $this->customer_id . "'";
        
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
 
        $result            = $result[0];
        $this->customer_id = $result['cid'];
        $this->customer_name    = $result['name'];
        $this->address     = $result['address'];
        $this->phoneno     = $result['phoneno'];
        $this->companyname = $result['companyname'];
        $this->balance=$result['balance'];
        return TRUE;
    }
    /*
     *   Add Cutomer Information
     * 
     */
    
    public function add_customer()
    {
        global $database;
        $ary_err    = array();
        $ary_return = array();
        
        
        
        if ($this->name = '') {
            $ary_err[] = 'Customer name not entered';
        }
        
        if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }
        
        $query = "INSERT INTO `customer`(`name`,`address`,`phoneno`,`companyname`,`balance`) VALUES ('" . $this->customer_name . "','" . $this->address . "','" . $this->phoneno . "','" . $this->companyname . "',".$this->balance.")";
        
        
        $result = $database->query($query);
        
        if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;
    }
    
    /*
     *   Edit Customer 
     */
    
    
    public function update_customer()
    {
        global $database;
        
        $ary_err    = array();
        $ary_return = array();
        
        $query = "UPDATE `customer` SET `name`='" . $this->customer_name . "',`address`='" . $this->address . "',`phoneno`='" . $this->phoneno . "',`companyname`='" . $this->companyname . "',`balance`=".$this->balance." WHERE `cid`=" . $this->customer_id . "";
    
        $result = $database->query($query);
        
        if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        
        $ary_return[]      = FALSE;
        $ary_err["errors"] = $ary_err;
        return $ary_return;
        
    }
    
    /*
     * Delete Customer Data 
     */
    
    public function delete_customer()
    {
        global $database;
        $ary_err    = array();
        $ary_return = array();
        $query      = "DELETE FROM CUSTOMER WHERE `cid`=" . $this->customer_id . "";
        
        $result = $database->query($query);
        
        if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        
        $ary_return[]      = FALSE;
        $ary_err["errors"] = $ary_err;
        return $ary_return;
        
    }
    
    /*
    *   show all data of Customer
    */
    
    public function all_list_customer()
    {
    global $database;
    $query="SELECT * FROM CUSTOMER";
    $result=$database->query_fetch_full_result($query);
    return $result;     
    }
    
    public function get_customer_byid($id)
    {
    global $database;
    $query="SELECT * FROM CUSTOMER WHERE cid=".$id."";
    $result=$database->query_fetch_full_result($query);
    return $result;     
    }
    
    /*
     * 
     * 
     * setter amd getter method    
     * 
     */
    
    
    
    
    public function get_customer_id()
    {
        return $this->customer_id;
    }
    
    public function set_customer_id($customer_id)
    {
        $this->customer_id = $customer_id;
        $this->load_customer();
    }
    
    public function get_customer_name()
    {
        return $this->customer_name;
    }
    
    public function set_customer_name($customer_name)
    {
        $this->customer_name = $customer_name;
    }
    
    public function get_address()
    {
        return $this->address;
    }
    
    public function set_address($address)
    {
        $this->address = $address;
    }
    
    public function get_phoneno()
    {
        return $this->phoneno;
    }
    
    public function set_phoneno($phoneno)
    {
        $this->phoneno = $phoneno;
    }
    
    public function get_companyname()
    {
        return $this->companyname;
    }
    
    public function set_companyname($companyname)
    {
        $this->companyname = $companyname;
    }
    
    public function get_balance(){
        return $this->balance;
    }

    public function set_balance($balance){
        $this->balance = $balance;
    }
    
    
}

?>