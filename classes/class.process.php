<?php
require_once('class.database.php');



class Ds_Process
{  
   
private $process_id;
private $purchase_id;
private $ptname;
private $amount;
private $weight;
/*
*
* constructor declared 
*/
public function Ds_process($process_id = '')
    {
        if ($process_id != '') 
        {
            if($this->set_process_id($process_id)){
                if ($this->load_process())
                    {return TRUE;
                    }else{
                    return FALSE;}
            } else
                return FALSE;
        }
    }
    
    public function __destruct()
    {
        // TODO: destructor code
    }  
 
/*
*   load process type
*/ 
 public function load_process()
 {
        global $database;
        $query = "SELECT * FROM `process` WHERE `process_id` = " . $this->process_id . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
        $result            = $result[0];
        $this->process_id = $result['process_id'];
        $this->purchase_id    = $result['purchase_id'];
        $this->ptname = $result['process_type'];
        $this->amount    = $result['amount'];
        $this->weight    = $result['weight'];
        return TRUE;    
 }

 /*
 * add processs 
 */
 public function add_process()
 {
        global $database;
        $ary_err    = array();
        $ary_return = array();
   
      
        
     $query = "INSERT INTO `process`(`purchase_id`, `process_type`, `amount`, `weight`) VALUES(".$this->purchase_id.",".$this->ptname.",".$this->amount.",".$this->weight.")";
     $result = $database->query($query);
/*
* calculate  for new_stock
*/   

     $query_select_stock="SELECT * FROM `stock_new` WHERE `purchase_id` =".$this->purchase_id."";
     $query_stock_result=$database->query_fetch_full_result($query_select_stock);
     $stock_rate=0;          
     $stock_weight=0;          
     $stock_amount=0;          
     
     $query_stock_result=$query_stock_result[0];
    
     $stock_rate=$query_stock_result['rate'];          
     $stock_weight=$query_stock_result['weight'];          
     $stock_amount=$query_stock_result['amount'];          
     
     
     $stock_amount=$stock_amount+$this->amount;
     $stock_rate=$stock_amount/$this->weight;
     $stock_weight=$this->weight;
     
     $query_update_stock="UPDATE `stock_new` SET `rate`=".$stock_rate.",`weight`=".$stock_weight.",`amount`=".$stock_amount." WHERE `purchase_id`=".$this->purchase_id."";   
     $stock_update_result=$database->query($query_update_stock);
     
          
     if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;        
     
     
 }


 /*
 * update processs 
 */
 public function update_process()
 {
     global $database;
        $ary_err    = array();
        $ary_return = array();
   
   
        
     $query = "UPDATE  `process` SET `purchase_id`=".$this->purchase_id.", `process_type`=".$this->ptname.", `amount`=".$this->amount.", `weight`=".$this->weight." WHERE `process_id`=".$this->process_id."";
     $result = $database->query($query);
     
 /*
* calculate  for new_stock   for Updatation
*/   

     $query_select_stock="SELECT * FROM `stock_new` WHERE `purchase_id` =".$this->purchase_id."";
     $query_stock_result=$database->query_fetch_full_result($query_select_stock);
     $stock_rate=0;          
     $stock_weight=0;          
     $stock_amount=0;          
     $query_stock_result=$query_stock_result[0];
     $stock_rate=$query_stock_result['rate'];          
     $stock_weight=$query_stock_result['weight'];          
     $stock_amount=$query_stock_result['amount'];          
     $stock_amount=$stock_amount+$this->amount;
     $stock_rate=$stock_amount/$this->weight;
     $stock_weight=$this->weight;
     
     $query_update_stock="UPDATE `stock_new` SET `rate`=".$stock_rate.",`weight`=".$stock_weight.",`amount`=".$stock_amount." WHERE `purchase_id`=".$this->purchase_id."";   
     $stock_update_result=$database->query($query_update_stock);
         
          
     if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;        
     
         
     
     
 }
  
 /*
 * delete processs 
 */
 public function delete_process()
 {
     
   global $database;
   $ary_err    = array();
   $ary_return = array();
   $query="DELETE FROM `process` WHERE `process_id`=".$this->process_id."";  
   $result=$database->query($query);     
      if ($result) {
            $ary_return["success"] = TRUE;
            return $ary_return;
        }
        
        $ary_return[]      = FALSE;
        $ary_err["errors"] = $ary_err;
        return $ary_return;
    return $ary_return;  
 }
 
 /*
 * get all information of processs 
 */
 public function list_of_process_data()
 {
   global $database;
   $query="SELECT * FROM `process`";  
   $result=$database->query_fetch_full_result($query);
   return $result;  
 }
 

           
  
/*
*   Setter and getter method  
*/
    public function get_process_id(){
        return $this->process_id;
    }

    public function set_process_id($process_id){
        $this->process_id = $process_id;
        $this->load_process();
    }

    public function get_purchase_id(){
        return $this->purchase_id;
    }

    public function set_purchase_id($purchase_id){
        $this->purchase_id = $purchase_id;
    }

    public function get_ptname(){
        return $this->ptname;
    }

    public function set_ptname($ptname){
        $this->ptname = $ptname;
    }

    public function get_amount(){
        return $this->amount;
    }

    public function set_amount($amount){
        $this->amount = $amount;
    }

    public function get_weight(){
        return $this->weight;
    }

    public function set_weight($weight){
        $this->weight = $weight;
    }
    
}

   
?>
