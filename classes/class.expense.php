<?php
require_once('class.database.php');


class Ds_Expense
{
private $id;
private $pay_type;
private $amount_paid;
private $remark;
private $created_on;





/*
*  Constructor define 
*/
public function Ds_expense($id = '')
    {
        if ($id != '') {
            if($this->set_id($id)){
                if ($this->load_payment())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    

    public function __destruct()
    {
        // TODO: destructor code
    }
    
    
/*
* load payment data 
*/
  public function load_expense()
 {
        global $database;
        $query = "SELECT * FROM `payment` WHERE `id`= " . $this->id . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
        
        $result            = $result[0];
        $this->id = $result['id'];
        $this->pay_type    = $result['pay_type'];
        $this->amount_paid    = $result['amount_paid'];
        $this->remark     = $result['remark'];
        $this->created_on     = $result['created_on'];   
        return TRUE;    
 }


/*
* add expence 
*/ 
 public function add_expense()
 {
 global $database;
 $this->pay_type="E";
 $query = "INSERT INTO `payment`(`pay_type`,`amount_paid`,`remark`,`created_on`) VALUES('".$this->pay_type."',".$this->amount_paid.",'".$this->remark."',".$this->created_on.")";    
 $result=$database->query($query);
 return $result;
 }

/*
* add expence 
*/ 
 public function update_expense()
 {
 global $database;
 $query = "UPDATE `payment` SET `pay_type`='E',amount_paid=".$this->amount_paid.",`remark`='".$this->remark."',`created_on`=".$this->created_on." WHERE `id`= " . $this->id . "";    
 $result=$database->query($query);
 return $result;
 }
 
/*
* list of all expence data 
*/ 
 public function list_of_all_expenses()
 {
 global $database;
 $query = "SELECT * FROM `payment` WHERE `pay_type`='E'";    
 $result=$database->query_fetch_full_result($query);
 return $result;
 }
 
 /*
 * list of all expenses from between two dates 
 */
 public function list_of_all_expenses_from_date($first,$second)
 {
 global $database;
 $query="";
 if($first!=0)
 {
 $query = "SELECT * FROM `payment` WHERE `pay_type`='E' AND created_on BETWEEN ".strtotime($first)." AND ".strtotime($second)."";    
 }
 else
 {
 $query = "SELECT * FROM `payment` WHERE `pay_type`='E'";    
 }
 $result=$database->query_fetch_full_result($query);
 return $result;
 } 
/*
* setter and getter method 
* 
*/
    public function get_id(){
        return $this->id;
    }

    public function set_id($id){
        $this->id = $id;
        $this->load_expense();
    }

    public function get_pay_type(){
        return $this->pay_type;
    }

    public function set_pay_type($pay_type){
        $this->pay_type = $pay_type;
    }

    public function get_amount_paid(){
        return $this->amount_paid;
    }

    public function set_amount_paid($amount_paid){
        $this->amount_paid = $amount_paid;
    }

    public function get_remark(){
        return $this->remark;
    }

    public function set_remark($remark){
        $this->remark = $remark;
    }

    public function get_created_on(){
        return $this->created_on;
    }

    public function set_created_on($created_on){
        $this->created_on = strtotime($created_on);
    }

}
?>