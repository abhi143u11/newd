<?php

require_once('class.database.php');



class Ds_Sales
{
private $sid;
private $scid;
private $pid;
private $broker;
private $brokertype;
private $brokeragevalue;
private $brokerageamount;
private $amount;
private $pdate;
private $no_of_days;
private $duedate;
private $rate;
private $weight;
private $weighttype;
private $type;
private $status; 
private $oldweigth;

/*
* constructor declared 
*/
public function Ds_sales($sid = '')
    {
        if ($sid != '') {
            if($this->set_sid($sid)){
                if ($this->load_sales())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }
    

    public function __destruct()
    {
        // TODO: destructor code
    }  
 
/*
*   load sales orders
*/ 
 
 public function load_sales()
 {
        global $database;
        $query = "SELECT * FROM `sales` WHERE `sid`= " . $this->sid . "";
        $result = $database->query_fetch_full_result($query);
        
        if (!$result) {
            return FALSE;
        }
   
        $result            = $result[0];
        $this->sid = $result['sid'];
        $this->scid    = $result['scid'];
        $this->pid    = $result['pid'];
        $this->broker    = $result['broker'];
        $this->brokertype    = $result['brokeragetype'];
        $this->brokeragevalue    = $result['brokeragevalue'];
        $this->brokerageamount    = $result['brokerageamount'];
        $this->amount    = $result['amount'];
        $this->pdate    = $result['pdate'];
        $this->duedate     = $result['duedate'];
        $this->no_of_days = $result['no_of_days'];
        $this->rate = $result['rate'];
        $this->weight    = $result['weight'];
        $this->weighttype     = $result['weighttype'];
        $this->type     = $result['type'];
        $this->status     = $result['status'];
        return TRUE;    
 }
 
/*
*  add sales order transaction 
*/    
public function add_sales()
{
    global $database; 
  
        global $database;
        $ary_err    = array();
        $ary_return = array();
   
      
        if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }
   
        $actualdate=$this->pdate;
        $no_of_days = $this->no_of_days * 86400;
        $total = $actualdate + $no_of_days;
        $duedate = date('Y-m-d', $total);
        $amount=$this->rate*$this->weight;
        $actualamount=$amount;
        
   
        if($this->brokertype=="PERCENTAGE")
        {
           $brokerageamount=($amount * $this->brokeragevalue)/100;
           $amount=$amount-$brokerageamount;
           $this->brokerageamount=$brokerageamount;
        }
        else
        {
            $brokerageamount=$this->brokeragevalue;
            $amount=$amount-$brokerageamount;
            $this->brokerageamount=$brokerageamount;    
        }
        
        if($this->broker==0)
        {
          $this->brokeragevalue=0;
          $this->brokerageamount=0;
          $amount=$actualamount;  
        }
        
        $this->amount=$amount;
        $query = "INSERT INTO `sales`(`scid`, `pid`, `broker`, `brokeragetype`, `brokeragevalue`, `brokerageamount`, `amount`, `pdate`, `no_of_days`, `duedate`, `rate`, `weight`, `weighttype`, `type`, `status`) VALUES (" . $this->scid . ",".$this->pid.",".$this->broker.",'".$this->brokertype."',".$this->brokeragevalue.",".$this->brokerageamount."," . $this->amount . "," . $this->pdate . ",'" . $this->no_of_days . "','" . $duedate . "'," . $this->rate . "," . $this->weight . ",'" . $this->weighttype . "','" . $this->type . "'," . $this->status . ")";
        $result = $database->query($query);
      
        
       
        $query_stock_decrement="UPDATE `stock_new` SET `weight`=weight-".$this->weight." WHERE `purchase_id`=".$this->pid."";
        
        if ($result)
        {
            $ary_return["success"] = TRUE;
            $result_stock_decrement=$database->query($query_stock_decrement);
           
            $query_stock_status="SELECT * FROM `stock_new` WHERE `purchase_id`=".$this->pid."";
            $stock_status_fetch=$database->query_fetch_full_result($query_stock_status);
            $stock_status_fetch=$stock_status_fetch[0];
            $fetch_stock=$stock_status_fetch['weight'];
 
            if($fetch_stock==0)
            {
            $query_status_update="UPDATE `stock_new` SET `status`=".$fetch_stock." WHERE `purchase_id`=".$this->pid."";     
            $stock_status_update=$database->query($query_status_update);    
            }
            return $ary_return;
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;  
}   

/*
* update sales order transaction 
*/ 

public function update_sales()
{
    global $database; 
  
        global $database;
        $ary_err    = array();
        $ary_return = array();
   
      
        if (count($ary_err) > 0) {
            $ary_return["success"] = FALSE;
            $ary_err["errors"]     = $ary_err;
            return $ary_return;
        }
   
        $actualdate=$this->pdate;
        $no_of_days = $this->no_of_days * 86400;
        $total = $actualdate + $no_of_days;
        $duedate = date('Y-m-d', $total);
        $amount=$this->rate*$this->weight;
        $actualamount=$amount;
   
        if($this->brokertype=="PERCENTAGE")
        {
           $brokerageamount=($amount * $this->brokeragevalue)/100;
           $amount=$amount-$brokerageamount;
           $this->brokerageamount=$brokerageamount;
        }
        else
        {
            $brokerageamount=$this->brokeragevalue;
            $amount=$amount-$brokerageamount;
            $this->brokerageamount=$brokerageamount;    
        }
        
        if($this->broker==0)
        {
          $this->brokeragevalue=0;
          $this->brokerageamount=0;
          $amount=$actualamount;  
        }
        
        $this->amount=$amount;
        $query = "UPDATE  `sales` SET `scid`=".$this->scid.", `pid`=".$this->pid.", `broker`=".$this->broker.", `brokeragetype`='".$this->brokertype."', `brokeragevalue`=".$this->brokeragevalue.", `brokerageamount`=".$this->brokerageamount.", `amount`=".$this->amount.", `pdate`=".$this->pdate.", `no_of_days`='".$this->no_of_days."', `duedate`='".$duedate."', `rate`=".$this->rate.", `weight`=".$this->weight.", `weighttype`='".$this->weighttype."', `type`='".$this->type."', `status`=".$this->status." WHERE `sid`=".$this->sid."";
       
       
        $result = $database->query($query);
     
     
        $current_stock=0;  //its take only for calculation  
              
        if($this->weight>=$this->oldweigth)
        {
        $current_stock=$this->weight-$this->oldweigth;    
        $query_stock_decrement="UPDATE `stock_new` SET `weight`=weight-".$current_stock." WHERE `purchase_id`=".$this->pid."";
        }
        else
        {
        $current_stock=$this->oldweigth-$this->weight;    
        $query_stock_decrement="UPDATE `stock_new` SET `weight`=weight+".$current_stock." WHERE `purchase_id`=".$this->pid."";
        }      
       
      
        if ($result)
        {
            $ary_return["success"] = TRUE;
            $result_stock_decrement=$database->query($query_stock_decrement);
            $query_stock_status="SELECT * FROM `stock_new` WHERE `purchase_id`=".$this->pid."";
            $stock_status_fetch=$database->query_fetch_full_result($query_stock_status);
            $stock_status_fetch=$stock_status_fetch[0];
            $fetch_stock=$stock_status_fetch['weight'];
            if($fetch_stock==0)
            {
            $query_status_update="UPDATE `stock_new` SET `status`=".$fetch_stock." WHERE `purchase_id`=".$this->pid."";     
            $stock_status_update=$database->query($query_status_update);    
            }
       
            return $ary_return;
            
        }
        $ary_return["success"] = FALSE;
        $ary_err["errors"]     = $ary_err;
        return $ary_return;  
}

/*
*  delete sales transaction bill 
*/
public function delete_sales()
{
    global $database;
    $query="DELETE * FROM `sales` WHERE `sid`=".$this->sid."";
    $result=$database->query($query);
    return $result;
}

/*
*  get_ all sales bills result
*/
public function list_of_all_sales()
{
    global $database;
    $query="SELECT * FROM `sales`";
    $result=$database->query_fetch_full_result($query);
    return $result;
}

/*
* list of sales list with customer name 
*/
public function list_of_all_sales_with_name()
{
    global $database;
    $query="SELECT a.*,b.name FROM sales a INNER JOIN customer b WHERE a.scid=b.cid";
    $result=$database->query_fetch_full_result($query);
    return $result;
}

public function list_of_all_sales_with_name_desc($customerid)
{
    global $database;
    $query="SELECT a.*,b.name FROM sales a INNER JOIN customer b WHERE a.scid=b.cid and a.scid=".$customerid." order by pdate desc";
    $result=$database->query_fetch_full_result($query);
    return $result;
}


/*
* get Broker name 
*/
public function sales_broker_name($cid)
{
    global $database;
    $query="SELECT * FROM customer WHERE cid=".$cid."";
    $result=$database->query_fetch_full_result($query);
    $result=$result[0];
    $brokername=$result['name'];
    return $brokername;
}

/*
* get all information  fro one sales id 
*/
public function sales_info_fromid($saleid)
{
    global $database;
    $query="SELECT * FROM sales  WHERE  sid=".$saleid."" ;
    $result=$database->query_fetch_full_result($query);
    return $result;
}

/*
*  get Sales custom information
*/
public function sales_info_from_custom($start,$end,$customerid)
{
    global $database;
    $query="";
    if($customerid!=0 && $start!=0 )
    {
    $query="SELECT * FROM sales  WHERE scid=".$customerid." AND pdate BETWEEN ".strtotime($start)." AND ".strtotime($end)."" ;
    } 
    else if($customerid!=0 && $start==0)
    {
    $query="SELECT * FROM sales  WHERE scid=".$customerid."" ;
    }
    else if($start!=0 && $customerid==0)
    {
    $query="SELECT * FROM sales  WHERE pdate BETWEEN ".strtotime($start)." AND ".strtotime($end)."" ;
    }
    else if($customerid==0 && $start==0)
    {
    $query="SELECT * FROM sales";    
    }                                   
   
    $result=$database->query_fetch_full_result($query);
    return $result;
                               
}

/*
* all sales information from date 
*/
public function sales_get_all_date($start,$end)
{
    global $database;
    $query="";
    if($start!=0 && $end!=0)
    {
    $query="SELECT DISTINCT pdate  FROM sales WHERE pdate BETWEEN ".strtotime($start)." AND ".strtotime($end)."" ;
    }
    else
    {  
    $query="SELECT DISTINCT pdate FROM sales" ;
    }
    $result=$database->query_fetch_full_result($query);
    return $result;
}

/*
* get total sales amount from date 
*/
public function sales_get_total_amount_from_date($date)
{
    global $database;
    $query="SELECT sum(amount) as amount FROM sales WHERE pdate=".$date."" ;
    $result=$database->query_fetch_full_result($query);
    $result=$result[0];
    return $result['amount'];
}


/*
* set_ter and get_ter method of selling  
*/    
       public function get_sid(){
        return $this->sid;
    }

    public function set_sid($sid){
        $this->sid = $sid;
        $this->load_sales();
    }

    public function get_scid(){
        return $this->scid;
    }

    public function set_scid($scid){
        $this->scid = $scid;
    }

    public function get_pid(){
        return $this->pid;
    }

    public function set_pid($pid){
        $this->pid = $pid;
    }

    public function get_broker(){
        return $this->broker;
    }

    public function set_broker($broker){
        $this->broker = $broker;
    }

    public function get_brokertype(){
        return $this->brokertype;
    }

    public function set_brokertype($brokertype){
        $this->brokertype = $brokertype;
    }

    public function get_brokeragevalue(){
        return $this->brokeragevalue;
    }

    public function set_brokeragevalue($brokeragevalue){
        $this->brokeragevalue = $brokeragevalue;
    }

    public function get_brokerageamount(){
        return $this->brokerageamount;
    }

    public function set_brokerageamount($brokerageamount){
        $this->brokerageamount = $brokerageamount;
    }

    public function get_amount(){
        return $this->amount;
    }

    public function set_amount($amount){
        $this->amount = $amount;
    }



    public function get_no_of_days(){
        return $this->no_of_days;
    }

    public function set_no_of_days($no_of_days){
        $this->no_of_days = $no_of_days;
    }

    public function get_duedate(){
        return $this->duedate;
    }

    public function set_duedate($duedate){
        $this->duedate = $duedate;
    }

    public function get_rate(){
        return $this->rate;
    }

    public function set_rate($rate){
        $this->rate = $rate;
    }

    public function get_weight(){
        return $this->weight;
    }

    public function set_weight($weight){
        $this->weight = $weight;
    }

    public function get_weighttype(){
        return $this->weighttype;
    }

    public function set_weighttype($weighttype){
        $this->weighttype = $weighttype;
    }

    public function get_type(){
        return $this->type;
    }

    public function set_type($type){
        $this->type = $type;
    }

    public function get_status(){
        return $this->status;
    }

    public function set_status($status){
        $this->status = $status;
    }
 
 
    public function get_pdate(){
        return  date('m/d/Y',$this->pdate);
    }

    public function set_pdate($pdate){
        $this->pdate =strtotime($pdate);
    }

    public function get_oldweigth(){
        return $this->oldweigth;
    }

    public function set_oldweigth($oldweigth){
        $this->oldweigth = $oldweigth;
    }
  
}      
?>
