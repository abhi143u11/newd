<?php

require_once('class.database.php');


class Ds_product{

    private $product_id;
    private $product_name;
    private $bag;
    private $qty_stock;
    private $pcs;
    private $rate;

   

    public function Ds_product($product_id = '') {
        if ($product_id != '') {
            if ($this->set_id($product_id)) {
                if ($this->load_product())
                    return TRUE;
                else
                    return FALSE;
            } else
                return FALSE;
        }
    }

    public function __destruct() {
        // TODO: destructor code
    }

    public function get_product_details() {
        global $database;
        $query = "SELECT SELECT * FROM `product` where `product_id`='" . $this->id . "'";
        $products = $database->query_fetch_full_result($query);
        return $products;
    }

    /*
     * ======================================================
     *               Class Level PRIVATE functions
     * ======================================================
     */

    private function load_product() {
        global $database;
            $query = "SELECT * FROM `product` JOIN `product_child` on `product`.`product_id`=`product_child`.`product_id` WHERE `product_child`.`product_id`='" . $this->id . "'";
        $result = $database->query_fetch_full_result($query);

        if (!$result)
            return FALSE;

        $result = $result[0];
        $this->product_id=$result['product_id'];
        $this->product_name=$result['product_name'];
        $this->bag=$result['bag'];
        $this->qty_stock=$result['qty_stock'];
        $this->pcs=$result['pcs'];
        $this->rate=$result['rate'];

        return TRUE;
    }

    /*
     * ======================================================
     *               ADD Customer function
     * ======================================================
     */   
       
        public function add_product()
        {
            global $database;
            $ary_err = array();
            $ary_return = array();
            /* Data Validation Start */
            
                if($this->product_name == '')
                {
                    $ary_err[] = "product name not entered";
                }
                
                                               
                if(count($ary_err) > 0)
                {
                    $ary_return["success"] = FALSE;
                    $ary_return["errors"] = $ary_err;
                    return $ary_return;
                }
                $query = "INSERT INTO `product`(`product_name`)VALUES ('".$this->product_name."')";
      
                $result = $database->query($query);
                if($result)
                {
                    
                    $lastid=$database->get_last_insert_id();
                    $this->set_product_id($lastid);
                    
                    $res=$this->add_product_child();
                    if($res){
                            $ary_return["success"] = TRUE;
                    return $ary_return;
                    }
                }
                $ary_return["success"] = FALSE;
                $ary_return["errors"] = $ary_err;                                                          
                return $ary_return;
                 
        }
        
         public function add_product_child()
        {
            global $database;
            $ary_err = array();
            $ary_return = array();
            /* Data Validation Start */
            
                if($this->bag == '')
                {
                    $ary_err[] = "bag not entered";
                }
                if($this->qty_stock == '')
                {
                    $ary_err[] = "qty stock not entered";
                }
                if($this->pcs == '')
                {
                    $ary_err[] = "pcs not entered";
                }
                if($this->rate == '')
                {
                    $ary_err[] = "rate not entered";
                }
                
                                               
                if(count($ary_err) > 0)
                {
                    $ary_return["success"] = FALSE;
                    $ary_return["errors"] = $ary_err;
                    return $ary_return;
                }
                $query = "INSERT INTO `product_child`(`product_id`, `bag`, `qty`, `pcs`, `rate`) VALUES ('".$this->product_id."','".$this->bag."','".$this->qty_stock."','".$this->pcs."','".$this->rate."')";
                $result = $database->query($query);
                if($result)
                { 
                    $ary_return["success"] = TRUE;
                    return $ary_return;
                }
                $ary_return["success"] = FALSE;
                $ary_return["errors"] = $ary_err;                                                          
                return $ary_return;
                 
        }
     /*
     * ======================================================
     *               Get All Product Detail functions
     * ======================================================
     */        
        
         public function get_only_product_list() {
        global $database;
        $query = "SELECT * FROM `product`";
        $result = $database->query_fetch_full_result($query);
        return $result;
        }
        
        public function get_product_list() {
        global $database;
        $query = "SELECT * FROM `product` JOIN `product_child` ON `product_child`.`product_id`=`product`.`product_id`";
        $result = $database->query_fetch_full_result($query);
        return $result;
        }
        
        public function get_one_product_list() {
        global $database;
        $query = "SELECT * FROM `product` where `product_name`='".$this->product_name."'";
        $result = $database->query_fetch_full_result($query);
        return $result;
        }
        
        public function get_stock_quantity($stock,$product_id) {
        global $database;
        $query = "SELECT * FROM `product_child` WHERE `product_id`='$product_id' and `qty`='$stock'";
        $result = $database->query_fetch_full_result($query);
        return $result;
        }
        
        public function get_product_details_by_id($id) {
        global $database;
        $query = "SELECT * FROM `product` JOIN `product_child` ON `product_child`.`product_id`=`product`.`product_id` where `product`.`product_id`='".$id."'";
        $result = $database->query_fetch_full_result($query);
       
        return $result;
        }
        
         public function get_product_details_id($id) {
        global $database;
        $query = "SELECT * FROM `product_child`  where `product_id`='".$id."'";
        $result = $database->query_fetch_full_result($query);
       
        return $result;
        }
    /*
     * ======================================================
     *               Delete Product Detail functions
     * ======================================================
     */    
        
        public function delete_product()
        {
            global $database;
            $query = "DELETE FROM `product` WHERE `product_id`='".$this->id."'";
            $result = $database->query($query);
            return $result;
        }
      
        
        
        /*
     * ======================================================
     *               Update Customer Detail functions
     * ======================================================
     */
       public function update_product_stock($newbag,$newpcs,$pro_child_id)
        {
            global $database;
            $ary_err = array();
            $ary_return = array();
            
            $query = "UPDATE `product_child` SET `bag`='$newbag',`pcs`='$newpcs' WHERE `pro_child_id`='$pro_child_id'";   
            $result = $database->query($query);
            if($result)
            {
                $ary_return["success"] = TRUE;
                return $ary_return;
            }else
            {
            $ary_return["success"] = FALSE;
            $ary_return["errors"] = $ary_err;
            return $ary_return;
            }
        }    
        
        
        
        
    /*
     * ======================================================
     *               Get and Set functions
     * ======================================================
     */
        
    private function mask_id() {
        $prefix = 'PRO';
        $id = $this->id;
        while (strlen($id) < 8) {
            $id = '0' . $id;
        }
        return $prefix . $id;
    }

    private function unmask_id($id) {
        $id = str_replace('PRO', '', $id);
        return (int) $id;
    }

    public function get_id() {
        return $this->mask_id();
    }

    public function set_id($id) {
        $id = $this->unmask_id($id);
        if ($id == is_numeric($id) && $id > 0) {
            $this->id = $id;
            $this->load_product();
        }
        return TRUE;
    }

     
     public function get_product_id() {
        return $this->product_id;
    }
    
    public function get_product_name() {
        return $this->product_name;
    }
    
    public function get_bag() {
        return $this->bag;
    }
    
    public function get_qty() {
        return $this->qyt;
    }

    public function get_pcs() {
        return $this->pcs;
    }

    public function get_rate() {
        return $this->rate;
    }

    public function set_product_id($product_id) {
        $this->product_id = $product_id;
    }

    public function set_product_name($product_name) {
        $this->product_name = $product_name;
    }

    public function set_bag($bag) {
        $this->bag = $bag;
    }
    
    public function set_qty($qty) {
        $this->qty_stock = $qty;
    }

    public function set_pcs($pcs) {
        $this->pcs = $pcs;
    }

    public function set_rate($rate) {
        $this->rate = $rate;
    }

}

?>
