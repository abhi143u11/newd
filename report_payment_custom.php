<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.payment.php');
require_once('classes/class.purchase.php');
require_once('classes/class.sales.php');
require_once('classes/class.customer.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}


$start=0;
$second=1;
$customerid=0;
if(isset($_GET['f']))
{
$start=$_GET['f'];
$second=$_GET['s'];
$customerid=$_GET['customername'];    
}

/*
* get Customer name 
*/
$customer_name=new Ds_Customer();
$customer_list=$customer_name->all_list_customer();
/*
*  get Purchase Information
*/
$purchase_custom=new Ds_Purchase();

/*
* get Sales Information
*/
$sales_custom=new Ds_Sales();


/*
* declare object of Payment Class 
*/
$payment_record=new Ds_Payment();
$payment_purchase_custom_result=$payment_record->purchase_payment_custom($start,$second,$customerid);
$payment_sales_custom_result=$payment_record->sales_payment_custom($start,$second,$customerid);
/*
* declare sales totalamoutn and total paid amount 
*/
 $sales_total_amount=0.00;
 $sales_pay_amount=0.00;
/*
* declare purchase totalamoutn and total paid amount 
*/
 $purchase_total_amount=0.00;
 $purchase_pay_amount=0.00;

?>
<?php include('header.php'); ?>

<style type="text/css"> 
</style>
     
        <section class="content">
                               
 
 
        
 <div class="col-md-12"  >
                <div class="box-header">
                  <h3 class="box-title"><?php echo "Customer Payment Ledger Report  ";    ?> </h3>
                </div><!-- /.box-header -->  
  
                    <div class="form-group col-md-2">
                    
                     <div class="input-group">
                      <button class="btn btn-default pull-right" id="daterange-btn">
                        <i class="fa fa-calendar"></i> Date range picker
                        <i class="fa fa-caret-down"></i>
                      </button>
                    </div>
                  
                    <form name="f1">
                     <input type="hidden" name="f" /><input type="hidden" name="s" />                  
                    
                   </div>   
         
                   
                     <div class="col-md-3">
                       <select id="customername" name="customername" class="form-control">
                       <option value="0">Select Customer Name</option>
                       <?php
                        if(!empty($customer_list))
                        {
                            foreach($customer_list as $val)
                            {
                             echo "<option value=".$val['cid'].">".$val['name']."</option>";
                            }
                        }
                        ?>
                         </select>
                          </div>
                   
                   <div class="col-md-2">
                    <input type="submit" class="btn btn-primary" onclick="getdate()" type="button" name="loaddata" id="loaddata" value="Load"/>
                     
                   </div>
                        
               </form> 
 </div>
 
<div class="col-md-12"  >
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTCUSTOMPURCHASE;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<table id="example2" class="table table-bordered table-striped">  
<thead>
<tr>
<th>Payment No</th>
<th>Date</th>
<th>Name</th>
<th>Weight Type</th>
<th>Rate</th>
<th>Weight</th>
<th>Bill No</th>
<th>Days</th>
<th>Paid Amount</th>

</tr>
</thead>
<?php
if(!empty($payment_purchase_custom_result))
{     
foreach($payment_purchase_custom_result as $obj)
{
$name=$customer_name->get_customer_byid($obj['customerID']);
$name=$name[0];    
$purchase_info=$purchase_custom->purchase_info_fromid($obj['invoiceID']);
$purchase_info=$purchase_info[0];

$date=date("Y-m-d",$obj['created_on']);

$d=date("Y-m-d",time());
$date1 = new DateTime($d);
$date2 = new DateTime($date);
$diff = $date1->diff($date2);
$dueday=$diff->days;
if($dueday==0){$dueday="Today";}else if($dueday==1){$dueday="Yesterday";}





    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".date('d/m/Y',$obj['created_on'])."</td>";
    echo "<td>".$name['name']."</td>";
    echo "<td>".$purchase_info['weighttype']."</td>";
    echo "<td>".$purchase_info['rate']."</td>";
    echo "<td>".$purchase_info['weight']."</td>";
    echo "<td>".$obj['invoiceID']."</td>";
    echo "<td>".$dueday."</td>";
    echo "<td>".$obj['amount_paid']."</td>";
    echo "</tr>";   
 

$purchase_total_amount=$purchase_total_amount+$obj['amount_paid'];
    
    
}    
}    
?>
<tr>
<th colspan="7">Total Amount</th>
<th ></th>
<th colspan="3"><?php 

echo number_format($purchase_total_amount,'2','.',',');


  ?></th>





</tr>
</table>
</div>
</div>
</div>



<!-- start sales report  -->

<div class="col-md-12"  >
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTCUSTOMSALES;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<table id="example2" class="table table-bordered table-striped">  
<thead>
<tr>
<th>Payment No</th>
<th>Date</th>
<th>Name</th>
<th>Weight Type</th>
<th>Rate</th>
<th>Weight</th>
<th>Bill No</th>
<th>Days</th>
<th>Paid Amount</th>

</tr>
</thead>
<?php
if(!empty($payment_sales_custom_result))
{     
foreach($payment_sales_custom_result as $obj)
{
$name=$customer_name->get_customer_byid($obj['customerID']);
$name=$name[0];
$sid=$obj['invoiceID'];    
$sales_data=$sales_custom->sales_info_fromid($sid);

$sales_data=$sales_data[0];

$date=date("Y-m-d",$obj['created_on']);

$d=date("Y-m-d",time());
$date1 = new DateTime($d);
$date2 = new DateTime($date);
$diff = $date1->diff($date2);
$dueday=$diff->days;
if($dueday==0){$dueday="Today";}else if($dueday==1){$dueday="Yesterday";}





    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".date('d/m/Y',$obj['created_on'])."</td>";
    echo "<td>".$name['name']."</td>";
    echo "<td>".$sales_data['weighttype']."</td>";
    echo "<td>".$sales_data['rate']."</td>";
    echo "<td>".$sales_data['weight']."</td>";
    echo "<td>".$obj['invoiceID']."</td>";
    echo "<td>".$dueday."</td>";
    echo "<td>".$obj['amount_paid']."</td>";
    echo "</tr>";   
 

$sales_total_amount=$sales_total_amount+$obj['amount_paid'];
    
    
}    
}    
?>
<tr>
<th colspan="7">Total Amount</th>
<th ></th>
<th colspan="3"><?php 

echo number_format($sales_total_amount,'2','.',',');


  ?></th>





</tr>
</table>
</div>
</div>
</div>








</section>
<?php include('footer.php');



 ?>
     
  </body>
</html>
<script type="text/javascript">
var startDate;
var endDate;
//Date range picker


        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            startDate = start;
            endDate = end
        
        }
        );

        
 function getdate()
 {
     var first=startDate.format('MM/DD/YYYY');
     var second=endDate.format('MM/DD/YYYY');
  document.f1.f.value=first;
  document.f1.s.value=second;
  
 }       
        
</script>



