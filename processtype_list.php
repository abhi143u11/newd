<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.processtype.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



$processtype_list=new Ds_Processtype();
$result=$processtype_list->list_of_all_processtype();



?>
<?php include('header.php'); ?>
        <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PROCESSTYPEALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr><th>ID</th><th>Type</th><th>Edit</th><th>Delete</th></tr></thead>
<?php
if(!empty($result))
{     
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['process_type_id']."</td>";
    echo "<td>".$obj['process_type']."</td>";
    echo "<td><a href='processtype_edit.php?prid=".$obj['process_type_id']."'><i class=\"fa fa-edit\"></i></a></td>";
    echo "<td><a href='processtype_delete.php?prid=".$obj['process_type_id']."'  onclick=\"return confirm('Really delete?');\"><i class=\"fa fa-trash text-red\"></i></a></td>";
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');

if(isset($_SESSION)){
 ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
    var notify = $.notify('', {
    type: '<?php echo $_SESSION['type']; ?>',
    allow_dismiss: true,
    showProgressbar: false,
    placement: {
        from: "top",
        align: "right"
    },
});

setTimeout(function() {
    notify.update('message', '<?php echo $_SESSION['message']; ?>');
}, 1000);
   </script>
      
   <?php 
  
}
    ?>
  </body>
</html>



