<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.expense.php');


if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}

$start=0;
$second=0;
if(isset($_GET['f']))
{
$start=$_GET['f'];    
$second=$_GET['s'];    
}

$expense_list=new Ds_Expense();
$result=$expense_list->list_of_all_expenses();
if($start!=0)
{
$result=$expense_list->list_of_all_expenses_from_date($start,$second);
}


?>
<?php include('header.php'); ?>
        <section class="content">


         
        
  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo EXPENSELIST;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                
 <!--  start of datepicker  -->
                
 <div class="col-md-12"  >
              
                    <div class="form-group col-md-2">
                    
                     <div class="input-group">
                      <button class="btn btn-default pull-right" id="daterange-btn">
                        <i class="fa fa-calendar"></i> Date range 
                        <i class="fa fa-caret-down"></i>
                      </button>
                    </div>
                  
                    <form name="f1">
                     <input type="hidden" name="f" /><input type="hidden" name="s" />                  
                    
                   </div>   
         
              
                   
                   <div class="col-md-2">
                    <input type="submit" class="btn btn-primary" onclick="getdate()" type="button" name="loaddata" id="loaddata" value="Load"/>
                     
                   </div>
                        
               </form> 
 </div>                
 <!--  end of date picker -->               
                
                
                
                
                
                
                
                
                
                
                  <table id="example2" class="table table-bordered table-striped">
                  <thead>
<tr>
<tr><th>ID</th><th>Date</th><th>Description</th><th>Amount</th></tr>

</tr>
</thead>
<?php
$totalamount=0;

if(!empty($result))
{    
foreach($result as $obj)
{
    echo "<tr>";
    echo "<td>".$obj['id']."</td>";
    echo "<td>".date("d/m/Y",$obj['created_on'])."</td>";
    echo "<td>".$obj['remark']."</td>";
    echo "<td>".$obj['amount_paid']."</td>";
    echo "</tr>";
$totalamount=$totalamount+$obj['amount_paid'];
}   
 
}     
?>




<tr>
<th >Total Expense</th>
<th></th>
<th></th>

<th><?php echo number_format($totalamount,'2','.',','); ?> </th>

</tr>

</table>
</div>
</div>
</section>
<?php include('footer.php'); ?>

 <script>
      $(function () {
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     

var startDate;
var endDate;
//Date range picker


        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            startDate = start;
            endDate = end
        
        }
        );

        
 function getdate()
 {
 //    var first=startDate.format('YYYY-MM-DD');
   //  var second=endDate.format('YYYY-MM-DD');
     var first=startDate.format('MM/DD/YYYY');
     var second=endDate.format('MM/DD/YYYY');
  document.f1.f.value=first;
  document.f1.s.value=second;
 alert(first+" and "+second);
 }       
        
   </script>
      
  
  </body>
</html>



