<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.processtype.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



$prid=0;


if(isset($_GET['prid']))
{
$prid=$_GET['prid'];;
}



if(isset($_REQUEST["Update"]))
{
 $data = $database->clean_data($_POST);

    $processtype = new Ds_Processtype();
    $processtype->set_process_type_id($data['process_type_id']);
    $processtype->set_process_type_name($data['process_name']);
    
    $update_processtype=$processtype->update_processtype();  
    
    if($update_processtype=TRUE){
    
    notify("info","Process Type updated successfully");
    ?>
<script type="text/javascript">
//window.location='processtype_list.php';
</script>
    
    <?php
   }}   




$prid_process = new Ds_Processtype($prid);
$prid_process->list_of_all_processtype();
?>

<?php include('header.php'); ?>


 <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo PROCESSTYPEUPDATE;  ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
<div class="container">
<div class="row">
<div class="col-md-12">
<form class="form-horizontal" method="POST">

<!-- Select Basic -->
<div class="form-group">
  
    <input type="hidden" name="process_type_id" value="<?php echo $prid_process->get_process_type_id() ?>"/> 
 
 
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="weight">Process Type</label>  
  <div class="col-md-4">
  <input id="process_name" value="<?php echo $prid_process->get_process_type_name(); ?>" name="process_name" placeholder="" class="form-control input-md" required="" type="text">
    
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="add"></label>
  <div class="col-md-4">
    <input type="submit" id="Update" name="Update" class="btn btn-default" value="Update">
  </div>
</div>




</form>
            
</div>
</div>
</div>
</div>
</div>
</section>

<?php include('footer.php'); ?>




