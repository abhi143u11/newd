<?php
session_start();
require_once('classes/class.database.php');
require_once('classes/class.purchase.php');
require_once('classes/class.payment.php');

if(!isset($_SESSION['user']))
{
    echo "<script>window.location='login.php';</script>";
}



$purchase_list=new Ds_Purchase();
$purchase_result=$purchase_list->list_of_all_purchase_with_name();
$payment_record=new Ds_Payment();

?>
<?php include('header.php'); ?>

  <section class="content">

  <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo REPORTPURCHASESRALL;    ?> </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped">  
                  
<thead>
<tr>
<th>Bill No</th>
<th>Customer Name</th>
<th>Date</th>
<th>Broker</th>
<th>Brokarage Type</th>
<th>Brokarage Value</th>
<th>Brokarage Amount</th>
<th>Weight Type</th>
<th>Type</th>
<th>Rate</th>
<th>Weight</th>
<th>Amount</th>
<th>Paid</th>
</tr></thead>
<?php
if(!empty($purchase_result))
{   
foreach($purchase_result as $obj)
{
$bval=$obj['broker'];    
$brokername=$purchase_list->purchase_broker_name($bval);
$sid=$obj['pid'];
$paidamount=$payment_record->sum_amount_of_purchase_invoice($sid);    
    
    echo "<tr><td>".$obj['pid']."</td>";
    echo "<td>".$obj['name']."</td>";
    echo "<td>".date("d/m/Y",$obj['pdate'])."</td>";
    echo "<td>".$brokername."</td>";
    echo "<td>".$obj['brokeragetype']."</td>";
    echo "<td>".$obj['brokeragevalue']."</td>";
    echo "<td>".$obj['brokerageamount']."</td>";
    echo "<td>".$obj['weighttype']."</td>";
    echo "<td>".$obj['type']."</td>";
    echo "<td>".$obj['rate']."</td>";
    echo "<td>".$obj['weight']."</td>";
    echo "<td>".$obj['amount']."</td>";
    echo "<td>".$paidamount."</td>";
   
    echo "</tr>";
}    
}    
?>
</table>
</div>
</div>
</section>
<?php include('footer.php');


 ?>

 <script>
     
      $(function(){
          
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      
     
 
    
  </body>
</html>




